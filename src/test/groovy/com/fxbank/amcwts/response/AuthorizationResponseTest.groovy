package com.fxbank.amcwts.response

import spock.lang.Specification
/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
class AuthorizationResponseTest extends Specification {
    def 'Test Status enum'() {
        when:
        def val = testVal
        def excepted = testExcepted
        then:
        val == excepted
        where:
        testVal                                       | testExcepted
        AuthorizationResponse.Status.valueOf("OK")    | AuthorizationResponse.Status.OK
        AuthorizationResponse.Status.valueOf("ERROR") | AuthorizationResponse.Status.ERROR
    }

    def 'Test #getValue'() {
        when:
        def authorizationResponse = new AuthorizationResponse(testValue)
        then:
        authorizationResponse.getValue() == testValue
        where:
        testValue << [AuthorizationResponse.Status.OK, AuthorizationResponse.Status.ERROR]
    }

    def 'Test #getStatus'() {
        when:
        def authorizationResponse = new AuthorizationResponse(testValue)
        then:
        authorizationResponse.getStatus() == testValue
        where:
        testValue << [AuthorizationResponse.Status.OK, AuthorizationResponse.Status.ERROR]
    }

    def 'Test #getComment'() {
        when:
        def authorizationResponse = new AuthorizationResponse(AuthorizationResponse.Status.OK, testComment)
        then:
        authorizationResponse.getComment() == testComment
        where:
        testComment << [null, "Comment"]
    }
}
