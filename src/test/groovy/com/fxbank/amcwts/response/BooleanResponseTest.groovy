package com.fxbank.amcwts.response

import spock.lang.Specification

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
class BooleanResponseTest extends Specification {
    def 'Test #getValue'() {
        when:
        def booleanResponse = new BooleanResponse(testValue)
        then:
        booleanResponse.getValue() == testValue
        where:
        testValue << [true, false]
    }

    def 'Test #getComment'() {
        when:
        def booleanResponse = new BooleanResponse(true, testComment)
        then:
        booleanResponse.getComment() == testComment
        where:
        testComment << [null, "Comment"]
    }

    def 'Test constant field'() {
        when:
        def booleanResponseTrue = BooleanResponse.TRUE
        def booleanResponseFalse = BooleanResponse.FALSE
        then:
        booleanResponseTrue.getValue()
        !booleanResponseFalse.getValue()
        booleanResponseTrue.getComment() == null
        booleanResponseFalse.getComment() == null
    }
}
