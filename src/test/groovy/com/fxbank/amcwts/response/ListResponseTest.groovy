package com.fxbank.amcwts.response

import spock.lang.Specification

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
class ListResponseTest extends Specification {
    def 'Test #getValue'() {
        when:
        def listResponse = new ListResponse<String>(testList)
        then:
        listResponse.getValue() == testList
        where:
        testList << [["Hello", "test"], []]
    }

    def 'Test #getComment'() {
        when:
        def listResponse = new ListResponse([], testComment)
        then:
        listResponse.getComment() == testComment
        where:
        testComment << [null, "Comment"]
    }
}
