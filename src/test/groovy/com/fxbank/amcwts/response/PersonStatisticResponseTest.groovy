package com.fxbank.amcwts.response

import com.fxbank.amcwts.violation.Violation
import spock.lang.Specification

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
class PersonStatisticResponseTest extends Specification {
    def 'Test #getValue'() {
        when:
        def personResponse = new PersonStatisticResponse(0L, 0L, testList)
        then:
        personResponse.getValue() == testList
        where:
        testList << [[new Violation(), new Violation()], []]
    }

    def 'Test #set/getViolations'() {
        given:
        def personResponse = new PersonStatisticResponse(0, 0, [])
        when:
        personResponse.setViolations(testValue)
        then:
        personResponse.getViolations() == testValue
        where:
        testValue << [[new Violation(), new Violation()], []]
    }

    def 'Test #set/getWorkingTime'() {
        given:
        def personResponse = new PersonStatisticResponse(0, 0, [])
        when:
        personResponse.setWorkingTime(testValue)
        then:
        personResponse.getWorkingTime() == testValue
        where:
        testValue << [0, Long.MIN_VALUE, Long.MAX_VALUE]
    }

    def 'Test #set/getNonWorkingTime'() {
        given:
        def personResponse = new PersonStatisticResponse(0, 0, [])
        when:
        personResponse.setNonWorkingTime(testValue)
        then:
        personResponse.getNonWorkingTime() == testValue
        where:
        testValue << [0, Long.MIN_VALUE, Long.MAX_VALUE]
    }
}
