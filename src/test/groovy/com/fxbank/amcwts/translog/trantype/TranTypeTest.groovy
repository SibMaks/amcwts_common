package com.fxbank.amcwts.translog.trantype

import spock.lang.Specification

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
class TranTypeTest extends Specification {
    def 'Test #get/setId'() {
        given:
        def tranType = new TranType()
        when:
        tranType.setId(testValue)
        then:
        tranType.getId() == testValue
        where:
        testValue << [0, Long.MIN_VALUE, Long.MAX_VALUE, -1]
    }

    def 'Test #get/setName'() {
        given:
        def tranType = new TranType()
        when:
        tranType.setName(testValue)
        then:
        tranType.getName() == testValue
        where:
        testValue << ["", "Test name", null]
    }

    def 'Test #is/setEnter'() {
        given:
        def tranType = new TranType()
        when:
        tranType.setEnter(testValue)
        then:
        tranType.isEnter() == testValue
        where:
        testValue << [true, false]
    }

    def 'Test default constructor'() {
        when:
        def tranType = new TranType()
        then:
        tranType.getName() == null
        tranType.getId() == 0
        !tranType.isEnter()
    }

    def 'Test constructor and get methods'() {
        when:
        def tranType = new TranType(testId, testName, testEnter)
        then:
        tranType.getName() == testName
        tranType.getId() == testId
        tranType.isEnter() == testEnter
        where:
        testId              | testName    | testEnter
        Long.MAX_VALUE      | "Test name" | true
        Long.MIN_VALUE      | "Test name" | true
        1                   | "Test"      | true
        1                   | "Test name" | false
        1                   | ""          | true
        0                   | null        | true
    }
}
