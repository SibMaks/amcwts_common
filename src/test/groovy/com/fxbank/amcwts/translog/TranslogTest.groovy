package com.fxbank.amcwts.translog

import com.fxbank.amcwts.person.Person
import com.fxbank.amcwts.translog.trantype.TranType
import spock.lang.Shared
import spock.lang.Specification

import java.time.LocalDateTime

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
class TranslogTest extends Specification {
    @Shared
    private Person person1 = new Person()
    @Shared
    private Person person2 = new Person()
    @Shared
    private TranType tranType1 = new TranType()
    @Shared
    private TranType tranType2 = new TranType()
    @Shared
    private LocalDateTime localDateTime1 = LocalDateTime.of(2010, 1, 1 ,10, 00)
    @Shared
    private LocalDateTime localDateTime2 = LocalDateTime.of(2010, 1, 1 ,20, 25)
    @Shared
    private String transId1 = "TRAN_ID_1"
    @Shared
    private String transId2 = "TRAN_ID_2"

    def setup() {
        person1.setLogin("person 1")
        person2.setLogin("person 2")

        tranType1.setName("trantype 1")
        tranType2.setName("trantype 2")
    }

    def 'Test #get/setId'() {
        given:
        def translog = new Translog()
        when:
        translog.setId(testValue)
        then:
        translog.getId() == testValue
        where:
        testValue << [0, Long.MIN_VALUE, Long.MAX_VALUE, -1]
    }

    def 'Test #get/setTranDate'() {
        given:
        def translog = new Translog()
        when:
        translog.setTranDate(testValue)
        then:
        translog.getTranDate() == testValue
        where:
        testValue << [localDateTime1, localDateTime2, null]
    }

    def 'Test #is/setTranType'() {
        given:
        def translog = new Translog()
        when:
        translog.setTranType(testValue)
        then:
        translog.getTranType() == testValue
        where:
        testValue << [tranType1, tranType2, null]
    }

    def 'Test #is/setPerson'() {
        given:
        def translog = new Translog()
        when:
        translog.setPerson(testValue)
        then:
        translog.getPerson() == testValue
        where:
        testValue << [person1, person2, null]
    }

    def 'Test #is/setTranId'() {
        given:
        def translog = new Translog()
        when:
        translog.setTranId(testValue)
        then:
        translog.getTranId() == testValue
        where:
        testValue << [transId1, transId2, null]
    }

    def 'Test default constructor'() {
        when:
        def translog = new Translog()
        then:
        translog.getId() == 0
        translog.getTranId() == null
        translog.getTranDate() == null
        translog.getTranType() == null
        translog.getPerson() == null
    }

    def 'Test constructor and get methods'() {
        when:
        def translog = new Translog(testId, testTranId, testTime, testType, testPerson)
        then:
        translog.getId() == testId
        translog.getTranId() == testTranId
        translog.getTranDate() == testTime
        translog.getTranType() == testType
        translog.getPerson() == testPerson
        where:
        testId              | testTranId    | testTime       | testType  | testPerson
        Long.MAX_VALUE      | transId1      | localDateTime1 | tranType1 | person1
        Long.MIN_VALUE      | transId1      | localDateTime1 | tranType1 | person1
        1                   | null          | localDateTime1 | tranType1 | person1
        1                   | transId1      | null           | tranType1 | person1
        1                   | transId1      | localDateTime1 | null      | person1
        1                   | transId1      | localDateTime1 | tranType1 | null

        1                   | transId2      | localDateTime1 | tranType1 | person1
        1                   | transId1      | localDateTime2 | tranType1 | person1
        1                   | transId1      | localDateTime1 | tranType2 | person1
        1                   | transId1      | localDateTime1 | tranType1 | person2
    }
}
