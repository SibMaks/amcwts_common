package com.fxbank.amcwts.person.accesslevel

import spock.lang.Specification
/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 11.05.2017
 */
class AccessLevelTest  extends Specification {
    def 'Test #get/setId'() {
        given:
        def accessLevel = new AccessLevel()
        when:
        accessLevel.setId(testId)
        then:
        accessLevel.getId() == testId
        where:
        testId << [0, Long.MIN_VALUE, Long.MAX_VALUE, -1]
    }

    def 'Test #get/setName'() {
        given:
        def accessLevel = new AccessLevel()
        when:
        accessLevel.setName(testName)
        then:
        accessLevel.getName() == testName
        where:
        testName << ["", "Test name", null]
    }

    def 'Test #get/setLevel'() {
        given:
        def accessLevel = new AccessLevel()
        when:
        accessLevel.setLevel(testLevel)
        then:
        accessLevel.getLevel() == testLevel
        where:
        testLevel << [Integer.MAX_VALUE, null, Integer.MIN_VALUE, 0]
    }

    def 'Test constructor and get methods'() {
        when:
        def accessLevel = new AccessLevel(testId, testName, testLevel)
        then:
        accessLevel.getLevel() == testLevel
        accessLevel.getName() == testName
        accessLevel.getId() == testId
        where:
        testId              | testName    | testLevel
        0                   | "Test name" | Integer.MAX_VALUE
        0                   | "Test"      | Integer.MIN_VALUE
        Long.MAX_VALUE      | "Test name" | 1
        Long.MIN_VALUE      | "Test"      | 1
        0                   | null        | 1
        0                   | ""          | 1
        0                   | "Test"      | null
    }
}