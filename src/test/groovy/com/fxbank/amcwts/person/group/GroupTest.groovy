package com.fxbank.amcwts.person.group

import spock.lang.Specification
/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 11.05.2017
 */
class GroupTest extends Specification {
    def 'Test #get/setId'() {
        given:
        def group = new Group()
        when:
        group.setId(testId)
        then:
        group.getId() == testId
        where:
        testId << [0, Long.MIN_VALUE, Long.MAX_VALUE, -1]
    }

    def 'Test #get/setName'() {
        given:
        def group = new Group()
        when:
        group.setName(testName)
        then:
        group.getName() == testName
        where:
        testName << ["", "Test name", null]
    }

    def 'Test constructor and get methods'() {
        when:
        def group = new Group(testId, testName)
        then:
        group.getName() == testName
        group.getId() == testId
        where:
        testId              | testName
        0                   | "Test name"
        Long.MAX_VALUE      | "Test name"
        Long.MIN_VALUE      | "Test name"
        0                   | null
        0                   | ""
    }
}