package com.fxbank.amcwts.person.position

import com.fxbank.amcwts.person.accesslevel.AccessLevel
import spock.lang.Shared
import spock.lang.Specification

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 11.05.2017
 */
class PositionTest extends Specification {
    @Shared
    def accessLevel = new AccessLevel()
    @Shared
    def accessLevel2 = new AccessLevel(1, "test", 2)

    def 'Test #get/setId'() {
        given:
        def position = new Position()
        when:
        position.setId(testId)
        then:
        position.getId() == testId
        where:
        testId << [0, Long.MIN_VALUE, Long.MAX_VALUE, -1]
    }

    def 'Test #get/setName'() {
        given:
        def position = new Position()
        when:
        position.setName(testName)
        then:
        position.getName() == testName
        where:
        testName << ["", "Test name", null]
    }

    def 'Test #get/setAccessLevel'() {
        given:
        def position = new Position()
        when:
        position.setAccessLevel(testLevel)
        then:
        position.getAccessLevel() == testLevel
        where:
        testLevel << [new AccessLevel(), null]
    }

    def 'Test constructor and get methods'() {
        when:
        def position = new Position(testId, testName, testLevel)
        then:
        position.getAccessLevel() == testLevel
        position.getName() == testName
        position.getId() == testId
        where:
        testId | testName    | testLevel
        0      | "Test name" | accessLevel
        1      | "Test"      | accessLevel
        1      | "Test name" | accessLevel2
        0      | "Test"      | accessLevel
        0      | null        | accessLevel
        0      | "Test"      | null
    }
}