package com.fxbank.amcwts.person.worktime

import spock.lang.Shared
import spock.lang.Specification

import java.time.LocalTime

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 11.05.2017
 */
class WorkTimeTest extends Specification {
    @Shared
    def time = LocalTime.of(9, 0)
    @Shared
    def time2 = LocalTime.of(18, 0)
    @Shared
    def timeTest = LocalTime.of(13, 0)

    def 'Test default constructor'() {
        when:
        def workTime = new WorkTime()
        then:
        workTime.getId() == 0
        workTime.getTimeStart() == LocalTime.of(9, 0)
        workTime.getTimeEnd() == LocalTime.of(18, 0)
    }

    def 'Test #get/setId'() {
        given:
        def workTime = new WorkTime(1, LocalTime.of(0, 0), LocalTime.of(2, 0))
        when:
        workTime.setId(testId)
        then:
        workTime.getId() == testId
        where:
        testId << [0, Long.MIN_VALUE, Long.MAX_VALUE, -1]
    }

    def 'Test #get/setTimeStart'() {
        given:
        def workTime = new WorkTime(1, time, time2)
        when:
        workTime.setTimeStart(timeStart)
        then:
        workTime.getTimeStart() == timeStart
        where:
        timeStart << [time]
    }

    def 'Test #get/setTimeStart with NPE'() {
        given:
        def workTime = new WorkTime(1, time, time2)
        when:
        workTime.setTimeStart(null)
        then:
        thrown(NullPointerException)
    }

    def 'Test #get/setTimeStart with IllegalArgumentsException'() {
        given:
        def workTime = new WorkTime(1, time, time2)
        when:
        workTime.setTimeStart(timeStart)
        then:
        thrown(IllegalArgumentException)
        where:
        timeStart << [time2.plusHours(1)]
    }

    def 'Test #get/setTimeEnd'() {
        given:
        def workTime = new WorkTime(1, time, time2)
        when:
        workTime.setTimeEnd(timeEnd)
        then:
        workTime.getTimeEnd() == timeEnd
        where:
        timeEnd << [time]
    }

    def 'Test #get/setTimeEnd with NPE'() {
        given:
        def workTime = new WorkTime(1, time, time2)
        when:
        workTime.setTimeEnd(null)
        then:
        thrown(NullPointerException)
    }

    def 'Test #get/setTimeEnd with IllegalArgumentsException'() {
        given:
        def workTime = new WorkTime(1, time, time2)
        when:
        workTime.setTimeEnd(timeEnd)
        then:
        thrown(IllegalArgumentException)
        where:
        timeEnd << [time.minusHours(1)]
    }

    def 'Test constructor and get methods'() {
        when:
        def workTime = new WorkTime(testId, startTime, endTime)
        then:
        workTime.getTimeStart() == startTime
        workTime.getTimeEnd() == endTime
        workTime.getId() == testId
        where:
        testId              | startTime | endTime
        0                   | time      | time2
        Long.MAX_VALUE      | time      | time2
        Long.MIN_VALUE      | time      | time2
        1                   | timeTest  | time2
        1                   | time      | timeTest
    }

    def 'Test constructor with NPE'() {
        when:
        new WorkTime(testId, startTime, endTime)
        then:
        thrown(NullPointerException)
        where:
        testId | startTime | endTime
        0      | null      | time2
        0      | time      | null
    }

    def 'Test constructor with Illegal Arguments'() {
        when:
        new WorkTime(testId, startTime, endTime)
        then:
        thrown(IllegalArgumentException)
        where:
        testId | startTime | endTime
        0      | time2     | time
    }
}
