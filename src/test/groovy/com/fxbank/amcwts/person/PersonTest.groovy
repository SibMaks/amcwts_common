package com.fxbank.amcwts.person

import com.fxbank.amcwts.person.accesslevel.AccessLevel
import com.fxbank.amcwts.person.position.Position
import spock.lang.Shared
import spock.lang.Specification
/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 17.05.2017
 */
class PersonTest extends Specification {
    @Shared
    def companyDirector = new Position(1, "Director", new AccessLevel(1, "Director", 0))
    @Shared
    def mainInGroup = new Position(2, "mainInGroup", new AccessLevel(2, "mainInGroup", 1))
    @Shared
    def employee = new Position(3, "employee", new AccessLevel(3, "employee", 2))

    def 'Test methods equals and hashCode on equals objs'() {
        given:
            def testPerson = new Person()
            testPerson.setId(1)
            testPerson.setPersonId("1PI")
        when:
            def person = new Person()
            person.setId(testId)
            person.setPersonId(testPID)
            person.setLogin(testLogin)
        then:
            testPerson == person
            testPerson.hashCode() == person.hashCode()
        where:
            testId      |   testPID     |   testLogin
            1           |   "1PI"       |   "Test"
            1           |   "1PI"       |   null
            1           |   "1PI"       |   ""
    }

    def 'Test methods equals and hashCode with null PIDs'() {
        given:
        def testPerson = new Person()
        testPerson.setId(1)
        testPerson.setPersonId(null)
        when:
        def person = new Person()
        person.setId(1)
        person.setPersonId(null)
        then:
        testPerson == person
        testPerson.hashCode() == person.hashCode()
    }

    def 'Test methods equals and hashCode with one null PID'() {
        given:
        def testPerson = new Person()
        testPerson.setId(1)
        testPerson.setPersonId(null)
        when:
        def person = new Person()
        person.setId(1)
        person.setPersonId("Test")
        then:
        testPerson != person
        testPerson.hashCode() != person.hashCode()
    }

    def 'Test methods equals and hashCode on non-equals objs'() {
        given:
        def testPerson = new Person()
        testPerson.setId(1)
        testPerson.setPersonId("1PI")
        when:
        def person = new Person()
        person.setId(testId)
        person.setPersonId(testPID)
        then:
        testPerson != person
        testPerson.hashCode() != person.hashCode()
        where:
        testId      |   testPID
        2           |   "1PI"
        1           |   "2PI"
    }

    def 'Test #getAccessLevel'() {
        given:
        def testPerson = new Person()
        when:
        testPerson.setPosition(testPosition)
        testPerson.setAdmin(testADmin)
        then:
        Person.getAccessLevel(testPerson) == excepted
        where:
        testPosition      |   testADmin     | excepted
        companyDirector   |   false         | 0
        companyDirector   |   true          | 0
        mainInGroup       |   true          | 0
        employee          |   true          | 0
        mainInGroup       |   false         | 1
        employee          |   false         | 2
    }
}
