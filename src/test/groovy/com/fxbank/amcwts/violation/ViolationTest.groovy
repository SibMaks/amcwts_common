package com.fxbank.amcwts.violation

import com.fxbank.amcwts.person.Person
import com.fxbank.amcwts.person.worktime.WorkTime
import com.fxbank.amcwts.violation.violationtype.ViolationType
import spock.lang.Shared
import spock.lang.Specification

import java.time.LocalDateTime
import java.time.LocalTime

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 11.05.2017
 */
class ViolationTest extends Specification {
    @Shared
    private Person person1 = new Person()
    @Shared
    private Person person2 = new Person()
    @Shared
    private WorkTime workTime1 = new WorkTime(1, LocalTime.of(0, 0), LocalTime.of(2, 0))
    @Shared
    private ViolationType violationType1 = new ViolationType()
    @Shared
    private ViolationType violationType2 = new ViolationType()
    @Shared
    private LocalDateTime localDateTime = LocalDateTime.now()

    def setup() {
        person1.setId(1)
        person2.setId(2)
        violationType1.setId(1)
        violationType2.setId(2)
    }

    def 'Test #get/setId'() {
        given:
        def violation = new Violation()
        when:
        violation.setId(testId)
        then:
        violation.getId() == testId
        where:
        testId << [0, Long.MIN_VALUE, Long.MAX_VALUE, -1]
    }

    def 'Test #get/setReason'() {
        given:
        def violation = new Violation()
        when:
        violation.setReason(testReason)
        then:
        violation.getReason() == testReason
        where:
        testReason << [null, "Reason"]
    }

    def 'Test #get/setViolationType'() {
        given:
        def violation = new Violation()
        when:
        violation.setViolationType(testType)
        then:
        violation.getViolationType() == testType
        where:
        testType << [null, violationType1]
    }

    def 'Test #get/setWorkTime'() {
        given:
        def violation = new Violation()
        when:
        violation.setWorkTime(testWorkTime)
        then:
        violation.getWorkTime() == testWorkTime
        where:
        testWorkTime << [null, workTime1]
    }

    def 'Test #get/setPerson'() {
        given:
        def violation = new Violation()
        when:
        violation.setPerson(testPerson)
        then:
        violation.getPerson() == testPerson
        where:
        testPerson << [null, person1]
    }

    def 'Test #get/setDate'() {
        given:
        def violation = new Violation()
        when:
        violation.setDate(testDate)
        then:
        violation.getDate() == testDate
        where:
        testDate << [null, localDateTime]
    }

    def 'Test constructor'() {
        when:
        def violationType = new ViolationType()
        def workTime = new WorkTime()
        def person = new Person()
        def violation = new Violation(1, "Test reason", violationType, localDateTime, workTime, person)
        then:
        violation.getId() == 1
        violation.getReason() == "Test reason"
        violation.getViolationType() == violationType
        violation.getDate() == localDateTime
        violation.getWorkTime() == workTime
        violation.getPerson() == person
    }
}
