package com.fxbank.amcwts.violation.violationtype

import spock.lang.Specification
/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
class ViolationTypeTest extends Specification {
    def 'Test #get/setId'() {
        given:
        def violationType = new ViolationType()
        when:
        violationType.setId(testId)
        then:
        violationType.getId() == testId
        where:
        testId << [0, Long.MIN_VALUE, Long.MAX_VALUE, -1]
    }

    def 'Test #get/setName'() {
        given:
        def violationTypeType = new ViolationType()
        when:
        violationTypeType.setName(testName)
        then:
        violationTypeType.getName() == testName
        where:
        testName << [null, "Name"]
    }

    def 'Test default constructor'() {
        when:
        def violationTypeType = new ViolationType(1, "Test")
        then:
        violationTypeType.getId() == 1
        violationTypeType.getName() == "Test"
    }
}
