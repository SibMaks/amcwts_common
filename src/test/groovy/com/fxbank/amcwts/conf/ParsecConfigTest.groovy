package com.fxbank.amcwts.conf

import spock.lang.Specification

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
class ParsecConfigTest extends Specification {
    def 'Test #get/setLogin'() {
        given:
        def parsecConfig = new ParsecConfig()
        when:
        parsecConfig.setLogin(testLogin)
        then:
        parsecConfig.getLogin() == testLogin
        where:
        testLogin << ["", "Test login", null]
    }


    def 'Test #get/setPassword'() {
        given:
        def parsecConfig = new ParsecConfig()
        when:
        parsecConfig.setPassword(testPassword)
        then:
        parsecConfig.getPassword() == testPassword
        where:
        testPassword << ["", "Test password", null]
    }


    def 'Test #get/setAddress'() {
        given:
        def parsecConfig = new ParsecConfig()
        when:
        parsecConfig.setAddress(testAddress)
        then:
        parsecConfig.getAddress() == testAddress
        where:
        testAddress << ["", "address", null]
    }


    def 'Test #get/setPort'() {
        given:
        def parsecConfig = new ParsecConfig()
        when:
        parsecConfig.setPort(testPort)
        then:
        parsecConfig.getPort() == testPort
        where:
        testPort << [0, Integer.MIN_VALUE, Integer.MAX_VALUE]
    }
}
