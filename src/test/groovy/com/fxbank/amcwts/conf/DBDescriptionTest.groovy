package com.fxbank.amcwts.conf

import spock.lang.Specification

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
class DBDescriptionTest extends Specification {
    def 'Test #get/setName'() {
        given:
        def dbDescription = new DBDescription()
        when:
        dbDescription.setName(testName)
        then:
        dbDescription.getName() == testName
        where:
        testName << ["", "Test name", null]
    }

    def 'Test #get/setLogin'() {
        given:
        def dbDescription = new DBDescription()
        when:
        dbDescription.setLogin(testLogin)
        then:
        dbDescription.getLogin() == testLogin
        where:
        testLogin << ["", "Test login", null]
    }


    def 'Test #get/setPassword'() {
        given:
        def dbDescription = new DBDescription()
        when:
        dbDescription.setPassword(testPassword)
        then:
        dbDescription.getPassword() == testPassword
        where:
        testPassword << ["", "Test password", null]
    }


    def 'Test #get/setConnectionString'() {
        given:
        def dbDescription = new DBDescription()
        when:
        dbDescription.setConnectionString(testConnectionString)
        then:
        dbDescription.getConnectionString() == testConnectionString
        where:
        testConnectionString << ["", "testConnectionString", null]
    }


    def 'Test #get/setDriverClassName'() {
        given:
        def dbDescription = new DBDescription()
        when:
        dbDescription.setDriverClassName(testDriverClassName)
        then:
        dbDescription.getDriverClassName() == testDriverClassName
        where:
        testDriverClassName << ["", "testDriverClassName", null]
    }
}
