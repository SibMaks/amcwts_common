package com.fxbank.amcwts.conf

import spock.lang.Shared
import spock.lang.Specification
/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
class SystemConfigTest extends Specification {
    @Shared
    private ParsecConfig parsecConfig1 = new ParsecConfig()
    @Shared
    private ParsecConfig parsecConfig2 = new ParsecConfig()
    @Shared
    private DBDescription[] dbDescriptions1 = new DBDescription[1]
    @Shared
    private DBDescription[] dbDescriptions2 = new DBDescription[1]
    @Shared
    private DBDescription dbDescription1 = new DBDescription()

    def setup() {
        parsecConfig1.setLogin("parsec1")
        parsecConfig2.setLogin("parsec2")
        dbDescription1.setName("dbDescription1")
        DBDescription dbDescription2 = new DBDescription()
        dbDescription2.setName("dbDescription2")
        dbDescriptions1[0] = dbDescription1
        dbDescriptions2[0] = dbDescription2
    }

    def 'Test #is/setServiceOn'() {
        given:
        def systemConfig = new SystemConfig()
        when:
        systemConfig.setServiceOn(testValue)
        then:
        systemConfig.isServiceOn() == testValue
        where:
        testValue << [true, false]
    }

    def 'Test #is/setCurrentDB'() {
        given:
        def systemConfig = new SystemConfig()
        when:
        systemConfig.setCurrentDB(testValue)
        then:
        systemConfig.getCurrentDB() == testValue
        where:
        testValue << [null, "", "Current DB"]
    }

    def 'Test #is/setAdminLogin'() {
        given:
        def systemConfig = new SystemConfig()
        when:
        systemConfig.setAdminLogin(testValue)
        then:
        systemConfig.getAdminLogin() == testValue
        where:
        testValue << [null, "", "Admin login"]
    }

    def 'Test #is/setAdminPassword'() {
        given:
        def systemConfig = new SystemConfig()
        when:
        systemConfig.setAdminPassword(testValue)
        then:
        systemConfig.getAdminPassword() == testValue
        where:
        testValue << [null, "", "Admin password"]
    }

    def 'Test #is/setAdminActive'() {
        given:
        def systemConfig = new SystemConfig()
        when:
        systemConfig.setAdminActive(testValue)
        then:
        systemConfig.isAdminActive() == testValue
        where:
        testValue << [true, false]
    }

    def 'Test #is/setParsecConfig'() {
        given:
        def systemConfig = new SystemConfig()
        when:
        systemConfig.setParsecConfig(testValue)
        then:
        systemConfig.getParsecConfig() == testValue
        where:
        testValue << [null, parsecConfig1, parsecConfig2]
    }

    def 'Test #is/setSupportedBd'() {
        given:
        def systemConfig = new SystemConfig()
        when:
        systemConfig.setSupportedBd(testValue)
        then:
        systemConfig.getSupportedBd() == testValue
        where:
        testValue << [dbDescriptions1, dbDescriptions2, null]
    }

    def 'Test #findDBDescriptionByName'() {
        given:
        def systemConfig = new SystemConfig()
        when:
        systemConfig.setSupportedBd(testValue)
        then:
        systemConfig.findDBDescriptionByName(testFound) == excepted
        where:
        testValue << [dbDescriptions1, dbDescriptions2]
        testFound << ["dbDescription1", "dbDescription1"]
        excepted <<  [dbDescription1, null]
    }

    def 'Test #findDBDescriptionByName with exception'() {
        given:
        def systemConfig = new SystemConfig()
        when:
        systemConfig.setSupportedBd(testValue)
        systemConfig.findDBDescriptionByName(testFound)
        then:
        thrown(excepted)
        where:
        testValue << [dbDescriptions1, null]
        testFound << [null, "dbDescription1"]
        excepted  << [IllegalArgumentException, NullPointerException]
    }
}
