package com.fxbank.amcwts.connection;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 18.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class})
public class ConnectionFactoryTest {

    @Before
    public void setup() throws Exception {
        Field instance = ConnectionFactory.class.getDeclaredField("connectionFactory");
        instance.setAccessible(true);
        instance.set(null, null);

        Field instanceConfigWorker = ConfigWorker.class.getDeclaredField("instance");
        instanceConfigWorker.setAccessible(true);
        instanceConfigWorker.set(null, null);

        mockStatic(Class.class);
        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));

        File destFile = new File(getClass().getResource("/conf/data-source.conf.ini").getPath());
        if(destFile.exists())
            destFile.delete();

        Files.copy(new File(getClass().getResource("/conf/correct.data-source.conf.ini").getPath()).toPath(),
                destFile.toPath());
    }

    @Test
    public void getInstance() {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        Assert.assertEquals(connectionFactory, ConnectionFactory.getInstance());
    }

    @Test(expected = RuntimeException.class)
    public void getInstanceWithIncorrectFile() throws IOException {
        File destFile = new File(getClass().getResource("/conf/data-source.conf.ini").getPath());
        if(destFile.exists())
            destFile.delete();

        Files.copy(new File(getClass().getResource("/conf/incorrect.data-source.conf.ini").getPath()).toPath(),
                destFile.toPath());

        ConnectionFactory.getInstance();
    }

    @Test(expected = RuntimeException.class)
    public void getInstanceWithEmptyFile() throws IOException {
        File destFile = new File(getClass().getResource("/conf/data-source.conf.ini").getPath());
        if(destFile.exists())
            destFile.delete();

        Files.copy(new File(getClass().getResource("/conf/empty.data-source.conf.ini").getPath()).toPath(),
                destFile.toPath());

        ConnectionFactory.getInstance();
    }

    @Test(expected = RuntimeException.class)
    public void getInstanceWithEmptyValidQuery() throws IOException {
        File destFile = new File(getClass().getResource("/conf/data-source.conf.ini").getPath());
        if(destFile.exists())
            destFile.delete();

        Files.copy(new File(getClass().getResource("/conf/emptyQuery.data-source.conf.ini").getPath()).toPath(),
                destFile.toPath());

        ConnectionFactory.getInstance();
    }

    @Test(expected = RuntimeException.class)
    public void getInstanceWithNullValidQuery() throws IOException {
        File destFile = new File(getClass().getResource("/conf/data-source.conf.ini").getPath());
        if(destFile.exists())
            destFile.delete();

        Files.copy(new File(getClass().getResource("/conf/invalidQuery.data-source.conf.ini").getPath()).toPath(),
                destFile.toPath());

        ConnectionFactory.getInstance();
    }

    @Test
    public void updatePropertiesWithNullConfWorker() {
        mockStatic(ConfigWorker.class);
        when(ConfigWorker.getInstance()).thenReturn(null);

        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        connectionFactory.updateProperties();
    }
}