package com.fxbank.amcwts.translog;

import com.fxbank.amcwts.CommonDAO;
import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.ParsecConfig;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.person.PersonDAO;
import com.fxbank.amcwts.person.ProxyPersonDAO;
import com.fxbank.amcwts.translog.trantype.ProxyTranTypeDAO;
import com.fxbank.amcwts.translog.trantype.TranType;
import com.fxbank.amcwts.translog.trantype.TranTypeDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.reset;
import static org.powermock.api.mockito.PowerMockito.*;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 17.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class ParsecTranslogDAOTest {
    private Field instance;
    private Field instanceProxyPerson;
    private Field instanceProxyTranType;
    private PersonDAO mockPersonDAO;
    private PreparedStatement preparedStatement;
    private TranTypeDAO mockTranTypeDAO;
    private Connection connection;

    private Person person = new Person();
    private Person person2 = new Person();
    private TranType tranType = new TranType();
    private Translog testTranslog = new Translog(-1, "tranId", LocalDateTime.now(), tranType, person);
    private Translog testTranslog2 = new Translog(-1, "tranId2", LocalDateTime.now(), tranType, person);
    private Translog testTranslog3 = new Translog(-1, "tranId3", LocalDateTime.now(), tranType, person);
    private Translog testTranslog4 = new Translog(-1, "tranId4", LocalDateTime.now(), tranType, person);

    public ParsecTranslogDAOTest() throws NoSuchFieldException {
        instance = ParsecTranslogDAO.class.getDeclaredField("TRANSLOG_DAO");
        instance.setAccessible(true);

        instanceProxyPerson = ProxyPersonDAO.class.getDeclaredField("PERSON_DAO");
        instanceProxyPerson.setAccessible(true);

        instanceProxyTranType = ProxyTranTypeDAO.class.getDeclaredField("TRAN_TYPE_DAO");
        instanceProxyTranType.setAccessible(true);

        preparedStatement = mock(PreparedStatement.class);
        mockPersonDAO = mock(PersonDAO.class);
        mockTranTypeDAO = mock(TranTypeDAO.class);
        connection = mock(Connection.class);
    }

    @Before
    public void setup() throws Exception {
        reset(preparedStatement, mockPersonDAO, mockTranTypeDAO, connection);

        person.setPersonId("PERSON_ID_1");
        person2.setPersonId("PERSON_ID_2");
        when(mockPersonDAO.findByPersId(person.getPersonId())).thenReturn(person);
        when(mockPersonDAO.findByPersId(person2.getPersonId())).thenReturn(person2);
        when(mockTranTypeDAO.findById(anyLong())).thenReturn(tranType);

        instance.set(null, null);
        instanceProxyPerson.set(null, mockPersonDAO);
        instanceProxyTranType.set(null, mockTranTypeDAO);

        mockStatic(ConnectionFactory.class);

        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));
        ParsecConfig parsecConfig = ConfigWorker.getInstance().getSystemConfig().getParsecConfig();

        String connectionString = String.format("jdbc:sqlserver://%s:%s;databaseName=Parsec3Trans;integratedSecurity=false;", parsecConfig.getAddress(), parsecConfig.getPort());

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connection, "prepareStatement", any()).thenReturn(preparedStatement);
        when(connection, "prepareStatement", any(), anyInt()).thenReturn(preparedStatement);
        when(connectionFactory.getConnection(connectionString, parsecConfig.getLogin(), parsecConfig.getPassword())).thenReturn(connection);

        when(mockTranTypeDAO.add(any())).thenReturn(true);
        when(mockTranTypeDAO.getAll()).thenReturn(Collections.emptyList());

        testTranslog.setPerson(person);
        testTranslog2.setPerson(person);
        testTranslog3.setPerson(person);
        testTranslog4.setPerson(person);
    }

    @Test
    public void getAll() throws Exception {
        List<Translog> translogs = new LinkedList<>();
        translogs.add(testTranslog);
        translogs.add(testTranslog2);
        translogs.add(testTranslog3);
        translogs.add(testTranslog4);

        init4Translog();

        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getAll(null), translogs));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithException() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenThrow(new SQLException());
        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.getAll(null);
    }

    @Test
    public void getAllByDates() throws Exception {
        List<Translog> translogs = new LinkedList<>();
        translogs.add(testTranslog);
        translogs.add(testTranslog2);
        translogs.add(testTranslog3);
        translogs.add(testTranslog4);

        init4Translog();

        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getAllByDates(null,  LocalDateTime.now(), LocalDateTime.now()), translogs));
    }

    @Test(expected = RuntimeException.class)
    public void getAllByDatesWithException() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenThrow(new SQLException());
        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.getAllByDates(null,  LocalDateTime.now(), LocalDateTime.now());
    }

    @Test
    public void getPersonTranslogsByDates() throws Exception {
        testTranslog.setPerson(person2);
        testTranslog3.setPerson(person2);

        List<Translog> translogs = new LinkedList<>();
        translogs.add(testTranslog);
        translogs.add(testTranslog3);

        init4Translog();

        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getPersonTranslogsByDates(person2,  LocalDateTime.now(), LocalDateTime.now()), translogs));
    }

    @Test
    public void createWithIssetTranTypeEnter() {
        List<TranType> tranTypes = new LinkedList<>();
        TranType testTranType = new TranType();
        testTranType.setEnter(true);
        tranType.setEnter(true);
        tranTypes.add(tranType);
        tranTypes.add(testTranType);
        when(mockTranTypeDAO.getAll()).thenReturn(tranTypes);

        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        Assert.assertNotNull(translogDAO);
        Assert.assertEquals(translogDAO, ParsecTranslogDAO.getInstance());
    }

    @Test
    public void createWithIssetTranTypeExit() {
        List<TranType> tranTypes = new LinkedList<>();
        TranType testTranType = new TranType();
        testTranType.setEnter(false);
        tranType.setEnter(false);
        tranTypes.add(tranType);
        tranTypes.add(testTranType);
        when(mockTranTypeDAO.getAll()).thenReturn(tranTypes);

        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        Assert.assertNotNull(translogDAO);
        Assert.assertEquals(translogDAO, ParsecTranslogDAO.getInstance());
    }

    @Test
    public void createWithIssetTranTypes() {
        List<TranType> tranTypes = new LinkedList<>();
        TranType testTranType = new TranType();
        TranType testTranType2 = new TranType();
        TranType testTranType3 = new TranType();
        tranType.setEnter(false);
        testTranType.setEnter(false);
        testTranType2.setEnter(true);
        testTranType3.setEnter(true);
        tranTypes.add(tranType);
        tranTypes.add(testTranType);
        tranTypes.add(testTranType2);
        tranTypes.add(testTranType3);
        when(mockTranTypeDAO.getAll()).thenReturn(tranTypes);

        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        Assert.assertNotNull(translogDAO);
        Assert.assertEquals(translogDAO, ParsecTranslogDAO.getInstance());
    }

    @Test(expected = RuntimeException.class)
    public void createWithErrorOfAddTranTypeEnter() {
        List<TranType> tranTypes = new LinkedList<>();
        tranType.setEnter(true);
        tranTypes.add(tranType);
        when(mockTranTypeDAO.getAll()).thenReturn(tranTypes);
        when(mockTranTypeDAO.add(any())).thenReturn(false);

        ParsecTranslogDAO.getInstance();
    }

    @Test(expected = RuntimeException.class)
    public void createWithErrorOfAddTranTypeExit() {
        List<TranType> tranTypes = new LinkedList<>();
        tranType.setEnter(false);
        tranTypes.add(tranType);
        when(mockTranTypeDAO.getAll()).thenReturn(tranTypes);
        when(mockTranTypeDAO.add(any())).thenReturn(false);

        ParsecTranslogDAO.getInstance();
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.getAll(person);
    }

    @Test(expected = RuntimeException.class)
    public void getAllByDatesWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.getAllByDates(person, LocalDateTime.now(), LocalDateTime.now());
    }

    @Test(expected = RuntimeException.class)
    public void getPersonTranslogsByDatesWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.getPersonTranslogsByDates(person, LocalDateTime.now(), LocalDateTime.now());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void add() {
        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.add(testTranslog);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void addAll() {
        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.addAll(Collections.emptyList());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void remove() {
        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.remove(testTranslog);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void removeAll() {
        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.removeAll(Collections.emptyList());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void update() {
        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.update(testTranslog);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void findById() {
        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.findById(0);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void contains() {
        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.contains(testTranslog);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getLast() throws IllegalAccessException {
        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.getLast(person);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getLastByPersonAndType() {
        TranslogDAO translogDAO = ParsecTranslogDAO.getInstance();
        translogDAO.getLastByPersonAndType(person, tranType);
    }

    static boolean assertEqualsListByContent(List<Translog> firstList, List<Translog> secondList) {
        if(firstList.size() != secondList.size())
            return false;
        for(int i = 0, size = firstList.size(); i < size; i++)
            if(!assertEqualsByContent(firstList.get(i), secondList.get(i)))
                return false;
        return true;
    }

    static boolean assertEqualsByContent(Translog translog, Translog translog2) {
        return translog.getId() == translog2.getId();
    }

    void init4Translog() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getString", "TRANTYPE_ID")
                .thenReturn("590144")
                .thenReturn("test")
                .thenReturn("590144")
                .thenReturn("test")
                .thenReturn(null);
        when(resultSet, "getString", "USR_ID")
                .thenReturn(testTranslog.getPerson().getPersonId())
                .thenReturn(testTranslog2.getPerson().getPersonId())
                .thenReturn(testTranslog3.getPerson().getPersonId())
                .thenReturn(testTranslog4.getPerson().getPersonId())
                .thenReturn(null);
        when(resultSet, "getString", "TRAN_DATE")
                .thenReturn(testTranslog.getTranDate().format(CommonDAO.MS_SQL_DATE_TIME_FORMATTER))
                .thenReturn(testTranslog2.getTranDate().format(CommonDAO.MS_SQL_DATE_TIME_FORMATTER))
                .thenReturn(testTranslog3.getTranDate().format(CommonDAO.MS_SQL_DATE_TIME_FORMATTER))
                .thenReturn(testTranslog4.getTranDate().format(CommonDAO.MS_SQL_DATE_TIME_FORMATTER))
                .thenReturn(null);
        when(resultSet, "getString", "TRAN_ID")
                .thenReturn(testTranslog.getTranId())
                .thenReturn(testTranslog2.getTranId())
                .thenReturn(testTranslog3.getTranId())
                .thenReturn(testTranslog4.getTranId())
                .thenReturn(null);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);
    }
}