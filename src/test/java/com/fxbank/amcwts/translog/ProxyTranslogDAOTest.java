package com.fxbank.amcwts.translog;

import com.fxbank.amcwts.CommonTest;
import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.translog.trantype.TranType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, MySQLTranslogDAO.class})
public class ProxyTranslogDAOTest {
    private Field instance;
    private SystemConfig systemConfig;
    private TranslogDAO mockTranslogDAO;
    private Translog testTranslog;
    private Person testPerson = new Person();
    private TranType testTranType = new TranType();
    private LocalDateTime localDateTime = LocalDateTime.now();

    public ProxyTranslogDAOTest() throws NoSuchFieldException {
        instance = ProxyTranslogDAO.class.getDeclaredField("TRANSLOG_DAO");
        systemConfig = mock(SystemConfig.class);
        mockTranslogDAO = mock(MySQLTranslogDAO.class);
        testTranslog = new Translog();
    }

    @Before
    public void setUp() throws Exception {
        instance.setAccessible(true);
        instance.set(null, null);

        Mockito.reset(systemConfig);
        Mockito.reset(mockTranslogDAO);

        PowerMockito.mockStatic(ConfigWorker.class);
        PowerMockito.mockStatic(MySQLTranslogDAO.class);

        ConfigWorker configWorker = PowerMockito.mock(ConfigWorker.class);
        PowerMockito.when(configWorker.getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(ConfigWorker.getInstance()).thenReturn(configWorker);

        when(ConfigWorker.getInstance().getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(MySQLTranslogDAO.getInstance()).thenReturn(mockTranslogDAO);
    }

    @Test
    public void currentDBIsCorrect() throws Exception {
        PowerMockito.when(mockTranslogDAO.getAll()).thenReturn(Collections.emptyList());
        PowerMockito.when(mockTranslogDAO.getAll(CommonTest.testPerson)).thenReturn(Collections.emptyList());
        PowerMockito.when(mockTranslogDAO.add(testTranslog)).thenReturn(false);
        PowerMockito.when(mockTranslogDAO.remove(testTranslog)).thenReturn(false);
        PowerMockito.when(mockTranslogDAO.update(testTranslog)).thenReturn(false);
        PowerMockito.when(mockTranslogDAO.contains(testTranslog)).thenReturn(false);
        PowerMockito.when(mockTranslogDAO.findById(0)).thenReturn(null);

        PowerMockito.when(mockTranslogDAO.addAll(Collections.emptyList())).thenReturn(false);
        PowerMockito.when(mockTranslogDAO.removeAll(Collections.emptyList())).thenReturn(false);
        PowerMockito.when(mockTranslogDAO.getLast(testPerson)).thenReturn(null);
        PowerMockito.when(mockTranslogDAO.getLastByPersonAndType(testPerson, testTranType)).thenReturn(null);
        PowerMockito.when(mockTranslogDAO.getAllByDates(testPerson, localDateTime, localDateTime)).thenReturn(Collections.emptyList());
        PowerMockito.when(mockTranslogDAO.getPersonTranslogsByDates(testPerson, localDateTime, localDateTime)).thenReturn(Collections.emptyList());

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        TranslogDAO translogDAO = ProxyTranslogDAO.getInstance();

        CommonTest.testOnDefault(translogDAO, testTranslog, Collections.emptyList(),false, null);

        Assert.assertEquals(translogDAO.addAll(Collections.emptyList()), false);
        Assert.assertEquals(translogDAO.removeAll(Collections.emptyList()), false);
        Assert.assertEquals(translogDAO.getLast(testPerson), null);
        Assert.assertEquals(translogDAO.getLastByPersonAndType(testPerson, testTranType), null);
        Assert.assertEquals(translogDAO.getAllByDates(testPerson, localDateTime, localDateTime), Collections.emptyList());
        Assert.assertEquals(translogDAO.getPersonTranslogsByDates(testPerson, localDateTime, localDateTime), Collections.emptyList());
    }

    @Test
    public void currentDBAndCorrectVal() throws Exception {
        List<Translog> translogs = new LinkedList<>();
        translogs.add(new Translog());
        PowerMockito.when(mockTranslogDAO.getAll()).thenReturn(translogs);
        PowerMockito.when(mockTranslogDAO.getAll(CommonTest.testPerson)).thenReturn(translogs);
        PowerMockito.when(mockTranslogDAO.add(testTranslog)).thenReturn(true);
        PowerMockito.when(mockTranslogDAO.remove(testTranslog)).thenReturn(true);
        PowerMockito.when(mockTranslogDAO.update(testTranslog)).thenReturn(true);
        PowerMockito.when(mockTranslogDAO.contains(testTranslog)).thenReturn(true);
        PowerMockito.when(mockTranslogDAO.findById(0)).thenReturn(testTranslog);

        PowerMockito.when(mockTranslogDAO.addAll(Collections.emptyList())).thenReturn(true);
        PowerMockito.when(mockTranslogDAO.removeAll(Collections.emptyList())).thenReturn(true);
        PowerMockito.when(mockTranslogDAO.getLast(testPerson)).thenReturn(translogs.get(0));
        PowerMockito.when(mockTranslogDAO.getLastByPersonAndType(testPerson, testTranType)).thenReturn(translogs.get(0));
        PowerMockito.when(mockTranslogDAO.getAllByDates(testPerson, localDateTime, localDateTime)).thenReturn(translogs);
        PowerMockito.when(mockTranslogDAO.getPersonTranslogsByDates(testPerson, localDateTime, localDateTime)).thenReturn(translogs);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        TranslogDAO translogDAO = ProxyTranslogDAO.getInstance();

        CommonTest.testOnDefault(translogDAO, testTranslog, translogs,true, testTranslog);

        Assert.assertEquals(translogDAO.addAll(Collections.emptyList()), true);
        Assert.assertEquals(translogDAO.removeAll(Collections.emptyList()), true);
        Assert.assertEquals(translogDAO.getLast(testPerson), translogs.get(0));
        Assert.assertEquals(translogDAO.getLastByPersonAndType(testPerson, testTranType), translogs.get(0));
        Assert.assertEquals(translogDAO.getAllByDates(testPerson, localDateTime, localDateTime), translogs);
        Assert.assertEquals(translogDAO.getPersonTranslogsByDates(testPerson, localDateTime, localDateTime), translogs);
    }

    @Test
    public void currentDBIsNull() throws IllegalAccessException {
        when(systemConfig.getCurrentDB()).thenReturn(null);
        TranslogDAO translogDAO = ProxyTranslogDAO.getInstance();
        CommonTest.testOnDefault(translogDAO, testTranslog, Collections.emptyList(), false, null);

        Assert.assertEquals(translogDAO.addAll(Collections.emptyList()), false);
        Assert.assertEquals(translogDAO.removeAll(Collections.emptyList()), false);
        Assert.assertEquals(translogDAO.getLast(testPerson), null);
        Assert.assertEquals(translogDAO.getLastByPersonAndType(testPerson, testTranType), null);
        Assert.assertEquals(translogDAO.getAllByDates(testPerson, localDateTime, localDateTime), Collections.emptyList());
        Assert.assertEquals(translogDAO.getPersonTranslogsByDates(testPerson, localDateTime, localDateTime), Collections.emptyList());
    }

    @Test
    public void currentDBIsEmpty() throws IllegalAccessException {
        when(systemConfig.getCurrentDB()).thenReturn("");
        TranslogDAO translogDAO = ProxyTranslogDAO.getInstance();
        CommonTest.testOnDefault(translogDAO, testTranslog, Collections.emptyList(), false, null);

        Assert.assertEquals(translogDAO.addAll(Collections.emptyList()), false);
        Assert.assertEquals(translogDAO.removeAll(Collections.emptyList()), false);
        Assert.assertEquals(translogDAO.getLast(testPerson), null);
        Assert.assertEquals(translogDAO.getLastByPersonAndType(testPerson, testTranType), null);
        Assert.assertEquals(translogDAO.getAllByDates(testPerson, localDateTime, localDateTime), Collections.emptyList());
        Assert.assertEquals(translogDAO.getPersonTranslogsByDates(testPerson, localDateTime, localDateTime), Collections.emptyList());
    }

    @Test
    public void existingInstanceTest() throws Exception {
        TranslogDAO translogDAO = ProxyTranslogDAO.getInstance();

        instance.set(null, translogDAO);

        Assert.assertEquals(translogDAO, ProxyTranslogDAO.getInstance());
    }
}