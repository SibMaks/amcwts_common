package com.fxbank.amcwts.translog.trantype;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 17.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class MySQLTranTypeDAOTest {
    private Field instance;
    private Connection connection;
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);
    private TranType testTranType = new TranType(-1, "Test name");

    public MySQLTranTypeDAOTest() throws SQLException, NoSuchFieldException {
        instance = MySQLTranTypeDAO.class.getDeclaredField("TRAN_TYPE_DAO");
        instance.setAccessible(true);
        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));
        mockStatic(ConnectionFactory.class);
        connection = mock(Connection.class);

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenReturn(connection);
    }

    @Before
    public void before() throws Exception {
        instance.set(null, null);
        Mockito.reset(connection);
        when(connection, "prepareStatement", any()).thenReturn(preparedStatement);
        when(connection, "prepareStatement", any(), anyInt()).thenReturn(preparedStatement);

        testTranType.setId(-1);
        testTranType.setName("Test name");
    }

    @Test
    public void add() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertTrue(tranTypeDAO.add(testTranType));
        /*verify(preparedStatement).setString(1, "Test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setInt(3, 1);
        verify(preparedStatement).getGeneratedKeys();
        verify(generatedKey).getLong(1);*/
        Assert.assertEquals(testTranType.getId(), 1);
    }

    @Test
    public void addWithErrorOfGeneratedKey() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(false);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertFalse(tranTypeDAO.add(testTranType));
    }

    @Test
    public void addWithErrorOfAddingInDB() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(0);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertFalse(tranTypeDAO.add(testTranType));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionUpdate() throws Exception {
        testTranType.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertFalse(tranTypeDAO.add(testTranType));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionGetConnection() throws Exception {
        testTranType.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertFalse(tranTypeDAO.add(testTranType));
    }

    @Test
    public void update() throws Exception {
        add();
        testTranType.setId(1);
        testTranType.setName("New test name");

        when(preparedStatement, "executeUpdate").thenReturn(1);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertTrue(tranTypeDAO.update(testTranType));
        /*verify(preparedStatement).setString(1, "New test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setLong(3, 1);*/
    }

    @Test
    public void updateWithUpdateErrorInDB() throws Exception {
        testTranType.setId(1);
        testTranType.setName("New test name");

        when(preparedStatement, "executeUpdate").thenReturn(0);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertFalse(tranTypeDAO.update(testTranType));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionUpdate() throws Exception {
        testTranType.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertFalse(tranTypeDAO.update(testTranType));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionGetConnection() throws Exception {
        testTranType.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertFalse(tranTypeDAO.update(testTranType));
    }

    @Test
    public void remove() throws Exception {
        testTranType.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertTrue(tranTypeDAO.remove(testTranType));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test
    public void removeWithErrorInDB() throws Exception {
        testTranType.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertFalse(tranTypeDAO.remove(testTranType));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionUpdate() throws Exception {
        testTranType.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertFalse(tranTypeDAO.remove(testTranType));
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionGetConnection() throws Exception {
        testTranType.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertFalse(tranTypeDAO.remove(testTranType));
    }

    @Test
    public void getAll() throws Exception {
        testTranType.setId(1);
        List<TranType> tranTypeList = new LinkedList<>();
        tranTypeList.add(testTranType);
        TranType testTranType2 = new TranType(2, "test2");
        tranTypeList.add(testTranType2);
        TranType testTranType3 = new TranType(3, null, true);
        tranTypeList.add(testTranType3);
        TranType testTranType4 = new TranType(4, "test4");
        tranTypeList.add(testTranType4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "TRANTYPE_id")
                .thenReturn(testTranType.getId())
                .thenReturn(testTranType2.getId())
                .thenReturn(testTranType3.getId())
                .thenReturn(testTranType4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "TRANTYPE_name")
                .thenReturn(testTranType.getName())
                .thenReturn(testTranType2.getName())
                .thenReturn(testTranType3.getName())
                .thenReturn(testTranType4.getName())
                .thenReturn(null);
        when(resultSet, "getBoolean", "TRANTYPE_is_enter")
                .thenReturn(testTranType.isEnter())
                .thenReturn(testTranType2.isEnter())
                .thenReturn(testTranType3.isEnter())
                .thenReturn(testTranType4.isEnter())
                .thenReturn(false);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(tranTypeDAO.getAll(), tranTypeList));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionQuery() throws Exception {
        testTranType.setId(1);

        when(preparedStatement, "executeQuery").thenThrow(new SQLException());

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        tranTypeDAO.getAll();
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithGetConnectionException() throws Exception {
        testTranType.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        tranTypeDAO.getAll();
    }

    @Test
    public void getAllByPerson() throws Exception {
        testTranType.setId(1);
        List<TranType> tranTypeList = new LinkedList<>();
        tranTypeList.add(testTranType);
        TranType testTranType2 = new TranType(2, "test2");
        tranTypeList.add(testTranType2);
        TranType testTranType3 = new TranType(3, null, true);
        tranTypeList.add(testTranType3);
        TranType testTranType4 = new TranType(4, "test4");
        tranTypeList.add(testTranType4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "TRANTYPE_id")
                .thenReturn(testTranType.getId())
                .thenReturn(testTranType2.getId())
                .thenReturn(testTranType3.getId())
                .thenReturn(testTranType4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "TRANTYPE_name")
                .thenReturn(testTranType.getName())
                .thenReturn(testTranType2.getName())
                .thenReturn(testTranType3.getName())
                .thenReturn(testTranType4.getName())
                .thenReturn(null);
        when(resultSet, "getBoolean", "TRANTYPE_is_enter")
                .thenReturn(testTranType.isEnter())
                .thenReturn(testTranType2.isEnter())
                .thenReturn(testTranType3.isEnter())
                .thenReturn(testTranType4.isEnter())
                .thenReturn(false);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(tranTypeDAO.getAll(null), tranTypeList));
    }


    @Test
    public void findById() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L).thenReturn(2L).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        testTranType.setId(1);
        TranType testTranType2 = new TranType(2, "test2");
        TranType testTranType3 = new TranType(3, null, true);
        TranType testTranType4 = new TranType(4, "test4");
        tranTypeDAO.add(testTranType);
        tranTypeDAO.add(testTranType2);
        tranTypeDAO.add(testTranType4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true);

        when(resultSet, "getLong", "TRANTYPE_id")
                .thenReturn(testTranType3.getId());
        when(resultSet, "getString", "TRANTYPE_name")
                .thenReturn(testTranType3.getName());
        when(resultSet, "getBoolean", "TRANTYPE_is_enter")
                .thenReturn(testTranType3.isEnter());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Assert.assertEquals(tranTypeDAO.findById(1), testTranType);
        Assert.assertEquals(tranTypeDAO.findById(2), testTranType2);
        Assert.assertTrue(assertEqualsByContent(tranTypeDAO.findById(3), testTranType3));
        Assert.assertEquals(tranTypeDAO.findById(4), testTranType4);
    }

    @Test(expected = RuntimeException.class)
    public void addWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        tranTypeDAO.add(testTranType);
    }

    @Test(expected = RuntimeException.class)
    public void removeWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        tranTypeDAO.remove(testTranType);
    }

    @Test(expected = RuntimeException.class)
    public void updateWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranTypeDAO tranTypeDAO = MySQLTranTypeDAO.getInstance();
        tranTypeDAO.update(testTranType);
    }

    static boolean assertEqualsListByContent(List<TranType> firstList, List<TranType> secondList) {
        if(firstList.size() != secondList.size())
            return false;
        for(int i = 0, size = firstList.size(); i < size; i++)
            if(!assertEqualsByContent(firstList.get(i), secondList.get(i)))
                return false;
        return true;
    }

    static boolean assertEqualsByContent(TranType tranType, TranType tranType2) {
        return tranType.getId() == tranType2.getId();
    }
}