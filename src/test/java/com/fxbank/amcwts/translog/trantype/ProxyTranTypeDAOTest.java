package com.fxbank.amcwts.translog.trantype;

import com.fxbank.amcwts.CommonTest;
import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, MySQLTranTypeDAO.class})
public class ProxyTranTypeDAOTest {
    private Field instance;
    private SystemConfig systemConfig;
    private TranTypeDAO mockTranTypeDAO;
    private TranType testTranType;

    public ProxyTranTypeDAOTest() throws NoSuchFieldException {
        instance = ProxyTranTypeDAO.class.getDeclaredField("TRAN_TYPE_DAO");
        systemConfig = mock(SystemConfig.class);
        mockTranTypeDAO = mock(MySQLTranTypeDAO.class);
        testTranType = new TranType();
    }

    @Before
    public void setUp() throws Exception {
        instance.setAccessible(true);
        instance.set(null, null);

        Mockito.reset(systemConfig);
        Mockito.reset(mockTranTypeDAO);

        PowerMockito.mockStatic(ConfigWorker.class);
        PowerMockito.mockStatic(MySQLTranTypeDAO.class);

        ConfigWorker configWorker = PowerMockito.mock(ConfigWorker.class);
        PowerMockito.when(configWorker.getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(ConfigWorker.getInstance()).thenReturn(configWorker);

        when(ConfigWorker.getInstance().getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(MySQLTranTypeDAO.getInstance()).thenReturn(mockTranTypeDAO);
    }

    @Test
    public void currentDBIsCorrect() throws Exception {
        PowerMockito.when(mockTranTypeDAO.getAll()).thenReturn(Collections.emptyList());
        PowerMockito.when(mockTranTypeDAO.getAll(CommonTest.testPerson)).thenReturn(Collections.emptyList());
        PowerMockito.when(mockTranTypeDAO.add(testTranType)).thenReturn(false);
        PowerMockito.when(mockTranTypeDAO.remove(testTranType)).thenReturn(false);
        PowerMockito.when(mockTranTypeDAO.update(testTranType)).thenReturn(false);
        PowerMockito.when(mockTranTypeDAO.contains(testTranType)).thenReturn(false);
        PowerMockito.when(mockTranTypeDAO.findById(0)).thenReturn(null);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        TranTypeDAO tranTypeDAO = ProxyTranTypeDAO.getInstance();

        CommonTest.testOnDefault(tranTypeDAO, testTranType, Collections.emptyList(),false, null);
    }

    @Test
    public void currentDBAndCorrectVal() throws Exception {
        List<TranType> tranTypes = new LinkedList<>();
        tranTypes.add(new TranType());
        PowerMockito.when(mockTranTypeDAO.getAll()).thenReturn(tranTypes);
        PowerMockito.when(mockTranTypeDAO.getAll(CommonTest.testPerson)).thenReturn(tranTypes);
        PowerMockito.when(mockTranTypeDAO.add(testTranType)).thenReturn(true);
        PowerMockito.when(mockTranTypeDAO.remove(testTranType)).thenReturn(true);
        PowerMockito.when(mockTranTypeDAO.update(testTranType)).thenReturn(true);
        PowerMockito.when(mockTranTypeDAO.contains(testTranType)).thenReturn(true);
        PowerMockito.when(mockTranTypeDAO.findById(0)).thenReturn(testTranType);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        TranTypeDAO tranTypeDAO = ProxyTranTypeDAO.getInstance();

        CommonTest.testOnDefault(tranTypeDAO, testTranType, tranTypes,true, testTranType);
    }

    @Test
    public void currentDBIsNull() {
        when(systemConfig.getCurrentDB()).thenReturn(null);
        TranTypeDAO tranTypeDAO = ProxyTranTypeDAO.getInstance();
        CommonTest.testOnDefault(tranTypeDAO, testTranType, Collections.emptyList(), false, null);
    }

    @Test
    public void currentDBIsEmpty() {
        when(systemConfig.getCurrentDB()).thenReturn("");
        TranTypeDAO tranTypeDAO = ProxyTranTypeDAO.getInstance();
        CommonTest.testOnDefault(tranTypeDAO, testTranType, Collections.emptyList(), false, null);
    }

    @Test
    public void existingInstanceTest() throws Exception {
        TranTypeDAO tranTypeDAO = ProxyTranTypeDAO.getInstance();

        instance.set(null, tranTypeDAO);

        Assert.assertEquals(tranTypeDAO, ProxyTranTypeDAO.getInstance());
    }
}