package com.fxbank.amcwts.translog;

import com.fxbank.amcwts.CommonDAO;
import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.person.PersonDAO;
import com.fxbank.amcwts.person.ProxyPersonDAO;
import com.fxbank.amcwts.person.accesslevel.AccessLevel;
import com.fxbank.amcwts.person.group.Group;
import com.fxbank.amcwts.person.position.Position;
import com.fxbank.amcwts.translog.trantype.ProxyTranTypeDAO;
import com.fxbank.amcwts.translog.trantype.TranType;
import com.fxbank.amcwts.translog.trantype.TranTypeDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.*;
import static org.powermock.api.mockito.PowerMockito.*;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 17.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class MySQLTranslogDAOTest {
    private Field instance;
    private Field instanceProxyPerson;
    private Field instanceProxyTranType;
    private Connection connection;
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);
    private TranTypeDAO mockTranTypeDAO = mock(TranTypeDAO.class);
    private PersonDAO mockPersonDAO = mock(PersonDAO.class);
    private Person person = new Person();
    private TranType tranType = new TranType();
    private TranType tranType2 = new TranType();
    private Translog testTranslog;
    private Translog testTranslog2;
    private Translog testTranslog3;
    private Translog testTranslog4;

    @Before
    public void setup() throws Exception {
        testTranslog = new Translog(-1, "tranId", LocalDateTime.now(), tranType, person);
        testTranslog2 = new Translog(2, "tranId2", LocalDateTime.now(), tranType, person);
        testTranslog3 = new Translog(3, "tranId3", LocalDateTime.now(), tranType, person);
        testTranslog4 = new Translog(4, "tranId4", LocalDateTime.now(), tranType, person);

        instance = MySQLTranslogDAO.class.getDeclaredField("TRANSLOG_DAO");
        instance.setAccessible(true);
        instanceProxyPerson = ProxyPersonDAO.class.getDeclaredField("PERSON_DAO");
        instanceProxyPerson.setAccessible(true);
        instanceProxyTranType = ProxyTranTypeDAO.class.getDeclaredField("TRAN_TYPE_DAO");
        instanceProxyTranType.setAccessible(true);
        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));
        mockStatic(ConnectionFactory.class);
        connection = mock(Connection.class);

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenReturn(connection);

        instance.set(null, null);
        instanceProxyPerson.set(null, mockPersonDAO);
        instanceProxyTranType.set(null, mockTranTypeDAO);

        Mockito.reset(connection);
        when(connection, "prepareStatement", any()).thenReturn(preparedStatement);
        when(connection, "prepareStatement", any(), anyInt()).thenReturn(preparedStatement);

        testTranslog.setId(-1);
        testTranslog.setTranId("Test tranID");

        when(mockPersonDAO.findById(anyLong())).thenReturn(person);
        when(mockTranTypeDAO.findById(tranType.getId())).thenReturn(tranType);
        tranType2.setId(2);
        when(mockTranTypeDAO.findById(tranType2.getId())).thenReturn(null);

        testTranslog.setId(-1);
        testTranslog2.setId(2);
        testTranslog3.setId(3);
        testTranslog4.setId(4);
    }

    @Test
    public void getInstance() {
        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertEquals(translogDAO, MySQLTranslogDAO.getInstance());
    }

    @Test
    public void add() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(translogDAO.add(testTranslog));
        /*verify(preparedStatement).setString(1, "Test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setInt(3, 1);
        verify(preparedStatement).getGeneratedKeys();
        verify(generatedKey).getLong(1);*/
        Assert.assertEquals(testTranslog.getId(), 1);
    }

    @Test
    public void addWithErrorOfGeneratedKey() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(false);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertFalse(translogDAO.add(testTranslog));
    }

    @Test
    public void addWithErrorOfAddingInDB() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(0);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertFalse(translogDAO.add(testTranslog));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionUpdate() throws Exception {
        testTranslog.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertFalse(translogDAO.add(testTranslog));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionGetConnection() throws Exception {
        testTranslog.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertFalse(translogDAO.add(testTranslog));
    }

    @Test
    public void addAll() throws Exception {
        testTranslog.setId(-1);
        testTranslog2.setId(-1);
        testTranslog3.setId(-1);
        testTranslog4.setId(-1);

        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(10L).thenReturn(20L).thenReturn(30L).thenReturn(40L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        List<Translog> translogs = new LinkedList<>();
        translogs.add(testTranslog);
        translogs.add(testTranslog2);
        translogs.add(testTranslog3);
        translogs.add(testTranslog4);

        TranslogDAO personDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(personDAO.addAll(translogs));
        Assert.assertEquals(testTranslog.getId(), 10);
        Assert.assertEquals(testTranslog2.getId(), 20);
        Assert.assertEquals(testTranslog3.getId(), 30);
        Assert.assertEquals(testTranslog4.getId(), 40);
    }

    @Test(expected = RuntimeException.class)
    public void addAllWithException() throws Exception {
        testTranslog.setId(-1);
        testTranslog2.setId(-1);
        testTranslog3.setId(-1);
        testTranslog4.setId(-1);

        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(10L).thenReturn(20L).thenReturn(30L).thenReturn(40L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1).thenThrow(new SQLException());

        List<Translog> translogs = new LinkedList<>();
        translogs.add(testTranslog);
        translogs.add(testTranslog2);
        translogs.add(testTranslog3);
        translogs.add(testTranslog4);

        TranslogDAO personDAO = MySQLTranslogDAO.getInstance();
        personDAO.addAll(translogs);
    }

    @Test(expected = RuntimeException.class)
    public void addAllWithOneIncorrect() throws Exception {
        testTranslog.setId(-1);
        testTranslog2.setId(-1);
        testTranslog3.setId(-1);
        testTranslog4.setId(-1);

        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(10L).thenReturn(-1L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        List<Translog> translogs = new LinkedList<>();
        translogs.add(testTranslog);
        translogs.add(testTranslog2);
        translogs.add(testTranslog3);
        translogs.add(testTranslog4);

        TranslogDAO personDAO = MySQLTranslogDAO.getInstance();
        personDAO.addAll(translogs);
    }

    @Test
    public void addAllWithSkipOne() throws Exception {
        testTranslog.setId(-1);
        testTranslog2.setId(-1);
        testTranslog3.setId(-1);
        testTranslog4.setId(-1);

        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(10L).thenReturn(30L).thenReturn(40L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1).thenReturn(0).thenReturn(1);

        List<Translog> translogs = new LinkedList<>();
        translogs.add(testTranslog);
        translogs.add(testTranslog2);
        translogs.add(testTranslog3);
        translogs.add(testTranslog4);

        TranslogDAO personDAO = MySQLTranslogDAO.getInstance();
        Assert.assertFalse(personDAO.addAll(translogs));
        Assert.assertEquals(testTranslog.getId(), 10);
        Assert.assertEquals(testTranslog2.getId(), -1);
        Assert.assertEquals(testTranslog3.getId(), 30);
        Assert.assertEquals(testTranslog4.getId(), 40);
    }

    @Test
    public void removeAll() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(1);

        List<Translog> translogs = new LinkedList<>();
        translogs.add(testTranslog);
        translogs.add(testTranslog2);
        translogs.add(testTranslog3);
        translogs.add(testTranslog4);

        TranslogDAO personDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(personDAO.removeAll(translogs));
    }

    @Test
    public void removeAllThenOneTranslogAlreadyRemoved() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(1).thenReturn(0).thenReturn(1);

        List<Translog> translogs = new LinkedList<>();
        translogs.add(testTranslog);
        translogs.add(testTranslog2);
        translogs.add(testTranslog3);
        translogs.add(testTranslog4);

        TranslogDAO personDAO = MySQLTranslogDAO.getInstance();
        Assert.assertFalse(personDAO.removeAll(translogs));
    }

    @Test(expected = RuntimeException.class)
    public void removeAllWithException() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(1).thenThrow(new SQLException());

        List<Translog> translogs = new LinkedList<>();
        translogs.add(testTranslog);
        translogs.add(testTranslog2);
        translogs.add(testTranslog3);
        translogs.add(testTranslog4);

        TranslogDAO personDAO = MySQLTranslogDAO.getInstance();
       personDAO.removeAll(translogs);
    }

    @Test
    public void getLast() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);

        Person person = new Person();
        person.setAdmin(true);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "TRANSLOG_ID")
                .thenReturn(testTranslog4.getId());
        when(resultSet, "getLong", "TRANTYPE_ID")
                .thenReturn(testTranslog4.getTranType().getId());
        when(resultSet, "getLong", "PERSON_ID")
                .thenReturn(testTranslog4.getPerson().getId());
        when(resultSet, "getString", "TRAN_DATE")
                .thenReturn(testTranslog4.getTranDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER));
        when(resultSet, "getString", "TRAN_ID")
                .thenReturn(testTranslog4.getTranId());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsByContent(testTranslog4, translogDAO.getLast(person)));
    }

    @Test(expected = RuntimeException.class)
    public void getLastWithException() throws Exception {
        Person person = new Person();
        person.setAdmin(true);

        ResultSet resultSet = mock(ResultSet.class);
        when(preparedStatement, "executeQuery").thenReturn(resultSet);
        when(resultSet, "next").thenThrow(new SQLException());

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.getLast(person);
    }

    @Test
    public void getLastByGroupManager() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);

        Person person = new Person();
        person.setPosition(new Position(1, "Test", new AccessLevel(1, "Test", 1)));
        person.setGroup(new Group(1, "Test"));

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "TRANSLOG_ID")
                .thenReturn(testTranslog4.getId());
        when(resultSet, "getLong", "TRANTYPE_ID")
                .thenReturn(testTranslog4.getTranType().getId());
        when(resultSet, "getLong", "PERSON_ID")
                .thenReturn(testTranslog4.getPerson().getId());
        when(resultSet, "getString", "TRAN_DATE")
                .thenReturn(testTranslog4.getTranDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER));
        when(resultSet, "getString", "TRAN_ID")
                .thenReturn(testTranslog4.getTranId());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsByContent(testTranslog4, translogDAO.getLast(person)));
    }

    @Test
    public void getLastFromClearDB() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);

        Person person = new Person();
        person.setPosition(new Position(1, "Test", new AccessLevel(1, "Test", 1)));
        person.setGroup(new Group(1, "Test"));

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(false);
        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertNull(translogDAO.getLast(person));
    }

    @Test(expected = RuntimeException.class)
    public void getLastByEmployee() throws Exception {
        Person person = new Person();
        person.setPosition(new Position(1, "Test", new AccessLevel(1, "Test", 2)));

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.getLast(person);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void update() throws Exception {
        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.update(testTranslog);
    }

    @Test
    public void remove() throws Exception {
        testTranslog.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(translogDAO.remove(testTranslog));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test
    public void removeWithErrorInDB() throws Exception {
        testTranslog.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertFalse(translogDAO.remove(testTranslog));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionUpdate() throws Exception {
        testTranslog.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertFalse(translogDAO.remove(testTranslog));
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionGetConnection() throws Exception {
        testTranslog.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertFalse(translogDAO.remove(testTranslog));
    }

    @Test
    public void getAll() throws Exception {
        testTranslog.setId(1);
        List<Translog> translogList = new LinkedList<>();
        translogList.add(testTranslog);
        translogList.add(testTranslog2);
        translogList.add(testTranslog3);
        translogList.add(testTranslog4);

        init4Translog();

        Person person = new Person();
        person.setAdmin(true);
        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getAll(person), translogList));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithException() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenThrow(new SQLException());
        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Person person = new Person();
        person.setAdmin(true);
        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.getAll(person);
    }

    @Test
    public void getAllWithOneNull() throws Exception {
        testTranslog.setId(1);
        testTranslog3.setTranType(tranType2);

        List<Translog> translogList = new LinkedList<>();
        translogList.add(testTranslog);
        translogList.add(testTranslog2);
        translogList.add(testTranslog4);

        init4Translog();

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();

        Person person = new Person();
        person.setAdmin(true);
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getAll(person), translogList));
    }

    @Test
    public void getAllForGroupManager() throws Exception {
        testTranslog.setId(1);
        List<Translog> translogList = new LinkedList<>();
        translogList.add(testTranslog);
        translogList.add(testTranslog2);
        translogList.add(testTranslog3);
        translogList.add(testTranslog4);

        init4Translog();

        Person person = new Person();
        person.setPosition(new Position(1, "Test", new AccessLevel(1, "Test", 1)));
        person.setGroup(new Group(1, "Test"));

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getAll(person), translogList));
    }

    @Test
    public void getAllByDates() throws Exception {
        testTranslog.setId(1);
        List<Translog> translogList = new LinkedList<>();
        translogList.add(testTranslog);
        translogList.add(testTranslog2);
        translogList.add(testTranslog3);
        translogList.add(testTranslog4);

        init4Translog();

        Person person = new Person();
        person.setAdmin(true);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getAllByDates(person, LocalDateTime.now(), LocalDateTime.now()), translogList));
    }

    @Test(expected = RuntimeException.class)
    public void getAllByDatesWithException() throws Exception {
        testTranslog.setId(1);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenThrow(new SQLException());
        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Person person = new Person();
        person.setAdmin(true);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.getAllByDates(person, LocalDateTime.now(), LocalDateTime.now());
    }

    @Test
    public void getAllByDatesWithOneNull() throws Exception {
        testTranslog.setId(1);
        testTranslog3.setTranType(tranType2);

        List<Translog> translogList = new LinkedList<>();
        translogList.add(testTranslog);
        translogList.add(testTranslog2);
        translogList.add(testTranslog4);

        init4Translog();

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();

        Person person = new Person();
        person.setAdmin(true);
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getAllByDates(person, LocalDateTime.now(), LocalDateTime.now()), translogList));
    }

    @Test
    public void getAllByDatesForGroupManager() throws Exception {
        testTranslog.setId(1);
        List<Translog> translogList = new LinkedList<>();
        translogList.add(testTranslog);
        translogList.add(testTranslog2);
        translogList.add(testTranslog3);
        translogList.add(testTranslog4);

        init4Translog();

        Person person = new Person();
        person.setPosition(new Position(1, "Test", new AccessLevel(1, "Test", 1)));
        person.setGroup(new Group(1, "Test"));

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getAllByDates(person, LocalDateTime.now(), LocalDateTime.now()), translogList));
    }

    @Test
    public void getAllByDatesForEmployee() throws Exception {
        testTranslog.setId(1);
        List<Translog> translogList = new LinkedList<>();
        translogList.add(testTranslog);
        translogList.add(testTranslog2);
        translogList.add(testTranslog3);
        translogList.add(testTranslog4);

        init4Translog();

        Person person = new Person();
        person.setPosition(new Position(1, "Test", new AccessLevel(1, "Test", 2)));

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getAllByDates(person, LocalDateTime.now(), LocalDateTime.now()), translogList));
    }

    @Test
    public void getPersonTranslogsByDates() throws Exception {
        testTranslog.setId(1);
        List<Translog> translogList = new LinkedList<>();
        translogList.add(testTranslog);
        translogList.add(testTranslog2);
        translogList.add(testTranslog3);
        translogList.add(testTranslog4);

        init4Translog();

        Person person = new Person();

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getPersonTranslogsByDates(person, LocalDateTime.now(), LocalDateTime.now()), translogList));
    }

    @Test
    public void getPersonTranslogsByDatesEmpty() throws Exception {
        testTranslog.setId(1);

        init4Translog();

        Person person = new Person();
        person.setId(2);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getPersonTranslogsByDates(person, LocalDateTime.now(), LocalDateTime.now()), Collections.emptyList()));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithIllegalAccess() throws Exception {
        testTranslog.setId(1);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();

        Person person = new Person();
        translogDAO.getAll(person);
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionQuery() throws Exception {
        testTranslog.setId(1);

        when(preparedStatement, "executeQuery").thenThrow(new SQLException());

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.getAll();
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithGetConnectionException() throws Exception {
        testTranslog.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.getAll();
    }

    @Test
    public void getAllByPerson() throws Exception {
        testTranslog.setId(1);
        List<Translog> translogList = new LinkedList<>();
        translogList.add(testTranslog);
        translogList.add(testTranslog2);
        translogList.add(testTranslog3);
        translogList.add(testTranslog4);

        init4Translog();

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Person ADMIN = new Person();
        ADMIN.setAdmin(true);
        Assert.assertTrue(assertEqualsListByContent(translogDAO.getAll(ADMIN), translogList));
    }


    @Test
    public void findById() throws Exception {
        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        Translog testTranslog3 = new Translog(3, "tranId3", LocalDateTime.now(), tranType, person);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true);
        when(resultSet, "getLong", "TRANSLOG_ID")
                .thenReturn(testTranslog3.getId());
        when(resultSet, "getLong", "TRANTYPE_ID")
                .thenReturn(testTranslog3.getTranType().getId());
        when(resultSet, "getLong", "PERSON_ID")
                .thenReturn(testTranslog3.getPerson().getId());
        when(resultSet, "getString", "TRAN_DATE")
                .thenReturn(testTranslog3.getTranDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER));
        when(resultSet, "getString", "TRAN_ID")
                .thenReturn(testTranslog3.getTranId());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Assert.assertTrue(assertEqualsByContent(translogDAO.findById(3), testTranslog3));
    }

    @Test
    public void getLastByPersonAndType() throws Exception {
        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true);
        when(resultSet, "getLong", "TRANSLOG_ID")
                .thenReturn(testTranslog3.getId());
        when(resultSet, "getLong", "TRANTYPE_ID")
                .thenReturn(testTranslog3.getTranType().getId());
        when(resultSet, "getLong", "PERSON_ID")
                .thenReturn(testTranslog3.getPerson().getId());
        when(resultSet, "getString", "TRAN_DATE")
                .thenReturn(testTranslog3.getTranDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER));
        when(resultSet, "getString", "TRAN_ID")
                .thenReturn(testTranslog3.getTranId());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Assert.assertTrue(assertEqualsByContent(translogDAO.getLastByPersonAndType(person, tranType), testTranslog3));
    }

    @Test(expected = RuntimeException.class)
    public void getLastByPersonAndTypeWithException() throws Exception {
        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenThrow(new SQLException());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        translogDAO.getLastByPersonAndType(person, tranType);
    }

    @Test
    public void getLastByPersonAndTypeWhenNotExist() throws Exception {
        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(false);
        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Assert.assertNull(translogDAO.getLastByPersonAndType(person, tranType));
    }

    @Test
    public void contains() throws Exception {
        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        testTranslog.setId(1);

        init4Translog();
        Translog translog = new Translog();
        translog.setId(5);

        Assert.assertTrue(translogDAO.contains(testTranslog));
        Assert.assertTrue(translogDAO.contains(testTranslog2));
        Assert.assertTrue(translogDAO.contains(testTranslog3));
        Assert.assertTrue(translogDAO.contains(testTranslog4));
        Assert.assertFalse(translogDAO.contains(translog));
    }

    @Test(expected = RuntimeException.class)
    public void addWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.add(testTranslog);
    }

    @Test(expected = RuntimeException.class)
    public void getAllByDatesWithNullConFactory() throws IllegalAccessException {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.getAllByDates(person, null, null);
    }

    @Test(expected = RuntimeException.class)
    public void getAllByPersonWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.getAll(person);
    }

    @Test(expected = RuntimeException.class)
    public void addAllWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.addAll(Collections.emptyList());
    }

    @Test(expected = RuntimeException.class)
    public void getLastWithNullConFactory() throws IllegalAccessException {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.getLast(person);
    }

    @Test(expected = RuntimeException.class)
    public void getLastByPersonAndTypeWithNullConFactory() throws IllegalAccessException {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.getLastByPersonAndType(person, tranType);
    }

    @Test(expected = RuntimeException.class)
    public void removeAllWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.removeAll(Collections.emptyList());
    }

    @Test(expected = RuntimeException.class)
    public void removeWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.remove(testTranslog);
    }

    @Test(expected = RuntimeException.class)
    public void updateWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        TranslogDAO translogDAO = MySQLTranslogDAO.getInstance();
        translogDAO.update(testTranslog);
    }

    static boolean assertEqualsListByContent(List<Translog> firstList, List<Translog> secondList) {
        if(firstList.size() != secondList.size())
            return false;
        for(int i = 0, size = firstList.size(); i < size; i++)
            if(!assertEqualsByContent(firstList.get(i), secondList.get(i)))
                return false;
        return true;
    }

    static boolean assertEqualsByContent(Translog translog, Translog translog2) {
        return translog.getId() == translog2.getId();
    }

    void init4Translog() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "TRANSLOG_ID")
                .thenReturn(testTranslog.getId())
                .thenReturn(testTranslog2.getId())
                .thenReturn(testTranslog3.getId())
                .thenReturn(testTranslog4.getId())
                .thenReturn(-1L);
        when(resultSet, "getLong", "TRANTYPE_ID")
                .thenReturn(testTranslog.getTranType().getId())
                .thenReturn(testTranslog2.getTranType().getId())
                .thenReturn(testTranslog3.getTranType().getId())
                .thenReturn(testTranslog4.getTranType().getId())
                .thenReturn(-1L);
        when(resultSet, "getLong", "PERSON_ID")
                .thenReturn(testTranslog.getPerson().getId())
                .thenReturn(testTranslog2.getPerson().getId())
                .thenReturn(testTranslog3.getPerson().getId())
                .thenReturn(testTranslog4.getPerson().getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "TRAN_DATE")
                .thenReturn(testTranslog.getTranDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER))
                .thenReturn(testTranslog2.getTranDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER))
                .thenReturn(testTranslog3.getTranDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER))
                .thenReturn(testTranslog4.getTranDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER))
                .thenReturn(null);
        when(resultSet, "getString", "TRAN_ID")
                .thenReturn(testTranslog.getTranId())
                .thenReturn(testTranslog2.getTranId())
                .thenReturn(testTranslog3.getTranId())
                .thenReturn(testTranslog4.getTranId())
                .thenReturn(null);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);
    }
}