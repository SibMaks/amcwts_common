package com.fxbank.amcwts.conf;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.google.gson.Gson;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;

import static org.mockito.Mockito.spy;
import static org.powermock.api.mockito.PowerMockito.*;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 04.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Gson.class, SystemConfig.class, ConnectionFactory.class})
public class ConfigWorkerTest {
    private Field instance;
    private File spyConfFile;

    public ConfigWorkerTest() throws NoSuchFieldException, IOException {
        instance = ConfigWorker.class.getDeclaredField("instance");
        instance.setAccessible(true);
        File defaultConfFile = new File(getClass().getResource("/default.conf.json").getFile());
        spyConfFile = spy(new File("test.conf.json"));
        if(spyConfFile.exists())
            spyConfFile.delete();
        Files.copy(defaultConfFile.toPath(), spyConfFile.toPath());
    }

    @Before
    public void before() throws Exception {
        instance.set(null, null);
        when(spyConfFile.exists()).thenReturn(true);
        mockStatic(ConnectionFactory.class);

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
    }

    @After
    public void after() {
        if(spyConfFile.exists())
            spyConfFile.delete();
    }

    @Test
    public void init() throws Exception {
        ConfigWorker.getInstance().init(spyConfFile);
    }

    @Test
    public void testGetSystemConfiguration() throws Exception {
        ConfigWorker.getInstance().init(spyConfFile);
        SystemConfig systemConfig = ConfigWorker.getInstance().getSystemConfig();

        Assert.assertEquals(systemConfig.getAdminLogin(), "admin");
        Assert.assertEquals(systemConfig.getAdminPassword(), "admin");
        Assert.assertEquals(systemConfig.getCurrentDB(), "");
        Assert.assertTrue(systemConfig.isAdminActive());
        Assert.assertFalse(systemConfig.isServiceOn());
    }

    @Test(expected = IllegalStateException.class)
    public void testGetSystemConfigurationWithException() throws Exception {
        ConfigWorker.getInstance().getSystemConfig();
    }

    @Test(expected = IllegalArgumentException.class)
    public void initNotExistConfFile() throws Exception {
        ConfigWorker.getInstance().init(new File("not-exist-file_.txt"));
    }

    @Test(expected = RuntimeException.class)
    public void initRemovedConfFile() throws Exception {
        spyConfFile.delete();
        ConfigWorker.getInstance().init(spyConfFile);
    }

    @Test(expected = RuntimeException.class)
    public void initCorruptedConfFile() throws Exception {
        ConfigWorker.getInstance().init(new File(getClass().getResource("/corrupted.test.conf.json").getFile()));
    }

    @Test
    public void update() {
        ConfigWorker.getInstance().init(spyConfFile);

        DBDescription dbDescription = new DBDescription("MySQL", "login", "password", "connectionString", "driver");

        SystemConfig systemConfig = ConfigWorker.getInstance().getSystemConfig();
        systemConfig.setAdminActive(false);
        systemConfig.setAdminLogin("login");
        systemConfig.setAdminPassword("password");
        systemConfig.setCurrentDB("MySQL");
        systemConfig.setServiceOn(true);
        systemConfig.setSupportedBd(new DBDescription[]{dbDescription});

        ConfigWorker.getInstance().update();

        ConfigWorker.getInstance().init(spyConfFile);
        SystemConfig systemConfigTest = ConfigWorker.getInstance().getSystemConfig();

        Assert.assertEquals(systemConfig.getCurrentDB(), systemConfigTest.getCurrentDB());
    }

    @Test(expected = RuntimeException.class)
    public void updateWhenConnectionFactoryNull() {
        ConfigWorker.getInstance().init(spyConfFile);

        when(ConnectionFactory.getInstance()).thenReturn(null);

        ConfigWorker.getInstance().update();
    }

    @Test(expected = IllegalStateException.class)
    public void updateBeforeInit() {
        ConfigWorker.getInstance().update();
    }
}