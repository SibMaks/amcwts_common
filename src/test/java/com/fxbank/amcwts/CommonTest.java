package com.fxbank.amcwts;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.reset;
import static org.powermock.api.mockito.PowerMockito.*;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 10.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class CommonTest {
    private Field instance;
    private CommonDAO<String> testCommonDAO;
    private ConnectionFactory mockConnectionFactory;
    private Connection mockConnection;

    public CommonTest() throws NoSuchFieldException {
        instance = ConnectionFactory.class.getDeclaredField("connectionFactory");
        mockConnectionFactory = mock(ConnectionFactory.class);
        mockConnection = mock(Connection.class);
        testCommonDAO = new CommonDAO<String>("TEST") {
            @Override
            public List<String> getAll(Person person) {
                List<String> strings = new LinkedList<>();
                strings.add("Test");
                return strings;
            }

            @Override
            public String findById(long id) {
                return "Test";
            }

            @Override
            public boolean contains(String value) {
                return true;
            }

            @Override
            public boolean add(String value) {
                return true;
            }

            @Override
            public boolean remove(String value) {
                return true;
            }

            @Override
            public boolean update(String value) {
                return true;
            }

            @Override
            protected String tryCreate(ResultSet resultSet) throws SQLException {
                return resultSet.first() ? "Test" : null;
            }
        };
    }

    public static final Person testPerson = new Person();

    @Before
    public void setup() throws IllegalAccessException, SQLException {
        instance.setAccessible(true);
        instance.set(null, null);

        reset(mockConnection, mockConnectionFactory);
        mockStatic(ConnectionFactory.class);

        when(ConnectionFactory.getInstance()).thenReturn(mockConnectionFactory);
        when(mockConnectionFactory.getConnection()).thenReturn(mockConnection);
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        testCommonDAO.getAll();
    }

    @Test(expected = RuntimeException.class)
    public void findWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        testCommonDAO.find("Test", "Test");
    }

    @Test
    public void find() throws Exception {
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "first").thenReturn(true).thenReturn(false);
        when(preparedStatement, "executeQuery").thenReturn(resultSet);
        when(mockConnection, "prepareStatement", anyString()).thenReturn(preparedStatement);
        String strings = testCommonDAO.find("Test", "Test");
        Assert.assertFalse(strings.isEmpty());
    }

    @Test
    public void findWithNull() throws Exception {
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(false);
        when(preparedStatement, "executeQuery").thenReturn(resultSet);
        when(mockConnection, "prepareStatement", anyString()).thenReturn(preparedStatement);
        String strings = testCommonDAO.find("Test", "Test");
        Assert.assertNull(strings);
    }

    @Test
    public void getAll() throws Exception {
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "first").thenReturn(true).thenReturn(false);
        when(preparedStatement, "executeQuery").thenReturn(resultSet);
        when(mockConnection, "prepareStatement", anyString()).thenReturn(preparedStatement);
        List<String> strings = testCommonDAO.getAll();
        Assert.assertFalse(strings.isEmpty());
        Assert.assertEquals(strings.get(0), "Test");
    }

    @Test
    public void makeException() {
        Assert.assertEquals(testCommonDAO.makeException("Title %s", "Message").getMessage(), "Title Message");
    }

    @Test
    public void makeException2Exception() {
        Assert.assertEquals(testCommonDAO.makeException("Title %s", new NullPointerException("Message")).getMessage(), "Title Message");
    }

    @Test
    public void makeException2ExceptionWithNull() {
        Assert.assertEquals(testCommonDAO.makeException("Title %s", (Exception) null).getMessage(), "Title ");
    }

    @Test
    public void makeExceptionNull() {
        Assert.assertEquals(testCommonDAO.makeException("Title %s", (String) null).getMessage(), "Title ");
    }

    public static <T> void testOnDefault(CommonDAO<T> testingDAO, T testedValue, List<T> testAnswerList, boolean testAnswer, T testFindAnswer) {
        Assert.assertEquals(testingDAO.getAll(), testAnswerList);

        Assert.assertEquals(testingDAO.getAll(testPerson), testAnswerList);

        Assert.assertEquals(testingDAO.add(testedValue), testAnswer);

        Assert.assertEquals(testingDAO.remove(testedValue), testAnswer);

        Assert.assertEquals(testingDAO.update(testedValue), testAnswer);

        Assert.assertEquals(testingDAO.contains(testedValue), testAnswer);

        Assert.assertEquals(testingDAO.findById(0), testFindAnswer);
    }
}
