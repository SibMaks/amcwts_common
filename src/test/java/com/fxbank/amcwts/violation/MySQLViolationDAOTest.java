package com.fxbank.amcwts.violation;

import com.fxbank.amcwts.CommonDAO;
import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.person.PersonDAO;
import com.fxbank.amcwts.person.ProxyPersonDAO;
import com.fxbank.amcwts.person.accesslevel.AccessLevel;
import com.fxbank.amcwts.person.group.Group;
import com.fxbank.amcwts.person.position.Position;
import com.fxbank.amcwts.person.worktime.ProxyWorkTimeDAO;
import com.fxbank.amcwts.person.worktime.WorkTime;
import com.fxbank.amcwts.person.worktime.WorkTimeDAO;
import com.fxbank.amcwts.violation.violationtype.ProxyViolationTypeDAO;
import com.fxbank.amcwts.violation.violationtype.ViolationType;
import com.fxbank.amcwts.violation.violationtype.ViolationTypeDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 17.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class MySQLViolationDAOTest {
    private Field instance;
    private Field instanceProxyPerson;
    private Field instanceProxyWorkTime;
    private Field instanceProxyViolationType;
    private Connection connection;
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);
    private Person person = new Person();
    private ViolationType violationType = new ViolationType();
    private ViolationType violationType2 = new ViolationType();
    private WorkTime workTime = new WorkTime(1, LocalTime.of(9, 0), LocalTime.of(18, 0));
    private Violation testViolation;
    private Violation testViolation2;
    private Violation testViolation3;
    private Violation testViolation4;

    public MySQLViolationDAOTest() throws SQLException, NoSuchFieldException {
        instance = MySQLViolationDAO.class.getDeclaredField("VIOLATION_DAO");
        instance.setAccessible(true);
        instanceProxyPerson = ProxyPersonDAO.class.getDeclaredField("PERSON_DAO");
        instanceProxyPerson.setAccessible(true);
        instanceProxyWorkTime = ProxyWorkTimeDAO.class.getDeclaredField("WORK_TIME_DAO");
        instanceProxyWorkTime.setAccessible(true);
        instanceProxyViolationType = ProxyViolationTypeDAO.class.getDeclaredField("VIOLATION_TYPE_DAO");
        instanceProxyViolationType.setAccessible(true);
        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));
        mockStatic(ConnectionFactory.class);
        connection = mock(Connection.class);

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenReturn(connection);
    }

    @Before
    public void setup() throws Exception {
        PersonDAO mockPersonDAO = mock(PersonDAO.class);
        WorkTimeDAO mockWorkTimeDAO = mock(WorkTimeDAO.class);
        ViolationTypeDAO mockViolationTypeDAO = mock(ViolationTypeDAO.class);

        testViolation = new Violation(-1, "Reason", violationType, LocalDateTime.now(), workTime, person);
        testViolation2 = new Violation(2, "Reason", violationType, LocalDateTime.now(), workTime, person);
        testViolation3 = new Violation(3, "Reason", violationType, LocalDateTime.now(), workTime, person);
        testViolation4 = new Violation(4, "Reason", violationType, LocalDateTime.now(), workTime, person);

        instance.set(null, null);
        instanceProxyPerson.set(null, mockPersonDAO);
        instanceProxyWorkTime.set(null, mockWorkTimeDAO);
        instanceProxyViolationType.set(null, mockViolationTypeDAO);

        Mockito.reset(connection);
        Mockito.reset(preparedStatement);
        when(connection, "prepareStatement", any()).thenReturn(preparedStatement);
        when(connection, "prepareStatement", any(), anyInt()).thenReturn(preparedStatement);

        violationType.setId(1);
        violationType2.setId(2);

        when(mockPersonDAO.findById(anyLong())).thenReturn(person);
        when(mockViolationTypeDAO.findById(violationType.getId())).thenReturn(violationType);
        when(mockViolationTypeDAO.findById(violationType2.getId())).thenReturn(null);
        when(mockWorkTimeDAO.findById(anyLong())).thenReturn(workTime);
    }

    @Test
    public void add() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertTrue(violationDAO.add(testViolation));
        /*verify(preparedStatement).setString(1, "Test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setInt(3, 1);
        verify(preparedStatement).getGeneratedKeys();
        verify(generatedKey).getLong(1);*/
        Assert.assertEquals(testViolation.getId(), 1);
    }

    @Test
    public void addWithErrorOfGeneratedKey() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(false);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertFalse(violationDAO.add(testViolation));
    }

    @Test
    public void addWithErrorOfAddingInDB() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(0);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertFalse(violationDAO.add(testViolation));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionUpdate() throws Exception {
        testViolation.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertFalse(violationDAO.add(testViolation));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionGetConnection() throws Exception {
        testViolation.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertFalse(violationDAO.add(testViolation));
    }

    @Test
    public void update() throws Exception {
        add();
        testViolation.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertTrue(violationDAO.update(testViolation));
        /*verify(preparedStatement).setString(1, "New test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setLong(3, 1);*/
    }

    @Test
    public void updateWithUpdateErrorInDB() throws Exception {
        testViolation.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertFalse(violationDAO.update(testViolation));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionUpdate() throws Exception {
        testViolation.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertFalse(violationDAO.update(testViolation));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionGetConnection() throws Exception {
        testViolation.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertFalse(violationDAO.update(testViolation));
    }

    @Test
    public void remove() throws Exception {
        testViolation.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertTrue(violationDAO.remove(testViolation));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test
    public void removeWithErrorInDB() throws Exception {
        testViolation.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertFalse(violationDAO.remove(testViolation));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionUpdate() throws Exception {
        testViolation.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertFalse(violationDAO.remove(testViolation));
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionGetConnection() throws Exception {
        testViolation.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertFalse(violationDAO.remove(testViolation));
    }

    @Test
    public void getAll() throws Exception {
        testViolation.setId(1);
        List<Violation> violationList = new LinkedList<>();
        violationList.add(testViolation);
        violationList.add(testViolation2);
        violationList.add(testViolation3);
        violationList.add(testViolation4);

        init4Violation();

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(violationDAO.getAll(), violationList));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithException() throws Exception {
        testViolation.setId(1);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenThrow(new SQLException());
        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.getAll(person);
    }

    @Test
    public void getAllForGroupManager() throws Exception {
        testViolation.setId(1);
        List<Violation> violationList = new LinkedList<>();
        violationList.add(testViolation);
        violationList.add(testViolation2);
        violationList.add(testViolation3);
        violationList.add(testViolation4);

        init4Violation();
        Person testPerson = new Person();
        testPerson.setGroup(new Group(1, "Test"));
        testPerson.setPosition(new Position(1, "test", new AccessLevel(1, "Test", 1)));

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(violationDAO.getAll(testPerson), violationList));
    }

    @Test
    public void getAllForEmployee() throws Exception {
        testViolation.setId(1);
        List<Violation> violationList = new LinkedList<>();
        violationList.add(testViolation);
        violationList.add(testViolation2);
        violationList.add(testViolation3);
        violationList.add(testViolation4);

        init4Violation();
        Person testPerson = new Person();
        testPerson.setPosition(new Position(1, "test", new AccessLevel(1, "Test", 2)));

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(violationDAO.getAll(testPerson), violationList));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionQuery() throws Exception {
        testViolation.setId(1);

        when(preparedStatement, "executeQuery").thenThrow(new SQLException());

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.getAll();
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithGetConnectionException() throws Exception {
        testViolation.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.getAll();
    }

    @Test
    public void getAllByPerson() throws Exception {
        testViolation.setId(1);
        List<Violation> violationList = new LinkedList<>();
        violationList.add(testViolation);
        violationList.add(testViolation2);
        violationList.add(testViolation3);
        violationList.add(testViolation4);

        init4Violation();

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Person ADMIN = new Person();
        ADMIN.setAdmin(true);
        Assert.assertTrue(assertEqualsListByContent(violationDAO.getAll(ADMIN), violationList));
    }


    @Test
    public void findById() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L).thenReturn(2L).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        testViolation.setId(1);
        violationDAO.add(testViolation);
        violationDAO.add(testViolation2);
        violationDAO.add(testViolation4);

        init4Violation();

        Assert.assertTrue(assertEqualsByContent(violationDAO.findById(1), testViolation));
        Assert.assertTrue(assertEqualsByContent(violationDAO.findById(2), testViolation2));
        Assert.assertTrue(assertEqualsByContent(violationDAO.findById(3), testViolation3));
        Assert.assertTrue(assertEqualsByContent(violationDAO.findById(4), testViolation4));
    }

    @Test(expected = RuntimeException.class)
    public void addWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.add(testViolation);
    }

    @Test(expected = RuntimeException.class)
    public void removeWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.remove(testViolation);
    }

    @Test(expected = RuntimeException.class)
    public void updateWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.update(testViolation);
    }

    @Test(expected = RuntimeException.class)
    public void getAllByPeronWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.getAll(null);
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.getAll();
    }

    @Test(expected = RuntimeException.class)
    public void getLastWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.getLast(null);
    }

    @Test(expected = RuntimeException.class)
    public void containsWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.contains(testViolation);
    }

    @Test(expected = RuntimeException.class)
    public void getAllByDatesWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.getAllByDates(null, null, null);
    }

    @Test(expected = RuntimeException.class)
    public void getByDatesForPersonWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.getByDatesForPerson(null, null, null);
    }

    @Test
    public void getLast() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        Person person = new Person();
        person.setAdmin(true);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "VIOLATION_id")
                .thenReturn(testViolation4.getId());
        when(resultSet, "getString", "VIOLATION_REASON")
                .thenReturn(testViolation4.getReason());
        when(resultSet, "getLong", "VIOLATION_TYPE_ID")
                .thenReturn(testViolation4.getViolationType().getId());
        when(resultSet, "getString", "DATE")
                .thenReturn(testViolation4.getDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER));
        when(resultSet, "getLong", "WORK_TIME_id")
                .thenReturn(testViolation4.getWorkTime().getId());
        when(resultSet, "getLong", "PERSON_ID")
                .thenReturn(testViolation4.getPerson().getId());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertTrue(assertEqualsByContent(testViolation4, violationDAO.getLast(person)));
    }

    @Test
    public void getLastNull() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        Person person = new Person();
        person.setAdmin(true);

        testViolation4.setViolationType(violationType2);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "VIOLATION_id")
                .thenReturn(testViolation4.getId());
        when(resultSet, "getString", "VIOLATION_REASON")
                .thenReturn(testViolation4.getReason());
        when(resultSet, "getLong", "VIOLATION_TYPE_ID")
                .thenReturn(testViolation4.getViolationType().getId());
        when(resultSet, "getString", "DATE")
                .thenReturn(testViolation4.getDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER));
        when(resultSet, "getLong", "WORK_TIME_id")
                .thenReturn(testViolation4.getWorkTime().getId());
        when(resultSet, "getLong", "PERSON_ID")
                .thenReturn(testViolation4.getPerson().getId());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertNull(violationDAO.getLast(person));
    }

    @Test(expected = RuntimeException.class)
    public void getLastWithException() throws Exception {
        Person person = new Person();
        person.setAdmin(true);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenThrow(new SQLException());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.getLast(person);
    }

    @Test
    public void getLastNoAdmin() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        Person person = new Person();

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "VIOLATION_id")
                .thenReturn(testViolation4.getId());
        when(resultSet, "getString", "VIOLATION_REASON")
                .thenReturn(testViolation4.getReason());
        when(resultSet, "getLong", "VIOLATION_TYPE_ID")
                .thenReturn(testViolation4.getViolationType().getId());
        when(resultSet, "getString", "DATE")
                .thenReturn(testViolation4.getDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER));
        when(resultSet, "getLong", "WORK_TIME_id")
                .thenReturn(testViolation4.getWorkTime().getId());
        when(resultSet, "getLong", "PERSON_ID")
                .thenReturn(testViolation4.getPerson().getId());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        person.setPosition(new Position(1, "Test", new AccessLevel(1, "Test", 2)));
        Assert.assertTrue(assertEqualsByContent(testViolation4, violationDAO.getLast(person)));
    }

    @Test
    public void getAllByDates() throws Exception {
        init4Violation();
        List<Violation> violations = new LinkedList<>();
        violations.add(testViolation);//
        violations.add(testViolation2);
        violations.add(testViolation3);
        violations.add(testViolation4);

        Person testPerson = new Person();
        testPerson.setAdmin(true);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(violationDAO.getAllByDates(testPerson, LocalDateTime.now(), LocalDateTime.now()), violations));
    }

    @Test
    public void getByDatesForPerson() throws Exception {
        init4Violation();
        List<Violation> violations = new LinkedList<>();
        violations.add(testViolation);
        violations.add(testViolation2);
        violations.add(testViolation3);
        violations.add(testViolation4);

        Person testPerson = new Person();
        testPerson.setAdmin(true);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(violationDAO.getByDatesForPerson(testPerson, LocalDateTime.now(), LocalDateTime.now()), violations));
    }

    @Test(expected = RuntimeException.class)
    public void getByDatesForPersonWithException() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenThrow(new SQLException());
        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Person testPerson = new Person();
        testPerson.setAdmin(true);

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        violationDAO.getByDatesForPerson(testPerson, LocalDateTime.now(), LocalDateTime.now());
    }

    @Test
    public void getAllByDatesForGroupManager() throws Exception {
        init4Violation();
        List<Violation> violations = new LinkedList<>();
        violations.add(testViolation);
        violations.add(testViolation2);
        violations.add(testViolation3);
        violations.add(testViolation4);

        Person testPerson = new Person();
        testPerson.setGroup(new Group(1, "Test"));
        testPerson.setPosition(new Position(1, "test", new AccessLevel(1, "Test", 1)));

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(violationDAO.getAllByDates(testPerson, LocalDateTime.now(), LocalDateTime.now()), violations));
    }

    @Test
    public void getAllByDatesForEmployee() throws Exception {
        init4Violation();
        List<Violation> violations = new LinkedList<>();
        violations.add(testViolation);
        violations.add(testViolation2);
        violations.add(testViolation3);
        violations.add(testViolation4);

        Person testPerson = new Person();
        testPerson.setPosition(new Position(1, "test", new AccessLevel(1, "Test", 2)));

        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(violationDAO.getAllByDates(testPerson, LocalDateTime.now(), LocalDateTime.now()), violations));
    }

    @Test
    public void contains() throws Exception {
        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        testViolation.setId(1);

        init4Violation();

        Assert.assertTrue(violationDAO.contains(testViolation));
        Assert.assertTrue(violationDAO.contains(testViolation2));
        Assert.assertTrue(violationDAO.contains(testViolation3));
        Assert.assertTrue(violationDAO.contains(testViolation4));
    }

    @Test(expected = RuntimeException.class)
    public void containsWithException() throws Exception {
        ViolationDAO violationDAO = MySQLViolationDAO.getInstance();
        testViolation.setId(1);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenThrow(new SQLException());
        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        violationDAO.contains(testViolation);
    }

    static boolean assertEqualsListByContent(List<Violation> firstList, List<Violation> secondList) {
        if(firstList.size() != secondList.size())
            return false;
        for(int i = 0, size = firstList.size(); i < size; i++)
            if(!assertEqualsByContent(firstList.get(i), secondList.get(i)))
                return false;
        return true;
    }

    static boolean assertEqualsByContent(Violation violation, Violation violation2) {
        return violation != null && violation2 != null ? violation.getId() == violation2.getId() : violation == null && violation2 == null;
    }

    void init4Violation() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L).thenReturn(2L).thenReturn(3L).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "VIOLATION_id")
                .thenReturn(testViolation.getId())
                .thenReturn(testViolation2.getId())
                .thenReturn(testViolation3.getId())
                .thenReturn(testViolation4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "VIOLATION_REASON")
                .thenReturn(testViolation.getReason())
                .thenReturn(testViolation2.getReason())
                .thenReturn(testViolation3.getReason())
                .thenReturn(testViolation4.getReason())
                .thenReturn(null);
        when(resultSet, "getLong", "VIOLATION_TYPE_ID")
                .thenReturn(testViolation.getViolationType().getId())
                .thenReturn(testViolation2.getViolationType().getId())
                .thenReturn(testViolation3.getViolationType().getId())
                .thenReturn(testViolation4.getViolationType().getId())
                .thenReturn(1L);
        when(resultSet, "getString", "DATE")
                .thenReturn(testViolation.getDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER))
                .thenReturn(testViolation2.getDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER))
                .thenReturn(testViolation3.getDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER))
                .thenReturn(testViolation4.getDate().format(CommonDAO.SERVICE_DATE_TIME_FORMATTER))
                .thenReturn(null);
        when(resultSet, "getLong", "WORK_TIME_id")
                .thenReturn(testViolation.getWorkTime().getId())
                .thenReturn(testViolation2.getWorkTime().getId())
                .thenReturn(testViolation3.getWorkTime().getId())
                .thenReturn(testViolation4.getWorkTime().getId())
                .thenReturn(1L);
        when(resultSet, "getLong", "PERSON_ID")
                .thenReturn(testViolation.getPerson().getId())
                .thenReturn(testViolation2.getPerson().getId())
                .thenReturn(testViolation3.getPerson().getId())
                .thenReturn(testViolation4.getPerson().getId())
                .thenReturn(1L);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);
    }
}