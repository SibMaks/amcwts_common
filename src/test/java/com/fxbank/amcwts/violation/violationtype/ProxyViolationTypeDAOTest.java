package com.fxbank.amcwts.violation.violationtype;

import com.fxbank.amcwts.CommonTest;
import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, MySQLViolationTypeDAO.class})
public class ProxyViolationTypeDAOTest {
    private Field instance;
    private SystemConfig systemConfig;
    private ViolationTypeDAO mockViolationTypeDAO;
    private ViolationType testViolationType;

    public ProxyViolationTypeDAOTest() throws NoSuchFieldException {
        instance = ProxyViolationTypeDAO.class.getDeclaredField("VIOLATION_TYPE_DAO");
        systemConfig = mock(SystemConfig.class);
        mockViolationTypeDAO = mock(MySQLViolationTypeDAO.class);
        testViolationType = new ViolationType();
    }

    @Before
    public void setUp() throws Exception {
        instance.setAccessible(true);
        instance.set(null, null);

        Mockito.reset(systemConfig);
        Mockito.reset(mockViolationTypeDAO);

        PowerMockito.mockStatic(ConfigWorker.class);
        PowerMockito.mockStatic(MySQLViolationTypeDAO.class);

        ConfigWorker configWorker = PowerMockito.mock(ConfigWorker.class);
        PowerMockito.when(configWorker.getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(ConfigWorker.getInstance()).thenReturn(configWorker);

        when(ConfigWorker.getInstance().getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(MySQLViolationTypeDAO.getInstance()).thenReturn(mockViolationTypeDAO);
    }

    @Test
    public void currentDBIsCorrect() throws Exception {
        PowerMockito.when(mockViolationTypeDAO.getAll()).thenReturn(Collections.emptyList());
        PowerMockito.when(mockViolationTypeDAO.getAll(CommonTest.testPerson)).thenReturn(Collections.emptyList());
        PowerMockito.when(mockViolationTypeDAO.add(testViolationType)).thenReturn(false);
        PowerMockito.when(mockViolationTypeDAO.remove(testViolationType)).thenReturn(false);
        PowerMockito.when(mockViolationTypeDAO.update(testViolationType)).thenReturn(false);
        PowerMockito.when(mockViolationTypeDAO.contains(testViolationType)).thenReturn(false);
        PowerMockito.when(mockViolationTypeDAO.findById(0)).thenReturn(null);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        ViolationTypeDAO violationTypeDAO = ProxyViolationTypeDAO.getInstance();

        CommonTest.testOnDefault(violationTypeDAO, testViolationType, Collections.emptyList(),false, null);
    }

    @Test
    public void currentDBAndCorrectVal() throws Exception {
        List<ViolationType> violationTypes = new LinkedList<>();
        violationTypes.add(new ViolationType());
        PowerMockito.when(mockViolationTypeDAO.getAll()).thenReturn(violationTypes);
        PowerMockito.when(mockViolationTypeDAO.getAll(CommonTest.testPerson)).thenReturn(violationTypes);
        PowerMockito.when(mockViolationTypeDAO.add(testViolationType)).thenReturn(true);
        PowerMockito.when(mockViolationTypeDAO.remove(testViolationType)).thenReturn(true);
        PowerMockito.when(mockViolationTypeDAO.update(testViolationType)).thenReturn(true);
        PowerMockito.when(mockViolationTypeDAO.contains(testViolationType)).thenReturn(true);
        PowerMockito.when(mockViolationTypeDAO.findById(0)).thenReturn(testViolationType);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        ViolationTypeDAO violationTypeDAO = ProxyViolationTypeDAO.getInstance();

        CommonTest.testOnDefault(violationTypeDAO, testViolationType, violationTypes,true, testViolationType);
    }

    @Test
    public void currentDBIsNull() {
        when(systemConfig.getCurrentDB()).thenReturn(null);
        ViolationTypeDAO violationTypeDAO = ProxyViolationTypeDAO.getInstance();
        CommonTest.testOnDefault(violationTypeDAO, testViolationType, Collections.emptyList(), false, null);
    }

    @Test
    public void currentDBIsEmpty() {
        when(systemConfig.getCurrentDB()).thenReturn("");
        ViolationTypeDAO violationTypeDAO = ProxyViolationTypeDAO.getInstance();
        CommonTest.testOnDefault(violationTypeDAO, testViolationType, Collections.emptyList(), false, null);
    }

    @Test
    public void existingInstanceTest() throws Exception {
        ViolationTypeDAO violationTypeDAO = ProxyViolationTypeDAO.getInstance();

        instance.set(null, violationTypeDAO);

        Assert.assertEquals(violationTypeDAO, ProxyViolationTypeDAO.getInstance());
    }
}