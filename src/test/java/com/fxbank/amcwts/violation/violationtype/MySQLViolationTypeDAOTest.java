package com.fxbank.amcwts.violation.violationtype;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 17.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class MySQLViolationTypeDAOTest {
    private Field instance;
    private Connection connection;
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);
    private ViolationType testViolationType = new ViolationType(-1, "Test name");

    public MySQLViolationTypeDAOTest() throws SQLException, NoSuchFieldException {
        instance = MySQLViolationTypeDAO.class.getDeclaredField("VIOLATION_TYPE_DAO");
        instance.setAccessible(true);
        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));
        mockStatic(ConnectionFactory.class);
        connection = mock(Connection.class);

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenReturn(connection);
    }

    @Before
    public void before() throws Exception {
        instance.set(null, null);
        Mockito.reset(connection);
        when(connection, "prepareStatement", any()).thenReturn(preparedStatement);
        when(connection, "prepareStatement", any(), anyInt()).thenReturn(preparedStatement);

        testViolationType.setId(-1);
        testViolationType.setName("Test name");
    }

    @Test
    public void add() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertTrue(violationTypeDAO.add(testViolationType));
        /*verify(preparedStatement).setString(1, "Test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setInt(3, 1);
        verify(preparedStatement).getGeneratedKeys();
        verify(generatedKey).getLong(1);*/
        Assert.assertEquals(testViolationType.getId(), 1);
    }

    @Test
    public void addWithErrorOfGeneratedKey() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(false);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertFalse(violationTypeDAO.add(testViolationType));
    }

    @Test
    public void addWithErrorOfAddingInDB() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(0);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertFalse(violationTypeDAO.add(testViolationType));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionUpdate() throws Exception {
        testViolationType.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertFalse(violationTypeDAO.add(testViolationType));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionGetConnection() throws Exception {
        testViolationType.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertFalse(violationTypeDAO.add(testViolationType));
    }

    @Test
    public void update() throws Exception {
        add();
        testViolationType.setId(1);
        testViolationType.setName("New test name");

        when(preparedStatement, "executeUpdate").thenReturn(1);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertTrue(violationTypeDAO.update(testViolationType));
        /*verify(preparedStatement).setString(1, "New test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setLong(3, 1);*/
    }

    @Test
    public void updateWithUpdateErrorInDB() throws Exception {
        testViolationType.setId(1);
        testViolationType.setName("New test name");

        when(preparedStatement, "executeUpdate").thenReturn(0);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertFalse(violationTypeDAO.update(testViolationType));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionUpdate() throws Exception {
        testViolationType.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertFalse(violationTypeDAO.update(testViolationType));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionGetConnection() throws Exception {
        testViolationType.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertFalse(violationTypeDAO.update(testViolationType));
    }

    @Test
    public void remove() throws Exception {
        testViolationType.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertTrue(violationTypeDAO.remove(testViolationType));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test
    public void removeWithErrorInDB() throws Exception {
        testViolationType.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertFalse(violationTypeDAO.remove(testViolationType));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionUpdate() throws Exception {
        testViolationType.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertFalse(violationTypeDAO.remove(testViolationType));
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionGetConnection() throws Exception {
        testViolationType.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertFalse(violationTypeDAO.remove(testViolationType));
    }

    @Test
    public void getAll() throws Exception {
        testViolationType.setId(1);
        List<ViolationType> violationTypeList = new LinkedList<>();
        violationTypeList.add(testViolationType);
        ViolationType testViolationType2 = new ViolationType(2, "test2");
        violationTypeList.add(testViolationType2);
        ViolationType testViolationType3 = new ViolationType(3, null);
        violationTypeList.add(testViolationType3);
        ViolationType testViolationType4 = new ViolationType(4, "test4");
        violationTypeList.add(testViolationType4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "VIOLATION_TYPE_id")
                .thenReturn(testViolationType.getId())
                .thenReturn(testViolationType2.getId())
                .thenReturn(testViolationType3.getId())
                .thenReturn(testViolationType4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "VIOLATION_TYPE_name")
                .thenReturn(testViolationType.getName())
                .thenReturn(testViolationType2.getName())
                .thenReturn(testViolationType3.getName())
                .thenReturn(testViolationType4.getName())
                .thenReturn(null);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(violationTypeDAO.getAll(), violationTypeList));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionQuery() throws Exception {
        testViolationType.setId(1);

        when(preparedStatement, "executeQuery").thenThrow(new SQLException());

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        violationTypeDAO.getAll();
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithGetConnectionException() throws Exception {
        testViolationType.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        violationTypeDAO.getAll();
    }

    @Test
    public void getAllByPerson() throws Exception {
        testViolationType.setId(1);
        List<ViolationType> violationTypeList = new LinkedList<>();
        violationTypeList.add(testViolationType);
        ViolationType testViolationType2 = new ViolationType(2, "test2");
        violationTypeList.add(testViolationType2);
        ViolationType testViolationType3 = new ViolationType(3, null);
        violationTypeList.add(testViolationType3);
        ViolationType testViolationType4 = new ViolationType(4, "test4");
        violationTypeList.add(testViolationType4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "VIOLATION_TYPE_id")
                .thenReturn(testViolationType.getId())
                .thenReturn(testViolationType2.getId())
                .thenReturn(testViolationType3.getId())
                .thenReturn(testViolationType4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "VIOLATION_TYPE_name")
                .thenReturn(testViolationType.getName())
                .thenReturn(testViolationType2.getName())
                .thenReturn(testViolationType3.getName())
                .thenReturn(testViolationType4.getName())
                .thenReturn(null);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(violationTypeDAO.getAll(null), violationTypeList));
    }


    @Test
    public void findById() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L).thenReturn(2L).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        testViolationType.setId(1);
        ViolationType testViolationType2 = new ViolationType(2, "test2");
        ViolationType testViolationType3 = new ViolationType(3, null);
        ViolationType testViolationType4 = new ViolationType(4, "test4");
        violationTypeDAO.add(testViolationType);
        violationTypeDAO.add(testViolationType2);
        violationTypeDAO.add(testViolationType4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true);

        when(resultSet, "getLong", "VIOLATION_TYPE_id")
                .thenReturn(testViolationType3.getId());
        when(resultSet, "getString", "VIOLATION_TYPE_name")
                .thenReturn(testViolationType3.getName());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Assert.assertEquals(violationTypeDAO.findById(1), testViolationType);
        Assert.assertEquals(violationTypeDAO.findById(2), testViolationType2);
        Assert.assertTrue(assertEqualsByContent(violationTypeDAO.findById(3), testViolationType3));
        Assert.assertEquals(violationTypeDAO.findById(4), testViolationType4);
    }

    @Test(expected = RuntimeException.class)
    public void addWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        violationTypeDAO.add(testViolationType);
    }

    @Test(expected = RuntimeException.class)
    public void removeWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        violationTypeDAO.remove(testViolationType);
    }

    @Test(expected = RuntimeException.class)
    public void updateWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        ViolationTypeDAO violationTypeDAO = MySQLViolationTypeDAO.getInstance();
        violationTypeDAO.update(testViolationType);
    }

    static boolean assertEqualsListByContent(List<ViolationType> firstList, List<ViolationType> secondList) {
        if(firstList.size() != secondList.size())
            return false;
        for(int i = 0, size = firstList.size(); i < size; i++)
            if(!assertEqualsByContent(firstList.get(i), secondList.get(i)))
                return false;
        return true;
    }

    static boolean assertEqualsByContent(ViolationType violationType, ViolationType violationType2) {
        return violationType.getId() == violationType2.getId();
    }
}