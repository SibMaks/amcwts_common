package com.fxbank.amcwts.violation;

import com.fxbank.amcwts.CommonTest;
import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.person.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, MySQLViolationDAO.class})
public class ProxyViolationDAOTest {
    private Field instance;
    private SystemConfig systemConfig;
    private ViolationDAO mockViolationDAO;
    private Violation testViolation;
    private Person testPerson = new Person();
    private LocalDateTime testLocalDT = LocalDateTime.now();

    public ProxyViolationDAOTest() throws NoSuchFieldException {
        instance = ProxyViolationDAO.class.getDeclaredField("VIOLATION_DAO");
        systemConfig = mock(SystemConfig.class);
        mockViolationDAO = mock(MySQLViolationDAO.class);
        testViolation = new Violation();
    }

    @Before
    public void setUp() throws Exception {
        instance.setAccessible(true);
        instance.set(null, null);

        Mockito.reset(systemConfig);
        Mockito.reset(mockViolationDAO);

        PowerMockito.mockStatic(ConfigWorker.class);
        PowerMockito.mockStatic(MySQLViolationDAO.class);

        ConfigWorker configWorker = PowerMockito.mock(ConfigWorker.class);
        PowerMockito.when(configWorker.getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(ConfigWorker.getInstance()).thenReturn(configWorker);

        when(ConfigWorker.getInstance().getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(MySQLViolationDAO.getInstance()).thenReturn(mockViolationDAO);
    }

    @Test
    public void currentDBIsCorrect() throws Exception {
        PowerMockito.when(mockViolationDAO.getAll()).thenReturn(Collections.emptyList());
        PowerMockito.when(mockViolationDAO.getAll(CommonTest.testPerson)).thenReturn(Collections.emptyList());
        PowerMockito.when(mockViolationDAO.add(testViolation)).thenReturn(false);
        PowerMockito.when(mockViolationDAO.remove(testViolation)).thenReturn(false);
        PowerMockito.when(mockViolationDAO.update(testViolation)).thenReturn(false);
        PowerMockito.when(mockViolationDAO.contains(testViolation)).thenReturn(false);
        PowerMockito.when(mockViolationDAO.findById(0)).thenReturn(null);

        PowerMockito.when(mockViolationDAO.getAllByDates(testPerson, testLocalDT, testLocalDT)).thenReturn(Collections.emptyList());
        PowerMockito.when(mockViolationDAO.getByDatesForPerson(testPerson, testLocalDT, testLocalDT)).thenReturn(Collections.emptyList());
        PowerMockito.when(mockViolationDAO.getLast(testPerson)).thenReturn(null);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        ViolationDAO violationDAO = ProxyViolationDAO.getInstance();

        CommonTest.testOnDefault(violationDAO, testViolation, Collections.emptyList(),false, null);

        Assert.assertEquals(violationDAO.getAllByDates(testPerson, testLocalDT, testLocalDT), Collections.emptyList());
        Assert.assertEquals(violationDAO.getByDatesForPerson(testPerson, testLocalDT, testLocalDT), Collections.emptyList());
        Assert.assertEquals(violationDAO.getLast(testPerson), null);
    }

    @Test
    public void currentDBAndCorrectVal() throws Exception {
        List<Violation> violations = new LinkedList<>();
        violations.add(new Violation());
        PowerMockito.when(mockViolationDAO.getAll()).thenReturn(violations);
        PowerMockito.when(mockViolationDAO.getAll(CommonTest.testPerson)).thenReturn(violations);
        PowerMockito.when(mockViolationDAO.add(testViolation)).thenReturn(true);
        PowerMockito.when(mockViolationDAO.remove(testViolation)).thenReturn(true);
        PowerMockito.when(mockViolationDAO.update(testViolation)).thenReturn(true);
        PowerMockito.when(mockViolationDAO.contains(testViolation)).thenReturn(true);
        PowerMockito.when(mockViolationDAO.findById(0)).thenReturn(testViolation);

        PowerMockito.when(mockViolationDAO.getAllByDates(testPerson, testLocalDT, testLocalDT)).thenReturn(violations);
        PowerMockito.when(mockViolationDAO.getByDatesForPerson(testPerson, testLocalDT, testLocalDT)).thenReturn(violations);
        PowerMockito.when(mockViolationDAO.getLast(testPerson)).thenReturn(violations.get(0));

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        ViolationDAO violationDAO = ProxyViolationDAO.getInstance();

        CommonTest.testOnDefault(violationDAO, testViolation, violations,true, testViolation);
;
        Assert.assertEquals(violationDAO.getAllByDates(testPerson, testLocalDT, testLocalDT), violations);
        Assert.assertEquals(violationDAO.getByDatesForPerson(testPerson, testLocalDT, testLocalDT), violations);
        Assert.assertEquals(violationDAO.getLast(testPerson), violations.get(0));
    }

    @Test
    public void currentDBIsNull() {
        when(systemConfig.getCurrentDB()).thenReturn(null);
        ViolationDAO violationDAO = ProxyViolationDAO.getInstance();
        CommonTest.testOnDefault(violationDAO, testViolation, Collections.emptyList(), false, null);

        Assert.assertEquals(violationDAO.getAllByDates(testPerson, testLocalDT, testLocalDT), Collections.emptyList());
        Assert.assertEquals(violationDAO.getByDatesForPerson(testPerson, testLocalDT, testLocalDT), Collections.emptyList());
        Assert.assertEquals(violationDAO.getLast(testPerson), null);
    }

    @Test
    public void currentDBIsEmpty() {
        when(systemConfig.getCurrentDB()).thenReturn("");
        ViolationDAO violationDAO = ProxyViolationDAO.getInstance();
        CommonTest.testOnDefault(violationDAO, testViolation, Collections.emptyList(), false, null);

        Assert.assertEquals(violationDAO.getAllByDates(testPerson, testLocalDT, testLocalDT), Collections.emptyList());
        Assert.assertEquals(violationDAO.getByDatesForPerson(testPerson, testLocalDT, testLocalDT), Collections.emptyList());
        Assert.assertEquals(violationDAO.getLast(testPerson), null);
    }

    @Test
    public void existingInstanceTest() throws Exception {
        ViolationDAO violationDAO = ProxyViolationDAO.getInstance();

        instance.set(null, violationDAO);

        Assert.assertEquals(violationDAO, ProxyViolationDAO.getInstance());
    }
}