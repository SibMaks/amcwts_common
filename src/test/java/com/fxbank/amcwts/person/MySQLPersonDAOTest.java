package com.fxbank.amcwts.person;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.group.Group;
import com.fxbank.amcwts.person.group.GroupDAO;
import com.fxbank.amcwts.person.group.ProxyGroupDAO;
import com.fxbank.amcwts.person.position.Position;
import com.fxbank.amcwts.person.position.PositionDAO;
import com.fxbank.amcwts.person.position.ProxyPositionDAO;
import com.fxbank.amcwts.person.worktime.ProxyWorkTimeDAO;
import com.fxbank.amcwts.person.worktime.WorkTime;
import com.fxbank.amcwts.person.worktime.WorkTimeDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.*;
import static org.powermock.api.mockito.PowerMockito.*;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 17.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class MySQLPersonDAOTest {
    private Field instance;
    private Field instanceProxyPosition;
    private Field instanceProxyWorkTime;
    private Field instanceProxyGroup;
    private PositionDAO mockPositionDAO = mock(PositionDAO.class);
    private WorkTimeDAO mockWorkTimeDAO = mock(WorkTimeDAO.class);
    private GroupDAO mockGroupDAO = mock(GroupDAO.class);
    private Connection connection;
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);
    private Group testGroup = new Group();
    private Position testPosition = new Position();
    private LocalTime localTime = LocalTime.of(9, 0);
    private LocalTime localTime2 = LocalTime.of(18, 0);
    private WorkTime testWorkTime = new WorkTime(1, localTime, localTime2);
    private Person testPerson = new Person(-1, "PERSID", "login", "password", "lastName", "firstName", "middleName", testGroup,false, testPosition, testWorkTime);
    private Person testPerson2 = new Person(2, "PERSID2", "login", "password", "lastName", "firstName", "middleName", testGroup,false, testPosition, testWorkTime);
    private Person testPerson3 = new Person(3, "PERSID3", "login", "password", "lastName", "firstName", "middleName", testGroup,false, testPosition, testWorkTime);
    private Person testPerson4 = new Person(4, "PERSID4", "login", "password", "lastName", "firstName", "middleName", testGroup,false, testPosition, testWorkTime);

    private Person ADMIN = new Person();

    public MySQLPersonDAOTest() throws SQLException, NoSuchFieldException {
        ADMIN.setAdmin(true);

        instanceProxyGroup = ProxyGroupDAO.class.getDeclaredField("GROUP_DAO");
        instanceProxyGroup.setAccessible(true);
        instanceProxyWorkTime = ProxyWorkTimeDAO.class.getDeclaredField("WORK_TIME_DAO");
        instanceProxyWorkTime.setAccessible(true);
        instanceProxyPosition = ProxyPositionDAO.class.getDeclaredField("POSITION_DAO");
        instanceProxyPosition.setAccessible(true);
        instance = MySQLPersonDAO.class.getDeclaredField("PERSON_DAO");
        instance.setAccessible(true);
        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));
        mockStatic(ConnectionFactory.class);
        connection = mock(Connection.class);

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenReturn(connection);

        when(mockGroupDAO.findById(anyLong())).thenReturn(testGroup);
        when(mockWorkTimeDAO.findById(anyLong())).thenReturn(testWorkTime);
        when(mockPositionDAO.findById(anyLong())).thenReturn(testPosition);
    }

    @Before
    public void setup() throws Exception {
        instance.set(null, null);
        Mockito.reset(connection);

        instanceProxyGroup.set(null, mockGroupDAO);
        instanceProxyWorkTime.set(null, mockWorkTimeDAO);
        instanceProxyPosition.set(null, mockPositionDAO);

        when(connection, "prepareStatement", any()).thenReturn(preparedStatement);
        when(connection, "prepareStatement", any(), anyInt()).thenReturn(preparedStatement);

        testPerson.setId(-1);
        testPerson.setLogin("login");
    }

    @Test
    public void add() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertTrue(personDAO.add(testPerson));
        /*verify(preparedStatement).setString(1, "Test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setInt(3, 1);
        verify(preparedStatement).getGeneratedKeys();
        verify(generatedKey).getLong(1);*/
        Assert.assertEquals(testPerson.getId(), 1);
    }

    @Test
    public void addWithErrorOfGeneratedKey() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(false);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.add(testPerson));
    }

    @Test
    public void addWithErrorOfAddingInDB() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(0);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.add(testPerson));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionUpdate() throws Exception {
        testPerson.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.add(testPerson));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionGetConnection() throws Exception {
        testPerson.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.add(testPerson));
    }

    @Test
    public void update() throws Exception {
        add();
        testPerson.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertTrue(personDAO.update(testPerson));
        /*verify(preparedStatement).setString(1, "New test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setLong(3, 1);*/
    }

    @Test
    public void updateWithUpdateErrorInDB() throws Exception {
        testPerson.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.update(testPerson));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionUpdate() throws Exception {
        testPerson.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.update(testPerson));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionGetConnection() throws Exception {
        testPerson.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.update(testPerson));
    }

    @Test
    public void remove() throws Exception {
        testPerson.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertTrue(personDAO.remove(testPerson));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test
    public void removeWithErrorInDB() throws Exception {
        testPerson.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.remove(testPerson));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionUpdate() throws Exception {
        testPerson.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.remove(testPerson));
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionGetConnection() throws Exception {
        testPerson.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.remove(testPerson));
    }

    @Test
    public void getAll() throws Exception {
        testPerson.setId(1);
        List<Person> personList = new LinkedList<>();
        personList.add(testPerson);
        personList.add(testPerson2);
        personList.add(testPerson3);
        personList.add(testPerson4);

        init4Persons();

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(personDAO.getAll(), personList));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionQuery() throws Exception {
        testPerson.setId(1);

        when(preparedStatement, "executeQuery").thenThrow(new SQLException());

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        personDAO.getAll();
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithGetConnectionException() throws Exception {
        testPerson.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        personDAO.getAll();
    }

    @Test
    public void getAllByPerson() throws Exception {
        testPerson.setId(1);
        List<Person> personList = new LinkedList<>();
        personList.add(testPerson);
        personList.add(testPerson2);
        personList.add(testPerson3);
        personList.add(testPerson4);

        init4Persons();

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(personDAO.getAll(ADMIN), personList));
    }

    @Test
    public void findById() throws Exception {
        PersonDAO personDAO = MySQLPersonDAO.getInstance();

        init4Persons();

        Assert.assertTrue(assertEqualsByContent(personDAO.findById(1), testPerson));
        Assert.assertTrue(assertEqualsByContent(personDAO.findById(2), testPerson2));
        Assert.assertTrue(assertEqualsByContent(personDAO.findById(3), testPerson3));
        Assert.assertTrue(assertEqualsByContent(personDAO.findById(4), testPerson4));
    }

    @Test
    public void findByLogin() throws Exception {
        PersonDAO personDAO = MySQLPersonDAO.getInstance();

        init4Persons();

        Assert.assertTrue(assertEqualsByContent(personDAO.findByLogin(testPerson.getLogin()), testPerson));
        Assert.assertTrue(assertEqualsByContent(personDAO.findByLogin(testPerson2.getLogin()), testPerson2));
        Assert.assertTrue(assertEqualsByContent(personDAO.findByLogin(testPerson3.getLogin()), testPerson3));
        Assert.assertTrue(assertEqualsByContent(personDAO.findByLogin(testPerson4.getLogin()), testPerson4));
    }

    @Test
    public void findByPersId() throws Exception {
        PersonDAO personDAO = MySQLPersonDAO.getInstance();

        init4Persons();

        Assert.assertTrue(assertEqualsByContent(personDAO.findByPersId(testPerson.getPersonId()), testPerson));
        Assert.assertTrue(assertEqualsByContent(personDAO.findByPersId(testPerson2.getPersonId()), testPerson2));
        Assert.assertTrue(assertEqualsByContent(personDAO.findByPersId(testPerson3.getPersonId()), testPerson3));
        Assert.assertTrue(assertEqualsByContent(personDAO.findByPersId(testPerson4.getPersonId()), testPerson4));
    }

    @Test
    public void contains() throws Exception {
        PersonDAO personDAO = MySQLPersonDAO.getInstance();

        init4Persons();
        Person testPerson5 = new Person();
        testPerson5.setId(5);

        Assert.assertTrue(personDAO.contains(testPerson));
        Assert.assertTrue(personDAO.contains(testPerson2));
        Assert.assertTrue(personDAO.contains(testPerson3));
        Assert.assertTrue(personDAO.contains(testPerson4));
        Assert.assertFalse(personDAO.contains(testPerson5));
    }

    @Test(expected = RuntimeException.class)
    public void addWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        personDAO.add(testPerson);
    }

    @Test(expected = RuntimeException.class)
    public void addAllWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        personDAO.addAll(Collections.emptyList());
    }

    @Test(expected = RuntimeException.class)
    public void removeWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        personDAO.remove(testPerson);
    }

    @Test(expected = RuntimeException.class)
    public void updateWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        personDAO.update(testPerson);
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithNullConFactory() throws Exception {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        personDAO.getAll();
    }

    @Test
    public void addAll() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(10L).thenReturn(20L).thenReturn(30L).thenReturn(40L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        List<Person> personList = new LinkedList<>();
        personList.add(testPerson);
        personList.add(testPerson2);
        personList.add(testPerson3);
        personList.add(testPerson4);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertTrue(personDAO.addAll(personList));
        Assert.assertEquals(testPerson.getId(), 10);
        Assert.assertEquals(testPerson2.getId(), 20);
        Assert.assertEquals(testPerson3.getId(), 30);
        Assert.assertEquals(testPerson4.getId(), 40);
    }

    @Test
    public void addAllWithOneIncorrect() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(10L).thenReturn(20L).thenReturn(30L).thenReturn(40L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1).thenReturn(0).thenReturn(1);

        List<Person> personList = new LinkedList<>();
        personList.add(testPerson);
        personList.add(testPerson2);
        personList.add(testPerson3);
        personList.add(testPerson4);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.addAll(personList));
    }

    @Test
    public void addAllWithOneMinOneKey() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(10L).thenReturn(-1L).thenReturn(30L).thenReturn(40L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        List<Person> personList = new LinkedList<>();
        personList.add(testPerson);
        personList.add(testPerson2);
        personList.add(testPerson3);
        personList.add(testPerson4);

        PersonDAO personDAO = MySQLPersonDAO.getInstance();
        Assert.assertFalse(personDAO.addAll(personList));
    }

    static boolean assertEqualsListByContent(List<Person> firstList, List<Person> secondList) {
        if(firstList.size() != secondList.size())
            return false;
        for(int i = 0, size = firstList.size(); i < size; i++)
            if(!assertEqualsByContent(firstList.get(i), secondList.get(i)))
                return false;
        return true;
    }

    static boolean assertEqualsByContent(Person person, Person person2) {
        return person.getId() == person2.getId();
    }

    void init4Persons() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L).thenReturn(2L).thenReturn(3L).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        testPerson.setId(1);
        ResultSet resultSet = mock(ResultSet.class);

        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "PERSON_id")
                .thenReturn(testPerson.getId())
                .thenReturn(testPerson2.getId())
                .thenReturn(testPerson3.getId())
                .thenReturn(testPerson4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "PERS_ID")
                .thenReturn(testPerson.getPersonId())
                .thenReturn(testPerson2.getPersonId())
                .thenReturn(testPerson3.getPersonId())
                .thenReturn(testPerson4.getPersonId())
                .thenReturn(null);
        when(resultSet, "getString", "LAST_NAME")
                .thenReturn(testPerson.getLastName())
                .thenReturn(testPerson2.getLastName())
                .thenReturn(testPerson3.getLastName())
                .thenReturn(testPerson4.getLastName())
                .thenReturn(null);
        when(resultSet, "getString", "FIRST_NAME")
                .thenReturn(testPerson.getFirstName())
                .thenReturn(testPerson2.getFirstName())
                .thenReturn(testPerson3.getFirstName())
                .thenReturn(testPerson4.getFirstName())
                .thenReturn(null);
        when(resultSet, "getString", "MIDDLE_NAME")
                .thenReturn(testPerson.getMiddleName())
                .thenReturn(testPerson2.getMiddleName())
                .thenReturn(testPerson3.getMiddleName())
                .thenReturn(testPerson4.getMiddleName())
                .thenReturn(null);
        when(resultSet, "getBoolean", "IS_ADMIN")
                .thenReturn(testPerson.isAdmin())
                .thenReturn(testPerson2.isAdmin())
                .thenReturn(testPerson3.isAdmin())
                .thenReturn(testPerson4.isAdmin())
                .thenReturn(false);
        when(resultSet, "getString", "LOGIN")
                .thenReturn(testPerson.getLogin())
                .thenReturn(testPerson2.getLogin())
                .thenReturn(testPerson3.getLogin())
                .thenReturn(testPerson4.getLogin())
                .thenReturn(null);
        when(resultSet, "getString", "PASSWORD")
                .thenReturn(testPerson.getPassword())
                .thenReturn(testPerson2.getPassword())
                .thenReturn(testPerson3.getPassword())
                .thenReturn(testPerson4.getPassword())
                .thenReturn(null);
        when(resultSet, "getLong", "GROUP_id")
                .thenReturn(testPerson.getGroup().getId())
                .thenReturn(testPerson2.getGroup().getId())
                .thenReturn(testPerson3.getGroup().getId())
                .thenReturn(testPerson4.getGroup().getId())
                .thenReturn(-1L);
        when(resultSet, "getLong", "POSITION_id")
                .thenReturn(testPerson.getPosition().getId())
                .thenReturn(testPerson2.getPosition().getId())
                .thenReturn(testPerson3.getPosition().getId())
                .thenReturn(testPerson4.getPosition().getId())
                .thenReturn(-1L);
        when(resultSet, "getLong", "WORK_TIME_id")
                .thenReturn(testPerson.getWorkTime().getId())
                .thenReturn(testPerson2.getWorkTime().getId())
                .thenReturn(testPerson3.getWorkTime().getId())
                .thenReturn(testPerson4.getWorkTime().getId())
                .thenReturn(-1L);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);
    }
}