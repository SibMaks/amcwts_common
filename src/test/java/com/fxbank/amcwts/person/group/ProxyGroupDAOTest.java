package com.fxbank.amcwts.person.group;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.CommonTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.powermock.api.mockito.PowerMockito.*;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 10.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, MySQLGroupDAO.class})
public class ProxyGroupDAOTest {
    private Field instance;
    private SystemConfig systemConfig;
    private GroupDAO mockGroupDAO;
    private Group testGroup;

    public ProxyGroupDAOTest() throws NoSuchFieldException {
        instance = ProxyGroupDAO.class.getDeclaredField("GROUP_DAO");
        systemConfig = mock(SystemConfig.class);
        mockGroupDAO = mock(MySQLGroupDAO.class);
        testGroup = new Group();
    }

    @Before
    public void setUp() throws Exception {
        instance.setAccessible(true);
        instance.set(null, null);

        Mockito.reset(systemConfig);
        Mockito.reset(mockGroupDAO);

        PowerMockito.mockStatic(ConfigWorker.class);
        PowerMockito.mockStatic(MySQLGroupDAO.class);

        ConfigWorker configWorker = PowerMockito.mock(ConfigWorker.class);
        PowerMockito.when(configWorker.getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(ConfigWorker.getInstance()).thenReturn(configWorker);

        when(ConfigWorker.getInstance().getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(MySQLGroupDAO.getInstance()).thenReturn(mockGroupDAO);
    }

    @Test
    public void currentDBIsCorrect() throws Exception {
        PowerMockito.when(mockGroupDAO.getAll()).thenReturn(Collections.emptyList());
        PowerMockito.when(mockGroupDAO.getAll(CommonTest.testPerson)).thenReturn(Collections.emptyList());
        PowerMockito.when(mockGroupDAO.add(testGroup)).thenReturn(false);
        PowerMockito.when(mockGroupDAO.remove(testGroup)).thenReturn(false);
        PowerMockito.when(mockGroupDAO.update(testGroup)).thenReturn(false);
        PowerMockito.when(mockGroupDAO.contains(testGroup)).thenReturn(false);
        PowerMockito.when(mockGroupDAO.findById(0)).thenReturn(null);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        GroupDAO groupDAO = ProxyGroupDAO.getInstance();

        CommonTest.testOnDefault(groupDAO, testGroup, Collections.emptyList(),false, null);
    }

    @Test
    public void currentDBAndCorrectVal() throws Exception {
        List<Group> groups = new LinkedList<>();
        groups.add(new Group());
        PowerMockito.when(mockGroupDAO.getAll()).thenReturn(groups);
        PowerMockito.when(mockGroupDAO.getAll(CommonTest.testPerson)).thenReturn(groups);
        PowerMockito.when(mockGroupDAO.add(testGroup)).thenReturn(true);
        PowerMockito.when(mockGroupDAO.remove(testGroup)).thenReturn(true);
        PowerMockito.when(mockGroupDAO.update(testGroup)).thenReturn(true);
        PowerMockito.when(mockGroupDAO.contains(testGroup)).thenReturn(true);
        PowerMockito.when(mockGroupDAO.findById(0)).thenReturn(testGroup);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        GroupDAO groupDAO = ProxyGroupDAO.getInstance();

        CommonTest.testOnDefault(groupDAO, testGroup, groups,true, testGroup);
    }

    @Test
    public void currentDBIsNull() {
        when(systemConfig.getCurrentDB()).thenReturn(null);
        GroupDAO groupDAO = ProxyGroupDAO.getInstance();
        CommonTest.testOnDefault(groupDAO, testGroup, Collections.emptyList(), false, null);
    }

    @Test
    public void currentDBIsEmpty() {
        when(systemConfig.getCurrentDB()).thenReturn("");
        GroupDAO groupDAO = ProxyGroupDAO.getInstance();
        CommonTest.testOnDefault(groupDAO, testGroup, Collections.emptyList(), false, null);
    }

    @Test
    public void existingInstanceTest() throws Exception {
        GroupDAO groupDAO = ProxyGroupDAO.getInstance();

        instance.set(null, groupDAO);

        Assert.assertEquals(groupDAO, ProxyGroupDAO.getInstance());
    }
}