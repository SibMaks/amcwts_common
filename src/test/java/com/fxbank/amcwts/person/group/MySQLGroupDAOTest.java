package com.fxbank.amcwts.person.group;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 17.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class MySQLGroupDAOTest {
    private Field instance;
    private Connection connection;
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);
    private Group testGroup = new Group(-1, "Test name");

    public MySQLGroupDAOTest() throws SQLException, NoSuchFieldException {
        instance = MySQLGroupDAO.class.getDeclaredField("GROUP_DAO");
        instance.setAccessible(true);
        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));
        mockStatic(ConnectionFactory.class);
        connection = mock(Connection.class);

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenReturn(connection);
    }

    @Before
    public void before() throws Exception {
        instance.set(null, null);
        Mockito.reset(connection);
        when(connection, "prepareStatement", any()).thenReturn(preparedStatement);
        when(connection, "prepareStatement", any(), anyInt()).thenReturn(preparedStatement);

        testGroup.setId(-1);
        testGroup.setName("Test name");
    }

    @Test
    public void add() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertTrue(groupDAO.add(testGroup));
        /*verify(preparedStatement).setString(1, "Test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setInt(3, 1);
        verify(preparedStatement).getGeneratedKeys();
        verify(generatedKey).getLong(1);*/
        Assert.assertEquals(testGroup.getId(), 1);
    }

    @Test
    public void addWithErrorOfGeneratedKey() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(false);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertFalse(groupDAO.add(testGroup));
    }

    @Test
    public void addWithErrorOfAddingInDB() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(0);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertFalse(groupDAO.add(testGroup));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionUpdate() throws Exception {
        testGroup.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertFalse(groupDAO.add(testGroup));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionGetConnection() throws Exception {
        testGroup.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertFalse(groupDAO.add(testGroup));
    }

    @Test
    public void update() throws Exception {
        add();
        testGroup.setId(1);
        testGroup.setName("New test name");

        when(preparedStatement, "executeUpdate").thenReturn(1);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertTrue(groupDAO.update(testGroup));
        /*verify(preparedStatement).setString(1, "New test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setLong(3, 1);*/
    }

    @Test
    public void updateWithUpdateErrorInDB() throws Exception {
        testGroup.setId(1);
        testGroup.setName("New test name");

        when(preparedStatement, "executeUpdate").thenReturn(0);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertFalse(groupDAO.update(testGroup));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionUpdate() throws Exception {
        testGroup.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertFalse(groupDAO.update(testGroup));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionGetConnection() throws Exception {
        testGroup.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertFalse(groupDAO.update(testGroup));
    }

    @Test
    public void remove() throws Exception {
        testGroup.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertTrue(groupDAO.remove(testGroup));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test
    public void removeWithErrorInDB() throws Exception {
        testGroup.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertFalse(groupDAO.remove(testGroup));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionUpdate() throws Exception {
        testGroup.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertFalse(groupDAO.remove(testGroup));
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionGetConnection() throws Exception {
        testGroup.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertFalse(groupDAO.remove(testGroup));
    }

    @Test
    public void getAll() throws Exception {
        testGroup.setId(1);
        List<Group> groupList = new LinkedList<>();
        groupList.add(testGroup);
        Group testGroup2 = new Group(2, "test2");
        groupList.add(testGroup2);
        Group testGroup3 = new Group(3, null);
        groupList.add(testGroup3);
        Group testGroup4 = new Group(4, "test4");
        groupList.add(testGroup4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "GROUP_id")
                .thenReturn(testGroup.getId())
                .thenReturn(testGroup2.getId())
                .thenReturn(testGroup3.getId())
                .thenReturn(testGroup4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "NAME")
                .thenReturn(testGroup.getName())
                .thenReturn(testGroup2.getName())
                .thenReturn(testGroup3.getName())
                .thenReturn(testGroup4.getName())
                .thenReturn(null);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(groupDAO.getAll(), groupList));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionQuery() throws Exception {
        testGroup.setId(1);

        when(preparedStatement, "executeQuery").thenThrow(new SQLException());

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        groupDAO.getAll();
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithGetConnectionException() throws Exception {
        testGroup.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        groupDAO.getAll();
    }

    @Test
    public void getAllByPerson() throws Exception {
        testGroup.setId(1);
        List<Group> groupList = new LinkedList<>();
        groupList.add(testGroup);
        Group testGroup2 = new Group(2, "test2");
        groupList.add(testGroup2);
        Group testGroup3 = new Group(3, null);
        groupList.add(testGroup3);
        Group testGroup4 = new Group(4, "test4");
        groupList.add(testGroup4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "GROUP_id")
                .thenReturn(testGroup.getId())
                .thenReturn(testGroup2.getId())
                .thenReturn(testGroup3.getId())
                .thenReturn(testGroup4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "NAME")
                .thenReturn(testGroup.getName())
                .thenReturn(testGroup2.getName())
                .thenReturn(testGroup3.getName())
                .thenReturn(testGroup4.getName())
                .thenReturn(null);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(groupDAO.getAll(null), groupList));
    }


    @Test
    public void findById() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L).thenReturn(2L).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        testGroup.setId(1);
        Group testGroup2 = new Group(2, "test2");
        Group testGroup3 = new Group(3, null);
        Group testGroup4 = new Group(4, "test4");
        groupDAO.add(testGroup);
        groupDAO.add(testGroup2);
        groupDAO.add(testGroup4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true);

        when(resultSet, "getLong", "GROUP_id")
                .thenReturn(testGroup3.getId());
        when(resultSet, "getString", "NAME")
                .thenReturn(testGroup3.getName());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Assert.assertEquals(groupDAO.findById(1), testGroup);
        Assert.assertEquals(groupDAO.findById(2), testGroup2);
        Assert.assertTrue(assertEqualsByContent(groupDAO.findById(3), testGroup3));
        Assert.assertEquals(groupDAO.findById(4), testGroup4);
    }

    @Test(expected = RuntimeException.class)
    public void addWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        groupDAO.add(testGroup);
    }

    @Test(expected = RuntimeException.class)
    public void removeWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        groupDAO.remove(testGroup);
    }

    @Test(expected = RuntimeException.class)
    public void updateWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        GroupDAO groupDAO = MySQLGroupDAO.getInstance();
        groupDAO.update(testGroup);
    }

    static boolean assertEqualsListByContent(List<Group> firstList, List<Group> secondList) {
        if(firstList.size() != secondList.size())
            return false;
        for(int i = 0, size = firstList.size(); i < size; i++)
            if(!assertEqualsByContent(firstList.get(i), secondList.get(i)))
                return false;
        return true;
    }

    static boolean assertEqualsByContent(Group group, Group group2) {
        return group.getId() == group2.getId();
    }
}