package com.fxbank.amcwts.person;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.ParsecConfig;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.accesslevel.AccessLevel;
import com.fxbank.amcwts.person.accesslevel.AccessLevelDAO;
import com.fxbank.amcwts.person.accesslevel.ProxyAccessLevelDAO;
import com.fxbank.amcwts.person.group.Group;
import com.fxbank.amcwts.person.group.GroupDAO;
import com.fxbank.amcwts.person.group.ProxyGroupDAO;
import com.fxbank.amcwts.person.position.Position;
import com.fxbank.amcwts.person.position.PositionDAO;
import com.fxbank.amcwts.person.position.ProxyPositionDAO;
import com.fxbank.amcwts.person.worktime.ProxyWorkTimeDAO;
import com.fxbank.amcwts.person.worktime.WorkTime;
import com.fxbank.amcwts.person.worktime.WorkTimeDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 18.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class ParsecPersonDAOTest {
    private Field instance;
    private Field instanceProxyGroup;
    private Field instanceProxyPosition;
    private Field instanceProxyWorkTime;
    private Field instanceProxyAccessLevel;
    private PreparedStatement preparedStatement;
    private Person testPerson;
    private Person testPerson2;
    private Person testPerson3;
    private Person testPerson4;
    private GroupDAO groupDAO;
    private PositionDAO positionDAO;
    private WorkTimeDAO workTimeDAO;
    private AccessLevelDAO accessLevelDAO;
    private Connection connection;

    public ParsecPersonDAOTest() throws NoSuchFieldException {
        testPerson = new Person();
        instance = ParsecPersonDAO.class.getDeclaredField("PERSON_DAO");
        instance.setAccessible(true);
        instanceProxyGroup = ProxyGroupDAO.class.getDeclaredField("GROUP_DAO");
        instanceProxyGroup.setAccessible(true);
        instanceProxyPosition = ProxyPositionDAO.class.getDeclaredField("POSITION_DAO");
        instanceProxyPosition.setAccessible(true);
        instanceProxyWorkTime = ProxyWorkTimeDAO.class.getDeclaredField("WORK_TIME_DAO");
        instanceProxyWorkTime.setAccessible(true);
        instanceProxyAccessLevel = ProxyAccessLevelDAO.class.getDeclaredField("ACCESS_LEVEL_DAO");
        instanceProxyAccessLevel.setAccessible(true);

        preparedStatement = mock(PreparedStatement.class);

        groupDAO = mock(GroupDAO.class);
        positionDAO = mock(PositionDAO.class);
        workTimeDAO = mock(WorkTimeDAO.class);
        accessLevelDAO = mock(AccessLevelDAO.class);

        Group group = new Group();
        Position position = new Position();
        WorkTime workTime = new WorkTime(1, LocalTime.of(9, 0), LocalTime.of(18, 0));
        AccessLevel accessLevel = new AccessLevel();

        when(groupDAO.findById(anyLong())).thenReturn(group);
        when(workTimeDAO.findById(anyLong())).thenReturn(workTime);
        when(accessLevelDAO.findById(anyLong())).thenReturn(accessLevel);
        testPerson = new Person(-1, "PERSID", "lastName-fm", "password", "lastName", "firstName", "middleName", group,true, position, workTime);
        testPerson2 = new Person(-1, "PERSID2", "lastName-f", "password", "lastName", "firstName", "", group,false, position, workTime);
        testPerson3 = new Person(-1, "PERSID3", "lastName-f1", "password", "lastName", "firstName", null, group,true, position, workTime);
        testPerson4 = new Person(-1, "PERSID4", "lastName-f2", "password", "lastName", "firstName", "", group,false, position, workTime);

        mockStatic(ConnectionFactory.class);
        connection = mock(Connection.class);
    }

    @Before
    public void setup() throws Exception {
        reset(groupDAO, positionDAO, workTimeDAO, accessLevelDAO);
        instance.set(null, null);
        instanceProxyGroup.set(null, groupDAO);
        instanceProxyPosition.set(null, positionDAO);
        instanceProxyWorkTime.set(null, workTimeDAO);
        instanceProxyAccessLevel.set(null, accessLevelDAO);

        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));
        ParsecConfig parsecConfig = ConfigWorker.getInstance().getSystemConfig().getParsecConfig();

        String connectionString = String.format("jdbc:sqlserver://%s:%s;databaseName=Parsec3;integratedSecurity=false;", parsecConfig.getAddress(), parsecConfig.getPort());

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        PowerMockito.when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        PowerMockito.when(connection, "prepareStatement", any()).thenReturn(preparedStatement);
        PowerMockito.when(connection, "prepareStatement", any(), anyInt()).thenReturn(preparedStatement);
        PowerMockito.when(connectionFactory.getConnection(connectionString, parsecConfig.getLogin(), parsecConfig.getPassword())).thenReturn(connection);
    }

    @Test
    public void getAll() throws Exception {
        List<Person> persons = new LinkedList<>();
        persons.add(testPerson);
        persons.add(testPerson2);
        persons.add(testPerson3);
        persons.add(testPerson4);

        init4Person();

        when(groupDAO.add(any())).thenReturn(true);
        when(accessLevelDAO.add(any())).thenReturn(true);
        when(positionDAO.add(any())).thenReturn(true);
        when(workTimeDAO.add(any())).thenReturn(true);

        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(personDAO.getAll(null), persons));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithNullConFactory() throws Exception {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.getAll(null);
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionAddGroup() throws Exception {
        when(groupDAO.add(any())).thenReturn(false);

        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.getAll(null);
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionAddAccessLevel() throws Exception {
        when(groupDAO.add(any())).thenReturn(true);
        when(accessLevelDAO.add(any())).thenReturn(false);
        when(positionDAO.add(any())).thenReturn(true);
        when(workTimeDAO.add(any())).thenReturn(true);

        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.getAll(null);
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionAddPosition() throws Exception {
        when(groupDAO.add(any())).thenReturn(true);
        when(accessLevelDAO.add(any())).thenReturn(true);
        when(positionDAO.add(any())).thenReturn(false);
        when(workTimeDAO.add(any())).thenReturn(true);

        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.getAll(null);
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionAddWorkTime() throws Exception {
        when(groupDAO.add(any())).thenReturn(true);
        when(accessLevelDAO.add(any())).thenReturn(true);
        when(positionDAO.add(any())).thenReturn(true);
        when(workTimeDAO.add(any())).thenReturn(false);

        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.getAll(null);
    }


    @Test(expected = UnsupportedOperationException.class)
    public void add() {
        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.add(testPerson);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void addAll() {
        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.addAll(Collections.emptyList());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void update() {
        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.update(testPerson);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void remove() {
        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.remove(testPerson);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void contains() {
        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.contains(testPerson);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void findById() {
        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.findById(1);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void findByLogin() {
        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.findByLogin("login");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void findByPersId() {
        PersonDAO personDAO = ParsecPersonDAO.getInstance();
        personDAO.findByPersId("pers id");
    }

    static boolean assertEqualsListByContent(List<Person> firstList, List<Person> secondList) {
        if(firstList.size() != secondList.size())
            return false;
        for(int i = 0, size = firstList.size(); i < size; i++)
            if(!assertEqualsByContent(firstList.get(i), secondList.get(i)))
                return false;
        return true;
    }

    static boolean assertEqualsByContent(Person person, Person person2) {
        return person.getLogin().equalsIgnoreCase(person2.getLogin()) && person.getPersonId().equalsIgnoreCase(person2.getPersonId()) && person.isAdmin() == person2.isAdmin();
    }

    void init4Person() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);
        PowerMockito.when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        PowerMockito.when(resultSet, "getString", "PERS_ID")
                .thenReturn(testPerson.getPersonId())
                .thenReturn(testPerson2.getPersonId())
                .thenReturn(testPerson3.getPersonId())
                .thenReturn(testPerson4.getPersonId())
                .thenReturn(null);
        PowerMockito.when(resultSet, "getString", "ORG_ID")
                .thenReturn("21353DAE-CB18-4C59-8CF3-0017C563314E")
                .thenReturn("TEST")
                .thenReturn("21353DAE-CB18-4C59-8CF3-0017C563314E")
                .thenReturn("TEST")
                .thenReturn(null);
        PowerMockito.when(resultSet, "getString", "LAST_NAME")
                .thenReturn(testPerson.getLastName())
                .thenReturn(testPerson2.getLastName())
                .thenReturn(testPerson3.getLastName())
                .thenReturn(testPerson4.getLastName())
                .thenReturn(null);
        PowerMockito.when(resultSet, "getString", "FIRST_NAME")
                .thenReturn(testPerson.getFirstName())
                .thenReturn(testPerson2.getFirstName())
                .thenReturn(testPerson3.getFirstName())
                .thenReturn(testPerson4.getFirstName())
                .thenReturn(null);
        PowerMockito.when(resultSet, "getString", "MIDDLE_NAME")
                .thenReturn(testPerson.getMiddleName())
                .thenReturn(testPerson2.getMiddleName())
                .thenReturn(testPerson3.getMiddleName())
                .thenReturn(testPerson4.getMiddleName())
                .thenReturn(null);

        PowerMockito.when(preparedStatement, "executeQuery").thenReturn(resultSet);
    }
}