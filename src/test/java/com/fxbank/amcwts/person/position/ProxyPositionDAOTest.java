package com.fxbank.amcwts.person.position;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.CommonTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 10.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, MySQLPositionDAO.class})
public class ProxyPositionDAOTest {
    private Field instance;
    private SystemConfig systemConfig;
    private PositionDAO mocPositionDAO;
    private Position testPosition;

    public ProxyPositionDAOTest() throws NoSuchFieldException {
        instance = ProxyPositionDAO.class.getDeclaredField("POSITION_DAO");
        systemConfig = mock(SystemConfig.class);
        mocPositionDAO = mock(MySQLPositionDAO.class);
        testPosition = new Position();
    }

    @Before
    public void setUp() throws Exception {
        instance.setAccessible(true);
        instance.set(null, null);

        Mockito.reset(systemConfig);
        Mockito.reset(mocPositionDAO);

        PowerMockito.mockStatic(ConfigWorker.class);
        PowerMockito.mockStatic(MySQLPositionDAO.class);

        ConfigWorker configWorker = PowerMockito.mock(ConfigWorker.class);
        PowerMockito.when(configWorker.getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(ConfigWorker.getInstance()).thenReturn(configWorker);

        when(ConfigWorker.getInstance().getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(MySQLPositionDAO.getInstance()).thenReturn(mocPositionDAO);
    }

    @Test
    public void currentDBIsCorrect() throws Exception {
        PowerMockito.when(mocPositionDAO.getAll()).thenReturn(Collections.emptyList());
        PowerMockito.when(mocPositionDAO.getAll(CommonTest.testPerson)).thenReturn(Collections.emptyList());
        PowerMockito.when(mocPositionDAO.add(testPosition)).thenReturn(false);
        PowerMockito.when(mocPositionDAO.remove(testPosition)).thenReturn(false);
        PowerMockito.when(mocPositionDAO.update(testPosition)).thenReturn(false);
        PowerMockito.when(mocPositionDAO.contains(testPosition)).thenReturn(false);
        PowerMockito.when(mocPositionDAO.findById(0)).thenReturn(null);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        PositionDAO positionDAO = ProxyPositionDAO.getInstance();

        CommonTest.testOnDefault(positionDAO, testPosition, Collections.emptyList(),false, null);
    }

    @Test
    public void currentDBAndCorrectVal() throws Exception {
        List<Position> positions = new LinkedList<>();
        positions.add(new Position());
        PowerMockito.when(mocPositionDAO.getAll()).thenReturn(positions);
        PowerMockito.when(mocPositionDAO.getAll(CommonTest.testPerson)).thenReturn(positions);
        PowerMockito.when(mocPositionDAO.add(testPosition)).thenReturn(true);
        PowerMockito.when(mocPositionDAO.remove(testPosition)).thenReturn(true);
        PowerMockito.when(mocPositionDAO.update(testPosition)).thenReturn(true);
        PowerMockito.when(mocPositionDAO.contains(testPosition)).thenReturn(true);
        PowerMockito.when(mocPositionDAO.findById(0)).thenReturn(testPosition);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        PositionDAO positionDAO = ProxyPositionDAO.getInstance();

        CommonTest.testOnDefault(positionDAO, testPosition, positions,true, testPosition);
    }

    @Test
    public void currentDBIsNull() {
        when(systemConfig.getCurrentDB()).thenReturn(null);
        PositionDAO positionDAO = ProxyPositionDAO.getInstance();
        CommonTest.testOnDefault(positionDAO, testPosition, Collections.emptyList(), false, null);
    }

    @Test
    public void currentDBIsEmpty() {
        when(systemConfig.getCurrentDB()).thenReturn("");
        PositionDAO positionDAO = ProxyPositionDAO.getInstance();
        CommonTest.testOnDefault(positionDAO, testPosition, Collections.emptyList(), false, null);
    }

    @Test
    public void existingInstanceTest() throws Exception {
        PositionDAO proxyPositionDAO = ProxyPositionDAO.getInstance();

        instance.set(null, proxyPositionDAO);

        Assert.assertEquals(proxyPositionDAO, ProxyPositionDAO.getInstance());
    }
}