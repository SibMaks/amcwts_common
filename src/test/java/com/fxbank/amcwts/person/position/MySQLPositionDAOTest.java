package com.fxbank.amcwts.person.position;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.accesslevel.AccessLevel;
import com.fxbank.amcwts.person.accesslevel.AccessLevelDAO;
import com.fxbank.amcwts.person.accesslevel.ProxyAccessLevelDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.powermock.api.mockito.PowerMockito.*;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 17.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class MySQLPositionDAOTest {
    private Field instance;
    private Field instanceProxyAccessLevel;
    private Connection connection;
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);
    private AccessLevel testAccessLevel = new AccessLevel(1, "Test", 2);
    private Position testPosition = new Position(-1, "Test name", testAccessLevel);
    private AccessLevelDAO mockAccessLevelDAO = mock(AccessLevelDAO.class);

    public MySQLPositionDAOTest() throws SQLException, NoSuchFieldException {
        instance = MySQLPositionDAO.class.getDeclaredField("POSITION_DAO");
        instance.setAccessible(true);
        instanceProxyAccessLevel = ProxyAccessLevelDAO.class.getDeclaredField("ACCESS_LEVEL_DAO");
        instanceProxyAccessLevel.setAccessible(true);
        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));

        mockStatic(ConnectionFactory.class);

        connection = mock(Connection.class);

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenReturn(connection);

        when(mockAccessLevelDAO.findById(anyInt())).thenReturn(testAccessLevel);
    }

    @Before
    public void before() throws Exception {
        instance.set(null, null);
        instanceProxyAccessLevel.set(null, mockAccessLevelDAO);

        Mockito.reset(connection);
        when(connection, "prepareStatement", any()).thenReturn(preparedStatement);
        when(connection, "prepareStatement", any(), anyInt()).thenReturn(preparedStatement);

        testPosition.setId(-1);
        testPosition.setName("Test name");
    }

    @Test
    public void add() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertTrue(positionDAO.add(testPosition));
        /*verify(preparedStatement).setString(1, "Test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setInt(3, 1);
        verify(preparedStatement).getGeneratedKeys();
        verify(generatedKey).getLong(1);*/
        Assert.assertEquals(testPosition.getId(), 1);
    }

    @Test
    public void addWithErrorOfGeneratedKey() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(false);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertFalse(positionDAO.add(testPosition));
    }

    @Test
    public void addWithErrorOfAddingInDB() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(0);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertFalse(positionDAO.add(testPosition));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionUpdate() throws Exception {
        testPosition.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertFalse(positionDAO.add(testPosition));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionGetConnection() throws Exception {
        testPosition.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertFalse(positionDAO.add(testPosition));
    }

    @Test
    public void update() throws Exception {
        add();
        testPosition.setId(1);
        testPosition.setName("New test name");

        when(preparedStatement, "executeUpdate").thenReturn(1);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertTrue(positionDAO.update(testPosition));
        /*verify(preparedStatement).setString(1, "New test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setLong(3, 1);*/
    }

    @Test
    public void updateWithUpdateErrorInDB() throws Exception {
        testPosition.setId(1);
        testPosition.setName("New test name");

        when(preparedStatement, "executeUpdate").thenReturn(0);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertFalse(positionDAO.update(testPosition));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionUpdate() throws Exception {
        testPosition.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertFalse(positionDAO.update(testPosition));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionGetConnection() throws Exception {
        testPosition.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertFalse(positionDAO.update(testPosition));
    }

    @Test
    public void remove() throws Exception {
        testPosition.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertTrue(positionDAO.remove(testPosition));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test
    public void removeWithErrorInDB() throws Exception {
        testPosition.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertFalse(positionDAO.remove(testPosition));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionUpdate() throws Exception {
        testPosition.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertFalse(positionDAO.remove(testPosition));
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionGetConnection() throws Exception {
        testPosition.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertFalse(positionDAO.remove(testPosition));
    }

    @Test
    public void getAll() throws Exception {
        testPosition.setId(1);
        List<Position> positionList = new LinkedList<>();
        positionList.add(testPosition);
        Position testPosition2 = new Position(2, "test2", testAccessLevel);
        positionList.add(testPosition2);
        Position testPosition3 = new Position(3, null, testAccessLevel);
        positionList.add(testPosition3);
        Position testPosition4 = new Position(4, "test4", testAccessLevel);
        positionList.add(testPosition4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "POSITION_id")
                .thenReturn(testPosition.getId())
                .thenReturn(testPosition2.getId())
                .thenReturn(testPosition3.getId())
                .thenReturn(testPosition4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "POSITION_name")
                .thenReturn(testPosition.getName())
                .thenReturn(testPosition2.getName())
                .thenReturn(testPosition3.getName())
                .thenReturn(testPosition4.getName())
                .thenReturn(null);
        when(resultSet, "getLong", "ACCESS_LEVEL_id")
                .thenReturn(testAccessLevel.getId());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(positionDAO.getAll(), positionList));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionQuery() throws Exception {
        testPosition.setId(1);

        when(preparedStatement, "executeQuery").thenThrow(new SQLException());

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        positionDAO.getAll();
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithGetConnectionException() throws Exception {
        testPosition.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        positionDAO.getAll();
    }

    @Test
    public void getAllByPerson() throws Exception {
        testPosition.setId(1);
        List<Position> positionList = new LinkedList<>();
        positionList.add(testPosition);
        Position testPosition2 = new Position(2, "test2", testAccessLevel);
        positionList.add(testPosition2);
        Position testPosition3 = new Position(3, null, testAccessLevel);
        positionList.add(testPosition3);
        Position testPosition4 = new Position(4, "test4", testAccessLevel);
        positionList.add(testPosition4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "POSITION_id")
                .thenReturn(testPosition.getId())
                .thenReturn(testPosition2.getId())
                .thenReturn(testPosition3.getId())
                .thenReturn(testPosition4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "POSITION_name")
                .thenReturn(testPosition.getName())
                .thenReturn(testPosition2.getName())
                .thenReturn(testPosition3.getName())
                .thenReturn(testPosition4.getName())
                .thenReturn(null);
        when(resultSet, "getLong", "ACCESS_LEVEL_id")
                .thenReturn(testAccessLevel.getId());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(positionDAO.getAll(null), positionList));
    }


    @Test
    public void findById() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L).thenReturn(2L).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        testPosition.setId(1);
        Position testPosition2 = new Position(2, "test2", testAccessLevel);
        Position testPosition3 = new Position(3, null, testAccessLevel);
        Position testPosition4 = new Position(4, "test4", testAccessLevel);
        positionDAO.add(testPosition);
        positionDAO.add(testPosition2);
        positionDAO.add(testPosition4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true);

        when(resultSet, "getLong", "POSITION_id")
                .thenReturn(testPosition3.getId());
        when(resultSet, "getString", "POSITION_name")
                .thenReturn(testPosition3.getName());
        when(resultSet, "getLong", "ACCESS_LEVEL_id")
                .thenReturn(testAccessLevel.getId());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Assert.assertEquals(positionDAO.findById(1), testPosition);
        Assert.assertEquals(positionDAO.findById(2), testPosition2);
        Assert.assertTrue(assertEqualsByContent(positionDAO.findById(3), testPosition3));
        Assert.assertEquals(positionDAO.findById(4), testPosition4);
    }

    @Test(expected = RuntimeException.class)
    public void addWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        positionDAO.add(testPosition);
    }

    @Test(expected = RuntimeException.class)
    public void removeWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        positionDAO.remove(testPosition);
    }

    @Test(expected = RuntimeException.class)
    public void updateWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        PositionDAO positionDAO = MySQLPositionDAO.getInstance();
        positionDAO.update(testPosition);
    }

    static boolean assertEqualsListByContent(List<Position> firstList, List<Position> secondList) {
        if(firstList.size() != secondList.size())
            return false;
        for(int i = 0, size = firstList.size(); i < size; i++)
            if(!assertEqualsByContent(firstList.get(i), secondList.get(i)))
                return false;
        return true;
    }

    static boolean assertEqualsByContent(Position position, Position position2) {
        return position.getId() == position2.getId();
    }
}