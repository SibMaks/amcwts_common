package com.fxbank.amcwts.person.accesslevel;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.powermock.api.mockito.PowerMockito.*;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 12.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class MySQLAccessLevelDAOTest {
    private Field instance;
    private Connection connection;
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);
    private AccessLevel testAccessLevel = new AccessLevel(-1, "Test name", 2);

    public MySQLAccessLevelDAOTest() throws SQLException, NoSuchFieldException {
        instance = MySQLAccessLevelDAO.class.getDeclaredField("ACCESS_LEVEL_DAO");
        instance.setAccessible(true);
        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));
        mockStatic(ConnectionFactory.class);
        connection = mock(Connection.class);

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenReturn(connection);
    }

    @Before
    public void before() throws Exception {
        instance.set(null, null);
        Mockito.reset(connection);
        when(connection, "prepareStatement", any()).thenReturn(preparedStatement);
        when(connection, "prepareStatement", any(), anyInt()).thenReturn(preparedStatement);

        testAccessLevel.setId(-1);
        testAccessLevel.setName("Test name");
        testAccessLevel.setLevel(2);
    }

    @Test
    public void add() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertTrue(accessLevelDAO.add(testAccessLevel));
        /*verify(preparedStatement).setString(1, "Test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setInt(3, 1);
        verify(preparedStatement).getGeneratedKeys();
        verify(generatedKey).getLong(1);*/
        Assert.assertEquals(testAccessLevel.getId(), 1);
    }

    @Test
    public void addWithErrorOfGeneratedKey() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(false);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertFalse(accessLevelDAO.add(testAccessLevel));
    }

    @Test
    public void addWithErrorOfAddingInDB() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(0);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertFalse(accessLevelDAO.add(testAccessLevel));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionUpdate() throws Exception {
        testAccessLevel.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertFalse(accessLevelDAO.add(testAccessLevel));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionGetConnection() throws Exception {
        testAccessLevel.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertFalse(accessLevelDAO.add(testAccessLevel));
    }

    @Test
    public void update() throws Exception {
        add();
        testAccessLevel.setId(1);
        testAccessLevel.setName("New test name");

        when(preparedStatement, "executeUpdate").thenReturn(1);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertTrue(accessLevelDAO.update(testAccessLevel));
        /*verify(preparedStatement).setString(1, "New test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setLong(3, 1);*/
    }

    @Test
    public void updateWithUpdateErrorInDB() throws Exception {
        testAccessLevel.setId(1);
        testAccessLevel.setName("New test name");

        when(preparedStatement, "executeUpdate").thenReturn(0);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertFalse(accessLevelDAO.update(testAccessLevel));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionUpdate() throws Exception {
        testAccessLevel.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertFalse(accessLevelDAO.update(testAccessLevel));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionGetConnection() throws Exception {
        testAccessLevel.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertFalse(accessLevelDAO.update(testAccessLevel));
    }

    @Test
    public void remove() throws Exception {
        testAccessLevel.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertTrue(accessLevelDAO.remove(testAccessLevel));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test
    public void removeWithErrorInDB() throws Exception {
        testAccessLevel.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertFalse(accessLevelDAO.remove(testAccessLevel));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionUpdate() throws Exception {
        testAccessLevel.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertFalse(accessLevelDAO.remove(testAccessLevel));
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionGetConnection() throws Exception {
        testAccessLevel.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertFalse(accessLevelDAO.remove(testAccessLevel));
    }

    @Test
    public void getAll() throws Exception {
        testAccessLevel.setId(1);
        List<AccessLevel> accessLevelList = new LinkedList<>();
        accessLevelList.add(testAccessLevel);
        AccessLevel testAccessLevel2 = new AccessLevel(2, "test2", 2);
        accessLevelList.add(testAccessLevel2);
        AccessLevel testAccessLevel3 = new AccessLevel(3, null, 0);
        accessLevelList.add(testAccessLevel3);
        AccessLevel testAccessLevel4 = new AccessLevel(4, "test4", 1);
        accessLevelList.add(testAccessLevel4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "ACCESS_LEVEL_id")
                .thenReturn(testAccessLevel.getId())
                .thenReturn(testAccessLevel2.getId())
                .thenReturn(testAccessLevel3.getId())
                .thenReturn(testAccessLevel4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "ACCESS_LEVEL_name")
                .thenReturn(testAccessLevel.getName())
                .thenReturn(testAccessLevel2.getName())
                .thenReturn(testAccessLevel3.getName())
                .thenReturn(testAccessLevel4.getName())
                .thenReturn(null);
        when(resultSet, "getInt", "ACCESS_LEVEL")
                .thenReturn(testAccessLevel.getLevel())
                .thenReturn(testAccessLevel2.getLevel())
                .thenReturn(testAccessLevel3.getLevel())
                .thenReturn(testAccessLevel4.getLevel())
                .thenReturn(-1);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(accessLevelDAO.getAll(), accessLevelList));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionQuery() throws Exception {
        testAccessLevel.setId(1);

        when(preparedStatement, "executeQuery").thenThrow(new SQLException());

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        accessLevelDAO.getAll();
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithGetConnectionException() throws Exception {
        testAccessLevel.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        accessLevelDAO.getAll();
    }

    @Test
    public void getAllByPerson() throws Exception {
        testAccessLevel.setId(1);
        List<AccessLevel> accessLevelList = new LinkedList<>();
        accessLevelList.add(testAccessLevel);
        AccessLevel testAccessLevel2 = new AccessLevel(2, "test2", 2);
        accessLevelList.add(testAccessLevel2);
        AccessLevel testAccessLevel3 = new AccessLevel(3, null, 0);
        accessLevelList.add(testAccessLevel3);
        AccessLevel testAccessLevel4 = new AccessLevel(4, "test4", 1);
        accessLevelList.add(testAccessLevel4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "ACCESS_LEVEL_id")
                .thenReturn(testAccessLevel.getId())
                .thenReturn(testAccessLevel2.getId())
                .thenReturn(testAccessLevel3.getId())
                .thenReturn(testAccessLevel4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "ACCESS_LEVEL_name")
                .thenReturn(testAccessLevel.getName())
                .thenReturn(testAccessLevel2.getName())
                .thenReturn(testAccessLevel3.getName())
                .thenReturn(testAccessLevel4.getName())
                .thenReturn(null);
        when(resultSet, "getInt", "ACCESS_LEVEL")
                .thenReturn(testAccessLevel.getLevel())
                .thenReturn(testAccessLevel2.getLevel())
                .thenReturn(testAccessLevel3.getLevel())
                .thenReturn(testAccessLevel4.getLevel())
                .thenReturn(-1);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(accessLevelDAO.getAll(null), accessLevelList));
    }


    @Test
    public void findById() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L).thenReturn(2L).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        testAccessLevel.setId(1);
        AccessLevel testAccessLevel2 = new AccessLevel(2, "test2", 2);
        AccessLevel testAccessLevel3 = new AccessLevel(3, null, 0);
        AccessLevel testAccessLevel4 = new AccessLevel(4, "test4", 1);
        accessLevelDAO.add(testAccessLevel);
        accessLevelDAO.add(testAccessLevel2);
        accessLevelDAO.add(testAccessLevel4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true);

        when(resultSet, "getLong", "ACCESS_LEVEL_id")
                .thenReturn(testAccessLevel3.getId());
        when(resultSet, "getString", "ACCESS_LEVEL_name")
                .thenReturn(testAccessLevel3.getName());
        when(resultSet, "getInt", "ACCESS_LEVEL")
                .thenReturn(testAccessLevel3.getLevel());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Assert.assertEquals(accessLevelDAO.findById(1), testAccessLevel);
        Assert.assertEquals(accessLevelDAO.findById(2), testAccessLevel2);
        Assert.assertTrue(assertEqualsByContent(accessLevelDAO.findById(3), testAccessLevel3));
        Assert.assertEquals(accessLevelDAO.findById(4), testAccessLevel4);
    }

    @Test(expected = RuntimeException.class)
    public void addWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        accessLevelDAO.add(testAccessLevel);
    }

    @Test(expected = RuntimeException.class)
    public void removeWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        accessLevelDAO.remove(testAccessLevel);
    }

    @Test(expected = RuntimeException.class)
    public void updateWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        AccessLevelDAO accessLevelDAO = MySQLAccessLevelDAO.getInstance();
        accessLevelDAO.update(testAccessLevel);
    }

    static boolean assertEqualsListByContent(List<AccessLevel> firstList, List<AccessLevel> secondList) {
        if(firstList.size() != secondList.size())
            return false;
        for(int i = 0, size = firstList.size(); i < size; i++)
            if(!assertEqualsByContent(firstList.get(i), secondList.get(i)))
                return false;
        return true;
    }

    static boolean assertEqualsByContent(AccessLevel accessLevel, AccessLevel accessLevel2) {
        return accessLevel.getId() == accessLevel2.getId();
    }
}