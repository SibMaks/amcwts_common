package com.fxbank.amcwts.person.accesslevel;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.CommonTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.powermock.api.mockito.PowerMockito.*;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 04.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, MySQLAccessLevelDAO.class})
public class ProxyAccessLevelDAOTest {
    private Field instance;
    private SystemConfig systemConfig = mock(SystemConfig.class);
    private AccessLevelDAO mockAccessLevelDAO = mock(MySQLAccessLevelDAO.class);
    private AccessLevel testAccessLevel = new AccessLevel();

    public ProxyAccessLevelDAOTest() throws NoSuchFieldException {
        instance = ProxyAccessLevelDAO.class.getDeclaredField("ACCESS_LEVEL_DAO");
        systemConfig = mock(SystemConfig.class);
        mockAccessLevelDAO = mock(MySQLAccessLevelDAO.class);
        testAccessLevel = new AccessLevel();
    }

    @Before
    public void setUp() throws Exception {
        instance.setAccessible(true);
        instance.set(null, null);

        Mockito.reset(systemConfig);
        Mockito.reset(mockAccessLevelDAO);

        PowerMockito.mockStatic(ConfigWorker.class);
        PowerMockito.mockStatic(MySQLAccessLevelDAO.class);

        ConfigWorker configWorker = PowerMockito.mock(ConfigWorker.class);
        PowerMockito.when(configWorker.getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(ConfigWorker.getInstance()).thenReturn(configWorker);

        when(ConfigWorker.getInstance().getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(MySQLAccessLevelDAO.getInstance()).thenReturn(mockAccessLevelDAO);
    }

    @Test
    public void currentDBIsCorrect() throws Exception {
        PowerMockito.when(mockAccessLevelDAO.getAll()).thenReturn(Collections.emptyList());
        PowerMockito.when(mockAccessLevelDAO.getAll(CommonTest.testPerson)).thenReturn(Collections.emptyList());
        PowerMockito.when(mockAccessLevelDAO.add(testAccessLevel)).thenReturn(false);
        PowerMockito.when(mockAccessLevelDAO.remove(testAccessLevel)).thenReturn(false);
        PowerMockito.when(mockAccessLevelDAO.update(testAccessLevel)).thenReturn(false);
        PowerMockito.when(mockAccessLevelDAO.contains(testAccessLevel)).thenReturn(false);
        PowerMockito.when(mockAccessLevelDAO.findById(0)).thenReturn(null);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        AccessLevelDAO accessLevelDAO = ProxyAccessLevelDAO.getInstance();

        CommonTest.testOnDefault(accessLevelDAO, testAccessLevel, Collections.emptyList(),false, null);
    }

    @Test
    public void currentDBAndCorrectVal() throws Exception {
        List<AccessLevel> accessLevels = new LinkedList<>();
        accessLevels.add(new AccessLevel());
        PowerMockito.when(mockAccessLevelDAO.getAll()).thenReturn(accessLevels);
        PowerMockito.when(mockAccessLevelDAO.getAll(CommonTest.testPerson)).thenReturn(accessLevels);
        PowerMockito.when(mockAccessLevelDAO.add(testAccessLevel)).thenReturn(true);
        PowerMockito.when(mockAccessLevelDAO.remove(testAccessLevel)).thenReturn(true);
        PowerMockito.when(mockAccessLevelDAO.update(testAccessLevel)).thenReturn(true);
        PowerMockito.when(mockAccessLevelDAO.contains(testAccessLevel)).thenReturn(true);
        PowerMockito.when(mockAccessLevelDAO.findById(0)).thenReturn(testAccessLevel);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        AccessLevelDAO accessLevelDAO = ProxyAccessLevelDAO.getInstance();

        CommonTest.testOnDefault(accessLevelDAO, testAccessLevel, accessLevels,true, testAccessLevel);
    }

    @Test
    public void currentDBIsNull() {
        when(systemConfig.getCurrentDB()).thenReturn(null);
        AccessLevelDAO accessLevelDAO = ProxyAccessLevelDAO.getInstance();
        CommonTest.testOnDefault(accessLevelDAO, testAccessLevel, Collections.emptyList(), false, null);
    }

    @Test
    public void currentDBIsEmpty() {
        when(systemConfig.getCurrentDB()).thenReturn("");
        AccessLevelDAO accessLevelDAO = ProxyAccessLevelDAO.getInstance();
        CommonTest.testOnDefault(accessLevelDAO, testAccessLevel, Collections.emptyList(), false, null);
    }

    @Test
    public void existingInstanceTest() throws Exception {
        AccessLevelDAO accessLevelDAO = ProxyAccessLevelDAO.getInstance();

        instance.set(null, accessLevelDAO);

        Assert.assertEquals(accessLevelDAO, ProxyAccessLevelDAO.getInstance());
    }
}