package com.fxbank.amcwts.person.worktime;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.powermock.api.mockito.PowerMockito.*;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 17.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, ResultSet.class, ConnectionFactory.class})
public class MySQLWorkTimeDAOTest {
    private Field instance;
    private Connection connection;
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);
    private LocalTime localTime = LocalTime.of(9, 0);
    private LocalTime localTime2 = LocalTime.of(18, 0);
    private WorkTime testWorkTime = new WorkTime(-1, localTime, localTime2);

    public MySQLWorkTimeDAOTest() throws SQLException, NoSuchFieldException {
        instance = MySQLWorkTimeDAO.class.getDeclaredField("WORK_TIME_DAO");
        instance.setAccessible(true);
        ConfigWorker.getInstance().init(new File(getClass().getResource("/default.conf.json").getFile()));
        mockStatic(ConnectionFactory.class);
        connection = mock(Connection.class);

        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenReturn(connection);
    }

    @Before
    public void before() throws Exception {
        instance.set(null, null);
        Mockito.reset(connection);
        when(connection, "prepareStatement", any()).thenReturn(preparedStatement);
        when(connection, "prepareStatement", any(), anyInt()).thenReturn(preparedStatement);

        testWorkTime.setId(-1);
        testWorkTime.setTimeStart(localTime);
        testWorkTime.setTimeEnd(localTime2);
    }

    @Test
    public void add() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertTrue(workTimeDAO.add(testWorkTime));
        /*verify(preparedStatement).setString(1, "Test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setInt(3, 1);
        verify(preparedStatement).getGeneratedKeys();
        verify(generatedKey).getLong(1);*/
        Assert.assertEquals(testWorkTime.getId(), 1);
    }

    @Test
    public void addWithErrorOfGeneratedKey() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(false);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertFalse(workTimeDAO.add(testWorkTime));
    }

    @Test
    public void addWithErrorOfAddingInDB() throws Exception {
        when(preparedStatement, "executeUpdate").thenReturn(0);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertFalse(workTimeDAO.add(testWorkTime));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionUpdate() throws Exception {
        testWorkTime.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertFalse(workTimeDAO.add(testWorkTime));
    }

    @Test(expected = RuntimeException.class)
    public void addWithExceptionGetConnection() throws Exception {
        testWorkTime.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertFalse(workTimeDAO.add(testWorkTime));
    }

    @Test
    public void update() throws Exception {
        add();
        testWorkTime.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertTrue(workTimeDAO.update(testWorkTime));
        /*verify(preparedStatement).setString(1, "New test name");
        verify(preparedStatement).setInt(2, 2);
        verify(preparedStatement).setLong(3, 1);*/
    }

    @Test
    public void updateWithUpdateErrorInDB() throws Exception {
        testWorkTime.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertFalse(workTimeDAO.update(testWorkTime));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionUpdate() throws Exception {
        testWorkTime.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertFalse(workTimeDAO.update(testWorkTime));
    }

    @Test(expected = RuntimeException.class)
    public void updateWithExceptionGetConnection() throws Exception {
        testWorkTime.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertFalse(workTimeDAO.update(testWorkTime));
    }

    @Test
    public void remove() throws Exception {
        testWorkTime.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(1);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertTrue(workTimeDAO.remove(testWorkTime));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test
    public void removeWithErrorInDB() throws Exception {
        testWorkTime.setId(1);

        when(preparedStatement, "executeUpdate").thenReturn(0);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertFalse(workTimeDAO.remove(testWorkTime));
        /*verify(preparedStatement).setLong(1, 1);*/
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionUpdate() throws Exception {
        testWorkTime.setId(1);

        when(preparedStatement, "executeUpdate").thenThrow(new SQLException());

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertFalse(workTimeDAO.remove(testWorkTime));
    }

    @Test(expected = RuntimeException.class)
    public void removeWithExceptionGetConnection() throws Exception {
        testWorkTime.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertFalse(workTimeDAO.remove(testWorkTime));
    }

    @Test
    public void getAll() throws Exception {
        testWorkTime.setId(1);
        List<WorkTime> workTimeList = new LinkedList<>();
        workTimeList.add(testWorkTime);
        WorkTime testWorkTime2 = new WorkTime(2, localTime, localTime2);
        workTimeList.add(testWorkTime2);
        WorkTime testWorkTime3 = new WorkTime(3, localTime, localTime2);
        workTimeList.add(testWorkTime3);
        WorkTime testWorkTime4 = new WorkTime(4, localTime, localTime2);
        workTimeList.add(testWorkTime4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "WORK_TIME_id")
                .thenReturn(testWorkTime.getId())
                .thenReturn(testWorkTime2.getId())
                .thenReturn(testWorkTime3.getId())
                .thenReturn(testWorkTime4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "TIME_START")
                .thenReturn(testWorkTime.getTimeStart().toString())
                .thenReturn(testWorkTime2.getTimeStart().toString())
                .thenReturn(testWorkTime3.getTimeStart().toString())
                .thenReturn(testWorkTime4.getTimeStart().toString())
                .thenReturn(null);
        when(resultSet, "getString", "TIME_END")
                .thenReturn(testWorkTime.getTimeEnd().toString())
                .thenReturn(testWorkTime2.getTimeEnd().toString())
                .thenReturn(testWorkTime3.getTimeEnd().toString())
                .thenReturn(testWorkTime4.getTimeEnd().toString())
                .thenReturn(null);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(workTimeDAO.getAll(), workTimeList));
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithExceptionQuery() throws Exception {
        testWorkTime.setId(1);

        when(preparedStatement, "executeQuery").thenThrow(new SQLException());

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        workTimeDAO.getAll();
    }

    @Test(expected = RuntimeException.class)
    public void getAllWithGetConnectionException() throws Exception {
        testWorkTime.setId(1);
        ConnectionFactory connectionFactory = mock(ConnectionFactory.class);
        when(ConnectionFactory.getInstance()).thenReturn(connectionFactory);
        when(connectionFactory.getConnection()).thenThrow(new SQLException());

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        workTimeDAO.getAll();
    }

    @Test
    public void getAllByPerson() throws Exception {
        testWorkTime.setId(1);
        List<WorkTime> workTimeList = new LinkedList<>();
        workTimeList.add(testWorkTime);
        WorkTime testWorkTime2 = new WorkTime(2, localTime, localTime2);
        workTimeList.add(testWorkTime2);
        WorkTime testWorkTime3 = new WorkTime(3, localTime, localTime2);
        workTimeList.add(testWorkTime3);
        WorkTime testWorkTime4 = new WorkTime(4, localTime, localTime2);
        workTimeList.add(testWorkTime4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet, "getLong", "WORK_TIME_id")
                .thenReturn(testWorkTime.getId())
                .thenReturn(testWorkTime2.getId())
                .thenReturn(testWorkTime3.getId())
                .thenReturn(testWorkTime4.getId())
                .thenReturn(-1L);
        when(resultSet, "getString", "TIME_START")
                .thenReturn(testWorkTime.getTimeStart().toString())
                .thenReturn(testWorkTime2.getTimeStart().toString())
                .thenReturn(testWorkTime3.getTimeStart().toString())
                .thenReturn(testWorkTime4.getTimeStart().toString())
                .thenReturn(null);
        when(resultSet, "getString", "TIME_END")
                .thenReturn(testWorkTime.getTimeEnd().toString())
                .thenReturn(testWorkTime2.getTimeEnd().toString())
                .thenReturn(testWorkTime3.getTimeEnd().toString())
                .thenReturn(testWorkTime4.getTimeEnd().toString())
                .thenReturn(null);

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        Assert.assertTrue(assertEqualsListByContent(workTimeDAO.getAll(null), workTimeList));
    }


    @Test
    public void findById() throws Exception {
        ResultSet generatedKey = mock(ResultSet.class);
        when(generatedKey, "next").thenReturn(true);
        when(generatedKey, "getLong", 1).thenReturn(1L).thenReturn(2L).thenReturn(4L);

        when(preparedStatement, "getGeneratedKeys").thenReturn(generatedKey);
        when(preparedStatement, "executeUpdate").thenReturn(1);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        testWorkTime.setId(1);
        WorkTime testWorkTime2 = new WorkTime(2, localTime, localTime2);
        WorkTime testWorkTime3 = new WorkTime(3, localTime, localTime2);
        WorkTime testWorkTime4 = new WorkTime(4, localTime, localTime2);
        workTimeDAO.add(testWorkTime);
        workTimeDAO.add(testWorkTime2);
        workTimeDAO.add(testWorkTime4);

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet, "next").thenReturn(true);

        when(resultSet, "getLong", "WORK_TIME_id")
                .thenReturn(testWorkTime3.getId());
        when(resultSet, "getString", "TIME_START")
                .thenReturn(testWorkTime3.getTimeStart().toString());
        when(resultSet, "getString", "TIME_END")
                .thenReturn(testWorkTime3.getTimeEnd().toString());

        when(preparedStatement, "executeQuery").thenReturn(resultSet);

        Assert.assertEquals(workTimeDAO.findById(1), testWorkTime);
        Assert.assertEquals(workTimeDAO.findById(2), testWorkTime2);
        Assert.assertTrue(assertEqualsByContent(workTimeDAO.findById(3), testWorkTime3));
        Assert.assertEquals(workTimeDAO.findById(4), testWorkTime4);
    }

    @Test(expected = RuntimeException.class)
    public void addWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        workTimeDAO.add(testWorkTime);
    }

    @Test(expected = RuntimeException.class)
    public void removeWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        workTimeDAO.remove(testWorkTime);
    }

    @Test(expected = RuntimeException.class)
    public void updateWithNullConFactory() {
        when(ConnectionFactory.getInstance()).thenReturn(null);

        WorkTimeDAO workTimeDAO = MySQLWorkTimeDAO.getInstance();
        workTimeDAO.update(testWorkTime);
    }

    static boolean assertEqualsListByContent(List<WorkTime> firstList, List<WorkTime> secondList) {
        if(firstList.size() != secondList.size())
            return false;
        for(int i = 0, size = firstList.size(); i < size; i++)
            if(!assertEqualsByContent(firstList.get(i), secondList.get(i)))
                return false;
        return true;
    }

    static boolean assertEqualsByContent(WorkTime workTime, WorkTime workTime2) {
        return workTime.getId() == workTime2.getId();
    }
}