package com.fxbank.amcwts.person.worktime;

import com.fxbank.amcwts.CommonTest;
import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.time.LocalTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 10.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, MySQLWorkTimeDAO.class})
public class ProxyWorkTimeDAOTest {
    private Field instance;
    private SystemConfig systemConfig;
    private WorkTimeDAO mockWorkTimeDAO;
    private WorkTime testWorkTime;

    public ProxyWorkTimeDAOTest() throws NoSuchFieldException {
        instance = ProxyWorkTimeDAO.class.getDeclaredField("WORK_TIME_DAO");
        systemConfig = mock(SystemConfig.class);
        mockWorkTimeDAO = mock(MySQLWorkTimeDAO.class);
        testWorkTime = new WorkTime(1, LocalTime.of(0, 0), LocalTime.of(2, 0));
    }

    @Before
    public void setUp() throws Exception {
        instance.setAccessible(true);
        instance.set(null, null);

        Mockito.reset(systemConfig);
        Mockito.reset(mockWorkTimeDAO);

        PowerMockito.mockStatic(ConfigWorker.class);
        PowerMockito.mockStatic(MySQLWorkTimeDAO.class);

        ConfigWorker configWorker = PowerMockito.mock(ConfigWorker.class);
        PowerMockito.when(configWorker.getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(ConfigWorker.getInstance()).thenReturn(configWorker);

        when(ConfigWorker.getInstance().getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(MySQLWorkTimeDAO.getInstance()).thenReturn(mockWorkTimeDAO);
    }

    @Test
    public void currentDBIsCorrect() throws Exception {
        PowerMockito.when(mockWorkTimeDAO.getAll()).thenReturn(Collections.emptyList());
        PowerMockito.when(mockWorkTimeDAO.getAll(CommonTest.testPerson)).thenReturn(Collections.emptyList());
        PowerMockito.when(mockWorkTimeDAO.add(testWorkTime)).thenReturn(false);
        PowerMockito.when(mockWorkTimeDAO.remove(testWorkTime)).thenReturn(false);
        PowerMockito.when(mockWorkTimeDAO.update(testWorkTime)).thenReturn(false);
        PowerMockito.when(mockWorkTimeDAO.contains(testWorkTime)).thenReturn(false);
        PowerMockito.when(mockWorkTimeDAO.findById(0)).thenReturn(null);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        WorkTimeDAO workTimeDAO = ProxyWorkTimeDAO.getInstance();

        CommonTest.testOnDefault(workTimeDAO, testWorkTime, Collections.emptyList(),false, null);
    }

    @Test
    public void currentDBAndCorrectVal() throws Exception {
        List<WorkTime> workTimes = new LinkedList<>();
        workTimes.add(new WorkTime(1, LocalTime.of(0, 0), LocalTime.of(2, 0)));
        PowerMockito.when(mockWorkTimeDAO.getAll()).thenReturn(workTimes);
        PowerMockito.when(mockWorkTimeDAO.getAll(CommonTest.testPerson)).thenReturn(workTimes);
        PowerMockito.when(mockWorkTimeDAO.add(testWorkTime)).thenReturn(true);
        PowerMockito.when(mockWorkTimeDAO.remove(testWorkTime)).thenReturn(true);
        PowerMockito.when(mockWorkTimeDAO.update(testWorkTime)).thenReturn(true);
        PowerMockito.when(mockWorkTimeDAO.contains(testWorkTime)).thenReturn(true);
        PowerMockito.when(mockWorkTimeDAO.findById(0)).thenReturn(testWorkTime);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        WorkTimeDAO workTimeDAO = ProxyWorkTimeDAO.getInstance();

        CommonTest.testOnDefault(workTimeDAO, testWorkTime, workTimes,true, testWorkTime);
    }

    @Test
    public void currentDBIsNull() {
        when(systemConfig.getCurrentDB()).thenReturn(null);
        WorkTimeDAO workTimeDAO = ProxyWorkTimeDAO.getInstance();
        CommonTest.testOnDefault(workTimeDAO, testWorkTime, Collections.emptyList(), false, null);
    }

    @Test
    public void currentDBIsEmpty() {
        when(systemConfig.getCurrentDB()).thenReturn("");
        WorkTimeDAO workTimeDAO = ProxyWorkTimeDAO.getInstance();
        CommonTest.testOnDefault(workTimeDAO, testWorkTime, Collections.emptyList(), false, null);
    }

    @Test
    public void existingInstanceTest() throws Exception {
        WorkTimeDAO workTimeDAO = ProxyWorkTimeDAO.getInstance();

        instance.set(null, workTimeDAO);

        Assert.assertEquals(workTimeDAO, ProxyWorkTimeDAO.getInstance());
    }
}