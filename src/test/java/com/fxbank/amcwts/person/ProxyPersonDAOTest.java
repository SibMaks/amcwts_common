package com.fxbank.amcwts.person;

import com.fxbank.amcwts.CommonTest;
import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.SystemConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author drobyshev-ma
 * @version 0.1
 * @since 04.05.2017
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigWorker.class, SystemConfig.class, MySQLPersonDAO.class})
public class ProxyPersonDAOTest {
    private Field instance;
    private SystemConfig systemConfig = mock(SystemConfig.class);
    private PersonDAO mockPersonDAO = mock(MySQLPersonDAO.class);
    private Person testPerson = new Person();

    public ProxyPersonDAOTest() throws NoSuchFieldException {
        instance = ProxyPersonDAO.class.getDeclaredField("PERSON_DAO");
        systemConfig = mock(SystemConfig.class);
        mockPersonDAO = mock(MySQLPersonDAO.class);
        testPerson = new Person();
    }

    @Before
    public void setUp() throws Exception {
        instance.setAccessible(true);
        instance.set(null, null);

        Mockito.reset(systemConfig);
        Mockito.reset(mockPersonDAO);

        PowerMockito.mockStatic(ConfigWorker.class);
        PowerMockito.mockStatic(MySQLPersonDAO.class);

        ConfigWorker configWorker = PowerMockito.mock(ConfigWorker.class);
        PowerMockito.when(configWorker.getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(ConfigWorker.getInstance()).thenReturn(configWorker);

        when(ConfigWorker.getInstance().getSystemConfig()).thenReturn(systemConfig);
        PowerMockito.when(MySQLPersonDAO.getInstance()).thenReturn(mockPersonDAO);
    }

    @Test
    public void currentDBIsCorrect() throws Exception {
        List<Person> persons = new LinkedList<>();

        PowerMockito.when(mockPersonDAO.getAll()).thenReturn(Collections.emptyList());
        PowerMockito.when(mockPersonDAO.getAll(CommonTest.testPerson)).thenReturn(Collections.emptyList());
        PowerMockito.when(mockPersonDAO.add(testPerson)).thenReturn(false);
        PowerMockito.when(mockPersonDAO.remove(testPerson)).thenReturn(false);
        PowerMockito.when(mockPersonDAO.update(testPerson)).thenReturn(false);
        PowerMockito.when(mockPersonDAO.contains(testPerson)).thenReturn(false);
        PowerMockito.when(mockPersonDAO.findById(0)).thenReturn(null);

        PowerMockito.when(mockPersonDAO.findByLogin("Login")).thenReturn(null);
        PowerMockito.when(mockPersonDAO.findByPersId("PersId")).thenReturn(null);
        PowerMockito.when(mockPersonDAO.addAll(persons)).thenReturn(false);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        PersonDAO personDAO = ProxyPersonDAO.getInstance();

        CommonTest.testOnDefault(personDAO, testPerson, Collections.emptyList(),false, null);

        Assert.assertEquals(personDAO.findByLogin("Login"), null);
        Assert.assertEquals(personDAO.findByPersId("PersId"), null);
        Assert.assertEquals(personDAO.addAll(persons), false);
    }

    @Test
    public void currentDBAndCorrectVal() throws Exception {
        List<Person> persons = new LinkedList<>();
        persons.add(new Person());

        PowerMockito.when(mockPersonDAO.getAll()).thenReturn(persons);
        PowerMockito.when(mockPersonDAO.getAll(CommonTest.testPerson)).thenReturn(persons);
        PowerMockito.when(mockPersonDAO.add(testPerson)).thenReturn(true);
        PowerMockito.when(mockPersonDAO.remove(testPerson)).thenReturn(true);
        PowerMockito.when(mockPersonDAO.update(testPerson)).thenReturn(true);
        PowerMockito.when(mockPersonDAO.contains(testPerson)).thenReturn(true);
        PowerMockito.when(mockPersonDAO.findById(0)).thenReturn(testPerson);

        PowerMockito.when(mockPersonDAO.findByLogin("Login")).thenReturn(testPerson);
        PowerMockito.when(mockPersonDAO.findByPersId("PersId")).thenReturn(testPerson);
        PowerMockito.when(mockPersonDAO.addAll(persons)).thenReturn(true);

        when(systemConfig.getCurrentDB()).thenReturn("MySQL");
        PersonDAO personDAO = ProxyPersonDAO.getInstance();

        CommonTest.testOnDefault(personDAO, testPerson, persons,true, testPerson);

        Assert.assertEquals(personDAO.findByLogin("Login"), testPerson);
        Assert.assertEquals(personDAO.findByPersId("PersId"), testPerson);
        Assert.assertEquals(personDAO.addAll(persons), true);
    }

    @Test
    public void currentDBIsNull() {
        when(systemConfig.getCurrentDB()).thenReturn(null);
        PersonDAO personDAO = ProxyPersonDAO.getInstance();
        CommonTest.testOnDefault(personDAO, testPerson, Collections.emptyList(), false, null);

        List<Person> persons = new LinkedList<>();

        Assert.assertEquals(personDAO.findByLogin("Login"), null);
        Assert.assertEquals(personDAO.findByPersId("PersId"), null);
        Assert.assertEquals(personDAO.addAll(persons), false);
    }

    @Test
    public void currentDBIsEmpty() {
        when(systemConfig.getCurrentDB()).thenReturn("");
        PersonDAO personDAO = ProxyPersonDAO.getInstance();
        CommonTest.testOnDefault(personDAO, testPerson, Collections.emptyList(), false, null);
        List<Person> persons = new LinkedList<>();

        Assert.assertEquals(personDAO.findByLogin("Login"), null);
        Assert.assertEquals(personDAO.findByPersId("PersId"), null);
        Assert.assertEquals(personDAO.addAll(persons), false);
    }

    @Test
    public void existingInstanceTest() throws Exception {
        PersonDAO personDAO = ProxyPersonDAO.getInstance();

        instance.set(null, personDAO);

        Assert.assertEquals(personDAO, ProxyPersonDAO.getInstance());
    }
}