package com.fxbank.amcwts.translog;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.translog.trantype.TranType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * The {@code ProxyTranslogDAO} is a proxy class for {@code TranslogDAO}. Class stores instances classes {@code TranslogDAO}s.
 * And when {@code ProxyTranslogDAO} methods invoke, it's invoke such method in current {@code TranslogDAO}.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-11
 * @see TranslogDAO
 * @see Translog
 * @see ConfigWorker
 */
public final class ProxyTranslogDAO extends TranslogDAO {
    private static TranslogDAO TRANSLOG_DAO;
    private final Map<String, TranslogDAO> SUPPORTED_DB = new HashMap<>();

    private ProxyTranslogDAO() {
        super("ProxyTranslog");
        SUPPORTED_DB.put("MySQL", MySQLTranslogDAO.getInstance());
    }

    /**
     * Method return instance of the ProxyTranslogDAO.
     * @return Instance of the TranslogDAO class for ProxyTranslogDAO.
     * @see TranslogDAO
     */
    public static TranslogDAO getInstance() {
        if(TRANSLOG_DAO == null)
            TRANSLOG_DAO = new ProxyTranslogDAO();
        return TRANSLOG_DAO;
    }

    /**
     * Gets the current database and tries to find the corresponding {@code TranslogDAO}.
     * @return Instance of TranslogDAO for current DB or null.
     * @see TranslogDAO
     */
    private TranslogDAO getTranslogDAO() {
        String currentDB = ConfigWorker.getInstance().getSystemConfig().getCurrentDB();
        if(currentDB != null && !currentDB.isEmpty()) {
            return SUPPORTED_DB.get(currentDB);
        }
        return null;
    }

    @Override
    public List<Translog> getAll(Person person) {
        TranslogDAO translogDAO = getTranslogDAO();
        if(translogDAO != null)
            return translogDAO.getAll(person);
        return Collections.emptyList();
    }

    @Override
    public List<Translog> getAll() {
        TranslogDAO translogDAO = getTranslogDAO();
        if(translogDAO != null)
            return translogDAO.getAll();
        return Collections.emptyList();
    }

    @Override
    public List<Translog> getAllByDates(Person person, LocalDateTime fromDate, LocalDateTime toDate) {
        TranslogDAO translogDAO = getTranslogDAO();
        if(translogDAO != null)
            return translogDAO.getAllByDates(person, fromDate, toDate);
        return Collections.emptyList();
    }

    @Override
    public List<Translog> getPersonTranslogsByDates(Person person, LocalDateTime fromDate, LocalDateTime toDate) {
        TranslogDAO translogDAO = getTranslogDAO();
        if(translogDAO != null)
            return translogDAO.getPersonTranslogsByDates(person, fromDate, toDate);
        return Collections.emptyList();
    }

    @Override
    public Translog findById(long id) {
        TranslogDAO translogDAO = getTranslogDAO();
        if(translogDAO != null)
            return translogDAO.findById(id);
        return null;
    }

    @Override
    public boolean contains(Translog value) {
        TranslogDAO translogDAO = getTranslogDAO();
        return translogDAO != null && translogDAO.contains(value);
    }

    @Override
    public Translog getLastByPersonAndType(Person person, TranType tranType) {
        TranslogDAO translogDAO = getTranslogDAO();
        if(translogDAO != null)
            return translogDAO.getLastByPersonAndType(person, tranType);
        return null;
    }

    @Override
    public Translog getLast(Person person) throws IllegalAccessException {
        TranslogDAO translogDAO = getTranslogDAO();
        if(translogDAO != null)
            return translogDAO.getLast(person);
        return null;
    }

    @Override
    public boolean add(Translog translog) {
        TranslogDAO translogDAO = getTranslogDAO();
        return translogDAO != null && translogDAO.add(translog);
    }

    @Override
    public boolean addAll(List<Translog> translog) {
        TranslogDAO translogDAO = getTranslogDAO();
        return translogDAO != null && translogDAO.addAll(translog);
    }

    @Override
    public boolean remove(Translog translog) {
        TranslogDAO translogDAO = getTranslogDAO();
        return translogDAO != null && translogDAO.remove(translog);
    }

    @Override
    public boolean update(Translog value) {
        TranslogDAO translogDAO = getTranslogDAO();
        return translogDAO != null && translogDAO.update(value);
    }

    @Override
    protected Translog tryCreate(ResultSet resultSet) throws SQLException {
        return null;
    }

    @Override
    public boolean removeAll(List<Translog> translog) {
        TranslogDAO translogDAO = getTranslogDAO();
        return translogDAO != null && translogDAO.removeAll(translog);
    }
}
