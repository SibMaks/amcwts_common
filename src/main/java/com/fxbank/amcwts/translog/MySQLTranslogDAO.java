package com.fxbank.amcwts.translog;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.person.PersonDAO;
import com.fxbank.amcwts.person.ProxyPersonDAO;
import com.fxbank.amcwts.translog.trantype.ProxyTranTypeDAO;
import com.fxbank.amcwts.translog.trantype.TranType;
import com.fxbank.amcwts.translog.trantype.TranTypeDAO;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Instance of {@code TranslogDAO}, that can working with MySQL DB. For correct work must be initialized {@code ConnectionFactory}.
 * Class {@code MySQLTranslogDAO} is Singleton, to obtain an instance, there is a method {@code getInstance()}.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-09
 * @see Translog
 * @see TranslogDAO
 */
final class MySQLTranslogDAO extends TranslogDAO {
    private static TranslogDAO TRANSLOG_DAO;

    private final String EXCEPTION_TITLE = "Внутренняя ошибка сервера - Ошибка службы учёта: %s";
    private final String ILLEGAL_ACCESS_ERROR_MESSAGE = "Недостаточно прав для выполнения данной операции.";
    private final String ADD_TRANSLOG_EXCEPTION = "Произошла ошибка при добавлении записи в БД.";
    private final String REMOVE_TRANSLOG_EXCEPTION= "Произошла ошибка при удалении записи из БД.";

    private final String SELECT_BY_PERSON_GROUP = "SELECT * FROM TRANSLOG as translog " +
            "INNER JOIN PERSON person ON person.PERSON_ID = translog.PERSON_ID WHERE person.GROUP_ID = ? ORDER BY TRAN_DATE";
    private final String SELECT_BY_DATES_QUERY = "SELECT * FROM TRANSLOG WHERE TRAN_DATE > ? AND TRAN_DATE <= ? ORDER BY TRAN_DATE";
    private final String SELECT_BY_DATES_FOR_PERSON_GROUP_QUERY = "SELECT * FROM TRANSLOG as translog " +
            "INNER JOIN PERSON person ON person.PERSON_ID = translog.PERSON_ID WHERE person.GROUP_ID = ? AND TRAN_DATE > ? AND TRAN_DATE <= ? ORDER BY TRAN_DATE";
    private final String SELECT_LAST_BY_PERSON_AND_TYPE = "SELECT * FROM TRANSLOG WHERE PERSON_id = ? AND TRANTYPE_id = ? ORDER BY TRAN_DATE DESC LIMIT 1";
    private final String SELECT_LAST_QUERY;
    private final String SELECT_LAST_BY_PERSON_GROUP = String.format("%s DESC LIMIT 1", SELECT_BY_PERSON_GROUP);
    private final String SELECT_BY_PERSON_QUERY = "SELECT * FROM TRANSLOG WHERE PERSON_ID = ? AND TRAN_DATE > ? AND TRAN_DATE <= ? ORDER BY TRAN_DATE";
    private final String INSERT_QUERY = "INSERT INTO TRANSLOG (`TRAN_ID`, `TRAN_DATE`, `TRANTYPE_ID`, `PERSON_ID`) VALUES (?, ?, ?, ?)";
    private final String DELETE_QUERY = "DELETE FROM TRANSLOG WHERE TRANSLOG_ID = ? AND TRAN_ID = ?";

    private final TranTypeDAO TRAN_TYPE_DAO = ProxyTranTypeDAO.getInstance();
    private final PersonDAO PERSON_DAO = ProxyPersonDAO.getInstance();

    private MySQLTranslogDAO() {
        super("TRANSLOG");
        SELECT_QUERY = "SELECT * FROM TRANSLOG ORDER BY TRAN_DATE";
        SELECT_LAST_QUERY = String.format("%s DESC LIMIT 1", SELECT_QUERY);
    }

    /**
     * Method return instance of the MySQLTranslogDAO.
     * @return Instance of the MySQLTranslogDAO.
     * @see TranslogDAO
     */
    static TranslogDAO getInstance() {
        if(TRANSLOG_DAO == null)
            TRANSLOG_DAO = new MySQLTranslogDAO();
        return TRANSLOG_DAO;
    }

    @Override
    public List<Translog> getAll(Person person) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement;
            int accessLevel = Person.getAccessLevel(person);

            if (accessLevel == 0)
                preparedStatement = connection.prepareStatement(SELECT_QUERY);
            else if (accessLevel == 1) {
                preparedStatement = connection.prepareStatement(SELECT_BY_PERSON_GROUP);
                preparedStatement.setLong(1, person.getGroup().getId());
            } else
                throw makeException(EXCEPTION_TITLE, ILLEGAL_ACCESS_ERROR_MESSAGE);

            ResultSet resultSet = preparedStatement.executeQuery();
            List<Translog> translogList = new LinkedList<>();

            while (resultSet.next()) {
                Translog translog = tryCreate(resultSet);
                if(translog != null)
                    translogList.add(translog);
            }
            return translogList;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public List<Translog> getAllByDates(Person person, LocalDateTime fromDate, LocalDateTime toDate) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement;
            int accessLevel = Person.getAccessLevel(person);

            if (accessLevel == 0) {
                preparedStatement = connection.prepareStatement(SELECT_BY_DATES_QUERY);
                preparedStatement.setString(1, fromDate.format(SERVICE_DATE_TIME_FORMATTER));
                preparedStatement.setString(2, toDate.format(SERVICE_DATE_TIME_FORMATTER));
            } else if (accessLevel == 1) {
                preparedStatement = connection.prepareStatement(SELECT_BY_DATES_FOR_PERSON_GROUP_QUERY);
                preparedStatement.setLong(1, person.getGroup().getId());
                preparedStatement.setString(2, fromDate.format(SERVICE_DATE_TIME_FORMATTER));
                preparedStatement.setString(3, toDate.format(SERVICE_DATE_TIME_FORMATTER));
            } else {
                preparedStatement = connection.prepareStatement(SELECT_BY_PERSON_QUERY);
                preparedStatement.setLong(1, person.getId());
                preparedStatement.setString(2, fromDate.format(SERVICE_DATE_TIME_FORMATTER));
                preparedStatement.setString(3, toDate.format(SERVICE_DATE_TIME_FORMATTER));
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            List<Translog> translogList = new LinkedList<>();

            while (resultSet.next()) {
                Translog translog = tryCreate(resultSet);
                if(translog != null)
                    translogList.add(translog);
            }
            return translogList;
        } catch (Exception e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public List<Translog> getPersonTranslogsByDates(Person person, LocalDateTime fromDate, LocalDateTime toDate) {
        List<Translog> translogs = getAllByDates(person, fromDate, toDate);
        return translogs
                .stream()
                .filter(translog -> translog.getPerson().getId() == person.getId())
                .collect(Collectors.toList());
    }

    @Override
    public Translog findById(long id) {
        return find("TRANSLOG_ID", id);
    }

    @Override
    public boolean contains(Translog value) {
        return findById(value.getId()) != null;
    }

    @Override
    public Translog getLastByPersonAndType(Person person, TranType tranType) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_LAST_BY_PERSON_AND_TYPE);
            preparedStatement.setLong(1, person.getId());
            preparedStatement.setLong(2, tranType.getId());

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return tryCreate(resultSet);
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
        return null;
    }

    @Override
    public Translog getLast(Person person) throws IllegalAccessException {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement;
            int accessLevel = Person.getAccessLevel(person);
            switch (accessLevel) {
                case 0:
                    preparedStatement = connection.prepareStatement(SELECT_LAST_QUERY);
                    break;
                case 1:
                    preparedStatement = connection.prepareStatement(SELECT_LAST_BY_PERSON_GROUP);
                    preparedStatement.setLong(1, person.getGroup().getId());
                    break;
                default:
                    throw makeException(EXCEPTION_TITLE, ILLEGAL_ACCESS_ERROR_MESSAGE);
            }

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next())
                return tryCreate(resultSet);
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
        return null;
    }

    @Override
    public boolean add(Translog translog) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, translog.getTranId());
            preparedStatement.setString(2, translog.getTranDate().format(SERVICE_DATE_TIME_FORMATTER));
            preparedStatement.setLong(3, translog.getTranType().getId());
            preparedStatement.setLong(4, translog.getPerson().getId());

            boolean successUpdate = preparedStatement.executeUpdate() == 1;
            if(successUpdate) {
                long translogId = getGeneratedKeyLong(preparedStatement);
                if(translogId != -1)
                    translog.setId(translogId);
                else
                    return false;
            }
            return successUpdate;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean addAll(List<Translog> translogs) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);

            connection.setAutoCommit(false);
            int result = 0;

            for(Translog translog : translogs) {
                preparedStatement.setString(1, translog.getTranId());
                preparedStatement.setString(2, translog.getTranDate().format(SERVICE_DATE_TIME_FORMATTER));
                preparedStatement.setLong(3, translog.getTranType().getId());
                preparedStatement.setLong(4, translog.getPerson().getId());

                try {
                    boolean successUpdate = preparedStatement.executeUpdate() == 1;
                    if (successUpdate) {
                        long translogId = getGeneratedKeyLong(preparedStatement);
                        if(translogId != -1) {
                            result++;
                            translog.setId(translogId);
                        } else
                            throw new SQLException(ADD_TRANSLOG_EXCEPTION);
                    }
                } catch (SQLException exception) {
                    connection.rollback();
                    connection.setAutoCommit(true);
                    throw new SQLException(exception);
                }
            }

            if(result == translogs.size()) {
                connection.commit();
            } else
                connection.rollback();
            connection.setAutoCommit(true);

            return result == translogs.size();
        } catch (Exception e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean remove(Translog translog) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
            preparedStatement.setLong(1, translog.getId());
            preparedStatement.setString(2, translog.getTranId());

            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean update(Translog value) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    protected Translog tryCreate(ResultSet resultSet) throws SQLException {
        long tranId = resultSet.getLong("TRANSLOG_ID");
        String tranLogId = resultSet.getString("TRAN_ID");
        String tranDateTimeStr = resultSet.getString("TRAN_DATE");
        long tranTypeId = resultSet.getLong("TRANTYPE_ID");
        long personId = resultSet.getLong("PERSON_ID");
        TranType tranType = TRAN_TYPE_DAO.findById(tranTypeId);
        Person person = PERSON_DAO.findById(personId);

        if (tranType != null && person != null && tranDateTimeStr != null) {
            LocalDateTime tranDateTime = LocalDateTime.parse(tranDateTimeStr, SERVICE_DATE_TIME_FORMATTER);
            return new Translog(tranId, tranLogId, tranDateTime, tranType, person);
        }
        return null;
    }

    @Override
    public boolean removeAll(List<Translog> translogs) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);

            connection.setAutoCommit(false);
            int result = 0;

            for(Translog translog : translogs) {
                preparedStatement.setLong(1, translog.getId());
                preparedStatement.setString(2, translog.getTranId());

                try {
                    result += preparedStatement.executeUpdate();
                } catch (SQLException sqlException) {
                    connection.rollback();
                    connection.setAutoCommit(true);
                    throw new SQLException(REMOVE_TRANSLOG_EXCEPTION);
                }
            }

            if(result == translogs.size())
                connection.commit();
            else
                connection.rollback();
            connection.setAutoCommit(true);

            return result == translogs.size();
        } catch (Exception e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }
}
