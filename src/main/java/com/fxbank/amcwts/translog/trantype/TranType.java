package com.fxbank.amcwts.translog.trantype;

/**
 * The {@code TranType} contains information about tran type.
 * Normally the values of id is non-negative, name is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-03-27
 */
public final class TranType {
    private long id;
    private String name;
    private boolean enter;

    /**
     * Creates a new TranType object that represents the tran type info.
     * The fields are initialized with the default values.
     */
    public TranType() {
    }

    /**
     * Creates a new TranType object that represents the tran type info.
     * @param id id of tran type.
     * @param name name of tran type.
     * @param enter it's enter or exit. Enter == true, exit == false.
     */
    public TranType(long id, String name, boolean enter) {
        this.id = id;
        this.name = name;
        this.enter = enter;
    }

    /**
     * Creates a new TranType object that represents the tran type info.
     * @param id id of tran type.
     * @param name name of tran type.
     */
    public TranType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Returns the id of tran type.
     * @return Returns the id of tran type.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id of this TranType object to the specified value.
     * @param id the new value for the TranType object.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns the name of tran type.
     * @return Returns the name of tran type.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this TranType object to the specified value.
     * @param name the new value for the TranType object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the is enter or exit tran type.
     * @return Returns the is enter or exit tran type.
     */
    public boolean isEnter() {
        return enter;
    }

    /**
     * Sets the enter of this TranType object to the specified value.
     * @param enter the new value for the TranType object.
     */
    public void setEnter(boolean enter) {
        this.enter = enter;
    }
}
