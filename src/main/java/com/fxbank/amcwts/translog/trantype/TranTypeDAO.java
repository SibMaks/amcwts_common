package com.fxbank.amcwts.translog.trantype;

import com.fxbank.amcwts.CommonDAO;

/**
 * Abstract class for managing a different {@code TranType}. Extends CommonDAO with type {@code TranType}.
 * This class is abstract for working with tran type.
 * Used to implement work with various types of databases, for example, such as My SQL, MS SQL.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-30
 * @see ProxyTranTypeDAO
 * @see TranType
 */
public abstract class TranTypeDAO extends CommonDAO<TranType> {
    protected TranTypeDAO(String tableName) {
        super(tableName);
    }
}
