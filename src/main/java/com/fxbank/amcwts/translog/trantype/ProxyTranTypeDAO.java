package com.fxbank.amcwts.translog.trantype;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.person.Person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@code ProxyTranTypeDAO} is a proxy class for {@code TranTypeDAO}. Class stores instances classes {@code TranTypeDAO}s.
 * And when {@code ProxyTranTypeDAO} methods invoke, it's invoke such method in current {@code TranTypeDAO}.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-30
 * @see TranTypeDAO
 * @see TranType
 * @see ConfigWorker
 */
public final class ProxyTranTypeDAO extends TranTypeDAO {
    private static TranTypeDAO TRAN_TYPE_DAO;
    private final Map<String, TranTypeDAO> SUPPORTED_DB = new HashMap<>();

    private ProxyTranTypeDAO() {
        super("ProxyTranType");
        SUPPORTED_DB.put("MySQL", MySQLTranTypeDAO.getInstance());
    }

    /**
     * Method return instance of the ProxyTranTypeDAO.
     * @return Instance of the TranTypeDAO class for ProxyTranTypeDAO.
     * @see TranTypeDAO
     */
    public static TranTypeDAO getInstance() {
        if(TRAN_TYPE_DAO == null)
            TRAN_TYPE_DAO = new ProxyTranTypeDAO();
        return TRAN_TYPE_DAO;
    }

    /**
     * Gets the current database and tries to find the corresponding {@code TranTypeDAO}.
     * @return Instance of TranTypeDAO for current DB or null.
     * @see TranTypeDAO
     */
    private TranTypeDAO getTranTypeDAO() {
        String currentDB = ConfigWorker.getInstance().getSystemConfig().getCurrentDB();
        if(currentDB != null && !currentDB.isEmpty()) {
            return SUPPORTED_DB.get(currentDB);
        }
        return null;
    }

    @Override
    public boolean add(TranType tranType) {
        TranTypeDAO tranTypeDAO = getTranTypeDAO();
        return tranTypeDAO != null && tranTypeDAO.add(tranType);
    }

    @Override
    public boolean update(TranType tranType) {
        TranTypeDAO tranTypeDAO = getTranTypeDAO();
        return tranTypeDAO != null && tranTypeDAO.update(tranType);
    }

    @Override
    protected TranType tryCreate(ResultSet resultSet) throws SQLException {
        return null;
    }

    @Override
    public boolean remove(TranType tranType) {
        TranTypeDAO tranTypeDAO = getTranTypeDAO();
        return tranTypeDAO != null && tranTypeDAO.remove(tranType);
    }

    @Override
    public TranType findById(long id) {
        TranTypeDAO tranTypeDAO = getTranTypeDAO();
        if(tranTypeDAO != null)
            return tranTypeDAO.findById(id);
        return null;
    }

    @Override
    public boolean contains(TranType value) {
        TranTypeDAO tranTypeDAO = getTranTypeDAO();
        return tranTypeDAO != null && tranTypeDAO.contains(value);
    }

    @Override
    public List<TranType> getAll(Person person) {
        TranTypeDAO tranTypeDAO = getTranTypeDAO();
        if(tranTypeDAO != null)
            return tranTypeDAO.getAll(person);
        return Collections.emptyList();
    }

    @Override
    public List<TranType> getAll() {
        TranTypeDAO tranTypeDAO = getTranTypeDAO();
        if(tranTypeDAO != null)
            return tranTypeDAO.getAll();
        return Collections.emptyList();
    }
}