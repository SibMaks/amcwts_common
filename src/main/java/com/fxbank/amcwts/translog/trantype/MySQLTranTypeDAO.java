package com.fxbank.amcwts.translog.trantype;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;

import java.sql.*;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Instance of {@code TranTypeDAO}, that can working with MySQL DB. For correct work must be initialized {@code ConnectionFactory}.
 * Class {@code MySQLTranTypeDAO} is Singleton, to obtain an instance, there is a method {@link MySQLTranTypeDAO#getInstance()}.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-30
 * @see MySQLTranTypeDAO
 * @see TranType
 * @see ConnectionFactory
 */
final class MySQLTranTypeDAO extends TranTypeDAO {
    private static TranTypeDAO TRAN_TYPE_DAO;

    private final String EXCEPTION_TITLE = "Внутренняя ошибка сервера - Служба работы с сотрудниками не доступна: %s";

    private final String INSERT_QUERY = "INSERT INTO IN_OUT (`TRANTYPE_NAME`, `TRANTYPE_IS_ENTER`) VALUES (?, ?)";
    private final String UPDATE_QUERY = "UPDATE IN_OUT SET TRANTYPE_NAME = ?, TRANTYPE_IS_ENTER = ? WHERE TRANTYPE_id = ?";
    private final String DELETE_QUERY = "DELETE FROM IN_OUT WHERE TRANTYPE_id = ?";

    private final Queue<TranType> TRAN_TYPES;

    private MySQLTranTypeDAO() {
        super("IN_OUT");
        SELECT_QUERY = "SELECT * FROM IN_OUT";
        TRAN_TYPES = new ConcurrentLinkedQueue<>();
    }

    /**
     * Method return instance of the MySQLTranTypeDAO.
     * @return Instance of the MySQLTranTypeDAO.
     */
    static TranTypeDAO getInstance() {
        if(TRAN_TYPE_DAO == null)
            TRAN_TYPE_DAO = new MySQLTranTypeDAO();
        return TRAN_TYPE_DAO;
    }

    @Override
    public boolean add(TranType tranType) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, tranType.getName());
            preparedStatement.setBoolean(2, tranType.isEnter());

            boolean successUpdate = preparedStatement.executeUpdate() == 1;
            if(successUpdate) {
                long tranTypeKey = getGeneratedKeyLong(preparedStatement);
                if(tranTypeKey != -1) {
                    tranType.setId(tranTypeKey);
                    TRAN_TYPES.add(tranType);
                } else
                    return false;
            }
            return successUpdate;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean update(TranType tranType) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, tranType.getName());
            preparedStatement.setBoolean(2, tranType.isEnter());
            preparedStatement.setLong(3, tranType.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result) {
                Optional<TranType> tranTypeOptional = Optional.ofNullable(findById(tranType.getId()));
                tranTypeOptional.ifPresent(tranType1 -> {
                    tranType1.setName(tranType.getName());
                    tranType1.setEnter(tranType.isEnter());
                });
            }
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    protected TranType tryCreate(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("TRANTYPE_id");

        if(!isCachedWithId(id)) {
            TranType tranType = new TranType(id,
                    resultSet.getString("TRANTYPE_name"),
                    resultSet.getBoolean("TRANTYPE_is_enter")
            );
            TRAN_TYPES.add(tranType);
            return tranType;
        } else
            return findById(id);
    }

    @Override
    public boolean remove(TranType tranType) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
            preparedStatement.setLong(1, tranType.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result)
                TRAN_TYPES.remove(tranType);
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public TranType findById(long id) {
        for(TranType tranType : TRAN_TYPES)
            if(tranType.getId() == id)
                return tranType;

        return find("TRANTYPE_id", id);
    }

    private boolean isCachedWithId(long id) {
        for(TranType tranType : TRAN_TYPES)
            if(tranType.getId() == id)
                return true;
        return false;
    }

    @Override
    public boolean contains(TranType value) {
        return findById(value.getId()) != null;
    }

    @Override
    public List<TranType> getAll(Person person) {
        return getAll();
    }
}
