package com.fxbank.amcwts.translog;

import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.translog.trantype.TranType;

import java.time.LocalDateTime;

/**
 * The {@code Translog} contains information about translog.
 * Normally the values of id is non-negative, tranDate, tranType, person is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-03-27
 */
public final class Translog {
    private long id;
    private String tranId;
    private LocalDateTime tranDate;
    private TranType tranType;
    private Person person;

    /**
     * Creates a new Translog object that represents the translog info.
     * The fields are initialized with the default values.
     */
    public Translog() {
    }

    /**
     * Creates a new Translog object that represents the translog info.
     * @param id id of translog.
     * @param tranId tranId of translog in Parsec.
     * @param tranDate date of translog.
     * @param tranType type of translog.
     * @param person person of translog.
     * @see LocalDateTime
     * @see TranType
     * @see Person
     */
    public Translog(long id, String tranId, LocalDateTime tranDate, TranType tranType, Person person) {
        this.id = id;
        this.tranId = tranId;
        this.tranDate = tranDate;
        this.tranType = tranType;
        this.person = person;
    }

    /**
     * Returns the id of translog.
     * @return Returns the id of translog.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id of this Translog object to the specified value.
     * @param id the new value for the Translog object.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns the tran date of translog.
     * @return Returns the tran date of translog.
     * @see LocalDateTime
     */
    public LocalDateTime getTranDate() {
        return tranDate;
    }

    /**
     * Sets the tran date of this Translog object to the specified value.
     * @param tranDate the new value for the Translog object.
     */
    public void setTranDate(LocalDateTime tranDate) {
        this.tranDate = tranDate;
    }

    /**
     * Returns the tran type of translog.
     * @return Returns the tran type of translog.
     * @see TranType
     */
    public TranType getTranType() {
        return tranType;
    }

    /**
     * Sets the tran type of this Translog object to the specified value.
     * @param tranType the new value for the Translog object.
     */
    public void setTranType(TranType tranType) {
        this.tranType = tranType;
    }

    /**
     * Returns the person of translog.
     * @return Returns the person of translog.
     * @see Person
     */
    public Person getPerson() {
        return person;
    }

    /**
     * Sets the person of this Translog object to the specified value.
     * @param person the new value for the Translog object.
     */
    public void setPerson(Person person) {
        this.person = person;
    }

    /**
     * Returns the tran id of translog in Parsec.
     * @return Returns the person of tran id.
     */
    public String getTranId() {
        return tranId;
    }

    /**
     * Sets the tran id of this Translog object to the specified value.
     * @param tranId the new value for the Translog object.
     */
    public void setTranId(String tranId) {
        this.tranId = tranId;
    }
}
