package com.fxbank.amcwts.translog;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.ParsecConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.person.PersonDAO;
import com.fxbank.amcwts.person.ProxyPersonDAO;
import com.fxbank.amcwts.translog.trantype.ProxyTranTypeDAO;
import com.fxbank.amcwts.translog.trantype.TranType;
import com.fxbank.amcwts.translog.trantype.TranTypeDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Instance of {@code TranslogDAO}, that can working with Parsec3. For correct work must be initialized {@code ConfWorker}.
 * Not all methods are implemented, because this service does not have permission to manipulate data in Parsec.
 * Class {@code ParsecTranslogDAO} is Singleton, to obtain an instance, there is a method {@code getInstance()}.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-30
 * @see Translog
 * @see TranslogDAO
 */
public final class ParsecTranslogDAO extends TranslogDAO {
    private static TranslogDAO TRANSLOG_DAO;

    private final String EXCEPTION_TITLE = "Внутренняя ошибка сервера - Ошибка службы импорта: %s";

    private final String SELECT_BY_DATES_QUERY = "SELECT * FROM TRANSLOG WHERE TRAN_DATE > ? AND TRAN_DATE <= ? ORDER BY TRAN_DATE";

    private final PersonDAO PERSON_DAO = ProxyPersonDAO.getInstance();
    private final ZoneOffset ZONE_OFFSET_LONDON = ZoneOffset.of("+0");
    private final ZoneOffset ZONE_OFFSET_SIBERIA = ZoneOffset.of("+7");
    private final ZoneId ZONE_LONDON = ZoneId.ofOffset("UTC", ZONE_OFFSET_LONDON);
    private final ZoneId ZONE_SIBERIA = ZoneId.ofOffset("UTC", ZONE_OFFSET_SIBERIA);

    private TranType tranTypeEnter;
    private TranType tranTypeExit;

    private ParsecTranslogDAO() {
        super("TRANSLOG");
        SELECT_QUERY = "SELECT * FROM TRANSLOG ORDER BY TRAN_DATE";

        TranTypeDAO tranTypeDAO = ProxyTranTypeDAO.getInstance();
        List<TranType> tranTypes = tranTypeDAO.getAll();

        for(TranType tranType : tranTypes) {
            if (tranTypeEnter == null && tranType.isEnter())
                tranTypeEnter = tranType;
            if (tranTypeExit == null && !tranType.isEnter())
                tranTypeExit = tranType;
            if(tranTypeEnter != null && tranTypeExit != null)
                break;
        }
        if(tranTypeEnter == null) {
            tranTypeEnter = new TranType(-1, "вход", true);
            if(!tranTypeDAO.add(tranTypeEnter))
                throw makeException(EXCEPTION_TITLE, "Ошибка добавления события вход.");
        }
        if(tranTypeExit == null) {
            tranTypeExit = new TranType(-1, "выход", false);
            if(!tranTypeDAO.add(tranTypeExit))
                throw makeException(EXCEPTION_TITLE, "Ошибка добавления события выход.");
        }
    }

    /**
     * Method return instance of the ParsecTranslogDAO.
     * @return Instance of the TranslogDAO class for ParsecTranslogDAO.
     * @see TranslogDAO
     */
    public static TranslogDAO getInstance() {
        if(TRANSLOG_DAO == null)
            TRANSLOG_DAO = new ParsecTranslogDAO();
        return TRANSLOG_DAO;
    }

    @Override
    protected Translog tryCreate(ResultSet resultSet) throws SQLException {
        String tranId = resultSet.getString("TRAN_ID");
        String personId = resultSet.getString("USR_ID");
        Person translogPerson = PERSON_DAO.findByPersId(personId);
        LocalDateTime tranDate = LocalDateTime.parse(resultSet.getString("TRAN_DATE"), MS_SQL_DATE_TIME_FORMATTER);
        String tranTypeStr = resultSet.getString("TRANTYPE_ID");
        TranType tranType = tranTypeExit;

        if (tranTypeStr.equalsIgnoreCase("590144") || tranTypeStr.equalsIgnoreCase("590152"))
            tranType = tranTypeEnter;

        if(translogPerson != null && tranDate != null && tranType != null) {
            tranDate = tranDate.atZone(ZONE_LONDON).withZoneSameInstant(ZONE_SIBERIA).toLocalDateTime();
            return new Translog(-1, tranId, tranDate, tranType, translogPerson);
        }
        return null;
    }

    @Override
    public List<Translog> getAll(Person person) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        List<Translog> translogs = new LinkedList<>();
        ParsecConfig parsecConfig = ConfigWorker.getInstance().getSystemConfig().getParsecConfig();
        String connectionString = String.format("jdbc:sqlserver://%s:%s;databaseName=Parsec3Trans;integratedSecurity=false;", parsecConfig.getAddress(), parsecConfig.getPort());
        try (Connection connection = connectionFactory.getConnection(connectionString, parsecConfig.getLogin(), parsecConfig.getPassword())) {
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(SELECT_QUERY);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Translog translog = tryCreate(resultSet);
                if(translog != null)
                    translogs.add(translog);
            }
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
        return translogs;
    }

    @Override
    public Translog findById(long id) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public boolean contains(Translog value) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public List<Translog> getAllByDates(Person person, LocalDateTime fromDate, LocalDateTime toDate) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        List<Translog> translogs = new LinkedList<>();
        ParsecConfig parsecConfig = ConfigWorker.getInstance().getSystemConfig().getParsecConfig();
        String connectionString = String.format("jdbc:sqlserver://%s:%s;databaseName=Parsec3Trans;integratedSecurity=false;", parsecConfig.getAddress(), parsecConfig.getPort());
        try (Connection connection = connectionFactory.getConnection(connectionString, parsecConfig.getLogin(), parsecConfig.getPassword())) {
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(SELECT_BY_DATES_QUERY);
            preparedStatement.setString(1, fromDate.atZone(ZONE_SIBERIA).withZoneSameInstant(ZONE_LONDON).toLocalDateTime().format(SERVICE_DATE_TIME_FORMATTER));
            preparedStatement.setString(2, toDate.atZone(ZONE_SIBERIA).withZoneSameInstant(ZONE_LONDON).toLocalDateTime().format(SERVICE_DATE_TIME_FORMATTER));

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Translog translog = tryCreate(resultSet);
                if(translog != null)
                    translogs.add(translog);
            }
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
        return translogs;
    }

    @Override
    public List<Translog> getPersonTranslogsByDates(Person person, LocalDateTime fromDate, LocalDateTime toDate) {
        List<Translog> translogs = getAllByDates(person, fromDate, toDate);
        return translogs
                .stream()
                .filter(translog -> {
                    String personId = translog.getPerson().getPersonId();
                    String personIdFind = person.getPersonId();
                    return personId == null ? null == personIdFind : personId.equalsIgnoreCase(personIdFind);
                })
                .collect(Collectors.toList());
    }

    @Override
    public Translog getLastByPersonAndType(Person person, TranType tranType) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public Translog getLast(Person person) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public boolean add(Translog translog) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public boolean addAll(List<Translog> translogs) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public boolean remove(Translog translog) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public boolean update(Translog value) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public boolean removeAll(List<Translog> translogs) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }
}
