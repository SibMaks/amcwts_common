package com.fxbank.amcwts.translog;

import com.fxbank.amcwts.CommonDAO;
import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.translog.trantype.TranType;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Abstract class for managing a different {@code Translog}. Extends CommonDAO with type {@code Translog}.
 * This class is abstract for working with translog.
 * Used to implement work with various types of databases, for example, such as My SQL, MS SQL.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-02
 * @see ProxyTranslogDAO
 * @see Translog
 */
public abstract class TranslogDAO extends CommonDAO<Translog> {

    protected TranslogDAO(String tableName) {
        super(tableName);
    }

    /**
     * Method, that returns all Translog between dates for all company or group.
     * @param person user, who request get Person. It's need to limit access.
     * @param fromDate date from which the Translog are sought.
     * @param toDate date to which the Translog are sought.
     * @return List of Translog or empty list.
     * @exception IllegalAccessException if Person doesn't have permission for company info or group if not permission.
     */
    abstract public List<Translog> getAllByDates(Person person, LocalDateTime fromDate, LocalDateTime toDate);

    /**
     * Method, that returns all Translog between dates.
     * @param person user, who request get Person. It's need to limit access.
     * @param fromDate date from which the Translog are sought.
     * @param toDate date to which the Translog are sought.
     * @return List of Translog or empty list.
     */
    abstract public List<Translog> getPersonTranslogsByDates(Person person, LocalDateTime fromDate, LocalDateTime toDate);

    /**
     * Method, that return last Translog with specific type and specific person.
     * @param person user, whose Translog must be returned.
     * @param tranType type of Translog, which must be returned.
     * @return Translog or null.
     */
    abstract public Translog getLastByPersonAndType(Person person, TranType tranType);

    /**
     * Method, that return last Translog to which the user has access.
     * @param person user, who request get Translog. It's need to limit access.
     * @return Translog or null.
     */
    abstract public Translog getLast(Person person) throws IllegalAccessException;

    /**
     * Method, that add all list of Translog in data base.
     * @param translogs list of Translog that need add to data base.
     * @return True if all elements have been added or false otherwise.
     */
    abstract public boolean addAll(List<Translog> translogs);

    /**
     * Method, that remove all Translog of list from data base.
     * @param translogs list of Translog that need remove from data base.
     * @return True if all elements have been removed or false otherwise.
     */
    abstract public boolean removeAll(List<Translog> translogs);
}