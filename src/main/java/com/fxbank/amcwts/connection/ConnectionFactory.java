package com.fxbank.amcwts.connection;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.DBDescription;
import com.fxbank.amcwts.conf.SystemConfig;
import org.apache.tomcat.jdbc.pool.DataSource;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Properties;

/**
 * The {@code ConnectionFactory} is needed to get {@code Connection} for current data base.
 * The {@code ConnectionFactory} get current data base from {@code ConfigWorker}.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-06
 * @see ConfigWorker
 */
public final class ConnectionFactory {
    private static ConnectionFactory connectionFactory;
    private DataSource dataSource;

    private ConnectionFactory() {
        dataSource = new DataSource();

        Properties dataSourceProperties = new Properties();

        try (InputStream fileInputStream = getClass().getResourceAsStream("/conf/data-source.conf.ini")) {
            dataSourceProperties.load(fileInputStream);
            String maxActiveProp = dataSourceProperties.getProperty("max-active");
            String maxIdleProp = dataSourceProperties.getProperty("max-idle");
            String minIdleProp = dataSourceProperties.getProperty("min-idle");
            String maxWaitProp = dataSourceProperties.getProperty("max-wait");
            String validationQueryProp = dataSourceProperties.getProperty("validation-query");
            String validationQueryTimeoutProp = dataSourceProperties.getProperty("validation-query-timeout");
            String removeAbandonedProp = dataSourceProperties.getProperty("remove-abandoned");
            String removeAbandonedTimeoutProp = dataSourceProperties.getProperty("remove-abandoned-timeout");
            String logAbandonedProp = dataSourceProperties.getProperty("log-abandoned");
            String maxAgeProp = dataSourceProperties.getProperty("max-age");

            try {
                int maxActive = Integer.parseInt(maxActiveProp);
                int maxIdle = Integer.parseInt(maxIdleProp);
                int minIdle = Integer.parseInt(minIdleProp);
                int maxWait = Integer.parseInt(maxWaitProp);
                int validationQueryTimeout = Integer.parseInt(validationQueryTimeoutProp);
                int removeAbandonedTimeout = Integer.parseInt(removeAbandonedTimeoutProp);
                long maxAge = Long.parseLong(maxAgeProp);

                if(validationQueryProp == null || validationQueryProp.isEmpty())
                    throw new NullPointerException();

                boolean removeAbandoned = Boolean.valueOf(removeAbandonedProp);
                boolean logAbandoned = Boolean.valueOf(logAbandonedProp);

                dataSource.setMaxActive(maxActive);
                dataSource.setMaxIdle(maxIdle);
                dataSource.setMinIdle(minIdle);
                dataSource.setMaxWait(maxWait);
                dataSource.setValidationQuery(validationQueryProp);
                dataSource.setValidationQueryTimeout(validationQueryTimeout);
                dataSource.setRemoveAbandoned(removeAbandoned);
                dataSource.setRemoveAbandonedTimeout(removeAbandonedTimeout);
                dataSource.setLogAbandoned(logAbandoned);
                dataSource.setMaxAge(maxAge);

                updateProperties();
            } catch (NullPointerException | NumberFormatException nfe) {
                throw new RuntimeException("Ошибка чтения файла конфигурации DataSource.");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка загрузки файла конфигурации DataSource.");
        }
    }

    /**
     * Returns instance of {@code ConnectionFactory}.
     * @return Returns instance of ConnectionFactory.
     */
    public static ConnectionFactory getInstance() {
        if(connectionFactory == null)
            connectionFactory = new ConnectionFactory();
        return connectionFactory;
    }

    /**
     * Returns connection to current data base.
     * @return Returns connection to current data base.
     * @throws SQLException if the current database is configured incorrectly.
     * @see Connection
     * @see SQLException
     */
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public Connection getConnection(String connectionString, String login, String password) throws SQLException {
        return DriverManager.getConnection(connectionString, login, password);
    }

    /**
     * Updates the pool properties, gets {@code DBDescription} of current data base from {@code ConfigWorker}.
     * @see DBDescription
     * @see ConfigWorker
     */
    public void updateProperties() {
        Optional.ofNullable(ConfigWorker.getInstance())
                .flatMap(configWorker1 -> Optional.ofNullable(configWorker1.getSystemConfig()))
                .ifPresent(this::update);
    }

    private void update(SystemConfig systemConfig) {
        String currentDB = Optional.of(systemConfig)
                .flatMap(systemConfig1 -> Optional.ofNullable(systemConfig1.getCurrentDB()))
                .orElse("default");

        Optional.ofNullable(systemConfig.findDBDescriptionByName(currentDB))
                .ifPresent(dbDescription -> {
                    dataSource.setUrl(dbDescription.getConnectionString());
                    dataSource.setDriverClassName(dbDescription.getDriverClassName());
                    dataSource.setUsername(dbDescription.getLogin());
                    dataSource.setPassword(dbDescription.getPassword());
                });
    }
}