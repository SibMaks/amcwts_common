package com.fxbank.amcwts.person;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.conf.ParsecConfig;
import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.accesslevel.AccessLevel;
import com.fxbank.amcwts.person.accesslevel.AccessLevelDAO;
import com.fxbank.amcwts.person.accesslevel.ProxyAccessLevelDAO;
import com.fxbank.amcwts.person.group.Group;
import com.fxbank.amcwts.person.group.GroupDAO;
import com.fxbank.amcwts.person.group.ProxyGroupDAO;
import com.fxbank.amcwts.person.position.Position;
import com.fxbank.amcwts.person.position.PositionDAO;
import com.fxbank.amcwts.person.position.ProxyPositionDAO;
import com.fxbank.amcwts.person.worktime.ProxyWorkTimeDAO;
import com.fxbank.amcwts.person.worktime.WorkTime;
import com.fxbank.amcwts.person.worktime.WorkTimeDAO;
import com.ibm.icu.text.Transliterator;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

/**
 * The {@code ParsecPersonDAO} extends {@code PersonDAO} and implements methods for working with Parsec DB.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-28
 */
public final class ParsecPersonDAO extends PersonDAO {
    private final String EXCEPTION_TITLE = "Ошибка импорта сотрудников: %s";

    private static PersonDAO PERSON_DAO;

    private final GroupDAO GROUP_DAO = ProxyGroupDAO.getInstance();
    private final PositionDAO POSITION_DAO = ProxyPositionDAO.getInstance();
    private final WorkTimeDAO WORK_TIME_DAO = ProxyWorkTimeDAO.getInstance();
    private final AccessLevelDAO ACCESS_LEVEL_DAO = ProxyAccessLevelDAO.getInstance();
    private final String CYRILLIC_TO_LATIN = "Russian-Latin/BGN";

    private ParsecPersonDAO() {
        super("PERSON");
        SELECT_QUERY = "SELECT * FROM PERSON";
    }

    /**
     * Method return instance of the ParsecPersonDAO.
     * @return Instance of the PersonDAO class for ParsecPersonDAO.
     * @see PersonDAO
     */
    public static PersonDAO getInstance() {
        if(PERSON_DAO == null)
            PERSON_DAO = new ParsecPersonDAO();
        return PERSON_DAO;
    }

    @Override
    public List<Person> getAll(Person person) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        List<Person> resultList = new LinkedList<>();
        ParsecConfig parsecConfig = ConfigWorker.getInstance().getSystemConfig().getParsecConfig();
        String connectionString = String.format("jdbc:sqlserver://%s:%s;databaseName=Parsec3;integratedSecurity=false;", parsecConfig.getAddress(), parsecConfig.getPort());
        try (Connection connection = connectionFactory.getConnection(connectionString, parsecConfig.getLogin(), parsecConfig.getPassword())) {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_QUERY);

            String defaultName = "Import - " + LocalDateTime.now();

            Group defaultGroup = new Group(-1, defaultName);
            if(!GROUP_DAO.add(defaultGroup))
                throw makeException(EXCEPTION_TITLE, "Ошибка при добавлении группы.");

            AccessLevel defaultAccessLevel = new AccessLevel(-1, defaultName, 2);
            if (!ACCESS_LEVEL_DAO.add(defaultAccessLevel))
                throw makeException(EXCEPTION_TITLE, "Ошибка при добавлении уровня доступа.");

            Position defaultPosition = new Position(-1, defaultName, defaultAccessLevel);
            if (!POSITION_DAO.add(defaultPosition))
                throw makeException(EXCEPTION_TITLE, "Ошибка при добавлении должности.");

            List<WorkTime> workTimes = WORK_TIME_DAO.getAll();

            WorkTime defaultWorkTime;
            if(workTimes.isEmpty()) {
                defaultWorkTime = new WorkTime(-1, LocalTime.of(9, 0), LocalTime.of(18, 0));
                if (!WORK_TIME_DAO.add(defaultWorkTime))
                    throw makeException(EXCEPTION_TITLE, "Ошибка при добавлении рабочего времени.");
            } else
                defaultWorkTime = workTimes.get(0);

            ResultSet resultSet = preparedStatement.executeQuery();
            StringBuilder loginStringBuilder = new StringBuilder();
            while (resultSet.next()) {
                Person newPerson = tryCreatePerson(resultSet, defaultGroup, defaultPosition, defaultWorkTime);

                if (newPerson != null) {
                    loginStringBuilder.delete(0, loginStringBuilder.length())
                            .append(newPerson.getLogin());
                    if(containLogin(newPerson.getLogin(), resultList)) {
                        int i = 1;
                        loginStringBuilder.append(i++);
                        while (containLogin(loginStringBuilder.toString(), resultList))
                            loginStringBuilder.delete(loginStringBuilder.length() - Integer.toString(i).length(), loginStringBuilder.length())
                                    .append(i++);
                    }

                    newPerson.setLogin(loginStringBuilder.toString());
                    resultList.add(newPerson);
                }
            }
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }

        return resultList;
    }

    @Override
    public Person findById(long id) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    private boolean containLogin(String login, List<Person> persons) {
        for(Person person : persons)
            if(login.equalsIgnoreCase(person.getLogin()))
                return true;
        return false;
    }

    private Person tryCreatePerson(ResultSet resultSet, Group group, Position position, WorkTime workTime) throws SQLException {
        String persId = resultSet.getString("PERS_ID");
        String ogrnId = resultSet.getString("ORG_ID");
        String lastName = resultSet.getString("LAST_NAME");
        String firstName = resultSet.getString("FIRST_NAME");
        String middleName = resultSet.getString("MIDDLE_NAME");
        if(persId != null && !persId.isEmpty() && lastName != null && !lastName.isEmpty() && firstName != null) {
            boolean admin = ogrnId != null && ogrnId.equals("21353DAE-CB18-4C59-8CF3-0017C563314E");
            String login = convertLogin(lastName, firstName, middleName);
            login = login.replaceAll("['ʹ .,_]+", "");

            //TODO:normal password generator
            String password = DigestUtils.sha256Hex(login);

            return new Person(-1, persId, login, password, lastName, firstName, middleName, group, admin, position, workTime);
        }
        return null;
    }

    private String convertLogin(String lastName, String firstName, String middleName) {
        Transliterator toLatinTrans = Transliterator.getInstance(CYRILLIC_TO_LATIN);
        StringBuilder stringBuilder = new StringBuilder(lastName);

        if(firstName != null && !firstName.isEmpty()) {
            stringBuilder.append("-").append(firstName.charAt(0));

            if (middleName != null && !middleName.isEmpty())
                stringBuilder.append(middleName.charAt(0));
        }
        return toLatinTrans.transliterate(stringBuilder.toString().toLowerCase());
    }

    @Override
    public boolean contains(Person value) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public boolean add(Person person) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public Person findByLogin(String login) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public Person findByPersId(String persId) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public boolean addAll(List<Person> persons) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public boolean update(Person person) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    protected Person tryCreate(ResultSet resultSet) throws SQLException {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }

    @Override
    public boolean remove(Person person) {
        throw new UnsupportedOperationException("This operation doesn't supported.");
    }
}
