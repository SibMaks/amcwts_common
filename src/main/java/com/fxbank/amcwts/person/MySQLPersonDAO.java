package com.fxbank.amcwts.person;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.group.Group;
import com.fxbank.amcwts.person.group.GroupDAO;
import com.fxbank.amcwts.person.group.ProxyGroupDAO;
import com.fxbank.amcwts.person.position.Position;
import com.fxbank.amcwts.person.position.PositionDAO;
import com.fxbank.amcwts.person.position.ProxyPositionDAO;
import com.fxbank.amcwts.person.worktime.ProxyWorkTimeDAO;
import com.fxbank.amcwts.person.worktime.WorkTime;
import com.fxbank.amcwts.person.worktime.WorkTimeDAO;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Instance of {@code PersonDAO}, that can working with MySQL DB. For correct work must be initialized {@code ConnectionFactory}.
 * Class {@code MySQLPersonDAO} is Singleton, to obtain an instance, there is a method {@code getInstance()}.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-02
 */
final class MySQLPersonDAO extends PersonDAO {
    private static PersonDAO PERSON_DAO;

    private final String EXCEPTION_TITLE = "Внутренняя ошибка сервера - Служба работы с сотрудниками не доступна: %s";

    private final String SELECT_BY_GROUP_QUERY = "SELECT * FROM PERSON WHERE GROUP_ID = ?";
    private final String SELECT_BY_ID_QUERY = "SELECT * FROM PERSON WHERE PERSON_ID = ?";
    private final String INSERT_PERSON_QUERY = "INSERT INTO PERSON (`PERS_ID`, `LAST_NAME`, `FIRST_NAME`, " +
            "`MIDDLE_NAME`, `GROUP_ID`, `IS_ADMIN`, `POSITION_ID`, `login`, `WORK_TIME_id`, `PASSWORD`)" +
            " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private final String UPDATE_PERSON_QUERY = "UPDATE PERSON SET PERS_ID = ?, LAST_NAME = ?, " +
            "FIRST_NAME = ?, MIDDLE_NAME = ?, GROUP_ID = ?, IS_ADMIN = ?, POSITION_ID = ?, login = ?, " +
            "password = ?, WORK_TIME_id = ? WHERE PERSON_ID = ?";
    private final String DELETE_PERSON_QUERY = "DELETE FROM PERSON WHERE PERSON_ID = ? AND PERS_ID = ?";

    private final GroupDAO GROUP_DAO = ProxyGroupDAO.getInstance();
    private final PositionDAO POSITION_DAO = ProxyPositionDAO.getInstance();
    private final WorkTimeDAO WORK_TIME_DAO = ProxyWorkTimeDAO.getInstance();
    private final Person ADMIN = new Person();

    private MySQLPersonDAO() {
        super("PERSON");
        SELECT_QUERY = "SELECT * FROM PERSON";
        ADMIN.setAdmin(true);
    }

    /**
     * Method return instance of the MySQLPersonDAO.
     * @return Instance of the MySQLPersonDAO.
     */
    static PersonDAO getInstance() {
        if(PERSON_DAO == null)
            PERSON_DAO = new MySQLPersonDAO();
        return PERSON_DAO;
    }

    @Override
    public List<Person> getAll(Person person) {
        List<Person> resultList = new LinkedList<>();

        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement;
            int accessLevel = Person.getAccessLevel(person);
            switch (accessLevel) {
                case 0:
                    preparedStatement = connection.prepareStatement(SELECT_QUERY);
                    break;
                case 1:
                    preparedStatement = connection.prepareStatement(SELECT_BY_GROUP_QUERY);
                    preparedStatement.setLong(1, person.getGroup().getId());
                    break;
                default:
                    preparedStatement = connection.prepareStatement(SELECT_BY_ID_QUERY);
                    preparedStatement.setLong(1, person.getId());
                    break;
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Person newPerson = tryCreate(resultSet);
                if(newPerson != null)
                    resultList.add(newPerson);
            }
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }

        return resultList;
    }

    @Override
    public List<Person> getAll() {
        return getAll(ADMIN);
    }

    protected Person tryCreate(ResultSet resultSet) throws SQLException {
        Person person = new Person();
        person.setId(resultSet.getLong("PERSON_id"));
        person.setPersonId(resultSet.getString("PERS_ID"));
        person.setLastName(resultSet.getString("LAST_NAME"));
        person.setFirstName(resultSet.getString("FIRST_NAME"));
        person.setMiddleName(resultSet.getString("MIDDLE_NAME"));
        person.setAdmin(resultSet.getBoolean("IS_ADMIN"));
        person.setLogin(resultSet.getString("LOGIN"));
        person.setPassword(resultSet.getString("PASSWORD"));

        long groupId = resultSet.getLong("GROUP_id");
        long positionId = resultSet.getLong("POSITION_id");
        long workTimeId = resultSet.getLong("WORK_TIME_id");

        if (groupId != -1 && positionId != -1 && workTimeId != -1) {
            Position position = POSITION_DAO.findById(positionId);
            if (position != null) {
                person.setPosition(position);
                Group group = GROUP_DAO.findById(groupId);
                if (group != null) {
                    person.setGroup(group);
                    WorkTime workTime = WORK_TIME_DAO.findById(workTimeId);
                    if(workTime != null) {
                        person.setWorkTime(workTime);
                        return person;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public Person findByLogin(String login) {
        return find("LOGIN", login);
    }

    @Override
    public Person findByPersId(String persId) {
        return find("PERS_ID", persId);
    }

    @Override
    public Person findById(long id) {
        return find("PERSON_id", id);
    }

    @Override
    public boolean contains(Person value) {
        return findById(value.getId()) != null;
    }

    @Override
    public boolean add(Person person) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PERSON_QUERY, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, person.getPersonId());
            preparedStatement.setString(2, person.getLastName());
            preparedStatement.setString(3, person.getFirstName());
            preparedStatement.setString(4, person.getMiddleName());
            preparedStatement.setLong(5, person.getGroup().getId());
            preparedStatement.setBoolean(6, person.isAdmin());
            preparedStatement.setLong(7, person.getPosition().getId());
            preparedStatement.setString(8, person.getLogin());
            preparedStatement.setLong(9, person.getWorkTime().getId());
            preparedStatement.setString(10, person.getPassword());

            boolean successUpdate = preparedStatement.executeUpdate() == 1;
            if(successUpdate) {
                long personKey = getGeneratedKeyLong(preparedStatement);
                if(personKey != -1)
                    person.setId(personKey);
                else
                    return false;
            }
            return successUpdate;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean addAll(List<Person> persons) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PERSON_QUERY, Statement.RETURN_GENERATED_KEYS);

            connection.setAutoCommit(false);
            int result = 0;

            for(Person person : persons) {
                preparedStatement.setString(1, person.getPersonId());
                preparedStatement.setString(2, person.getLastName());
                preparedStatement.setString(3, person.getFirstName());
                preparedStatement.setString(4, person.getMiddleName());
                preparedStatement.setLong(5, person.getGroup().getId());
                preparedStatement.setBoolean(6, person.isAdmin());
                preparedStatement.setLong(7, person.getPosition().getId());
                preparedStatement.setString(8, person.getLogin());
                preparedStatement.setLong(9, person.getWorkTime().getId());
                preparedStatement.setString(10, person.getPassword());

                try {
                    boolean successUpdate = preparedStatement.executeUpdate() == 1;
                    if (successUpdate) {
                        long personId = getGeneratedKeyLong(preparedStatement);
                        if(personId != -1) {
                            result++;
                            person.setId(personId);
                        }
                    }
                } catch (SQLException sqlException) {
                    connection.rollback();
                    connection.setAutoCommit(true);
                    throw makeException(EXCEPTION_TITLE, sqlException);
                }
            }

            if(result == persons.size())
                connection.commit();
            else
                connection.rollback();

            connection.setAutoCommit(true);

            return result == persons.size();
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean update(Person person) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_PERSON_QUERY);
            preparedStatement.setString(1, person.getPersonId());
            preparedStatement.setString(2, person.getLastName());
            preparedStatement.setString(3, person.getFirstName());
            preparedStatement.setString(4, person.getMiddleName());
            preparedStatement.setLong(5, person.getGroup().getId());
            preparedStatement.setBoolean(6, person.isAdmin());
            preparedStatement.setLong(7, person.getPosition().getId());
            preparedStatement.setString(8, person.getLogin());
            preparedStatement.setString(9, person.getPassword());
            preparedStatement.setLong(10, person.getWorkTime().getId());
            preparedStatement.setLong(11, person.getId());

            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean remove(Person person) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_PERSON_QUERY);
            preparedStatement.setLong(1, person.getId());
            preparedStatement.setString(2, person.getPersonId());

            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }
}