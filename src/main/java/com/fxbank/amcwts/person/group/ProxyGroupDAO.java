package com.fxbank.amcwts.person.group;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.person.Person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@code ProxyGroupDAO} is a proxy class for {@code GroupDAO}. Class stores instances classes {@code GroupDAO}s.
 * And when {@code ProxyGroupDAO} methods invoke, it's invoke such method in current {@code GroupDAO}.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 * @see GroupDAO
 * @see Group
 * @see ConfigWorker
 */
public final class ProxyGroupDAO extends GroupDAO {
    private static GroupDAO GROUP_DAO;
    private final Map<String, GroupDAO> SUPPORTED_DB = new HashMap<>();

    private ProxyGroupDAO() {
        super("ProxyGroup");
        SUPPORTED_DB.put("MySQL", MySQLGroupDAO.getInstance());
    }

    /**
     * Method return instance of the ProxyGroupDAO.
     * @return Instance of the GroupDAO class for ProxyGroupDAO.
     * @see GroupDAO
     */
    public static GroupDAO getInstance() {
        if(GROUP_DAO == null)
            GROUP_DAO = new ProxyGroupDAO();
        return GROUP_DAO;
    }

    /**
     * Gets the current database and tries to find the corresponding {@code GroupDAO}.
     * @return Instance of GroupDAO for current DB or null.
     * @see GroupDAO
     */
    private GroupDAO getGroupDAO() {
        String currentDB = ConfigWorker.getInstance().getSystemConfig().getCurrentDB();
        if(currentDB != null && !currentDB.isEmpty()) {
            return SUPPORTED_DB.get(currentDB);
        }
        return null;
    }

    @Override
    public boolean add(Group group) {
        GroupDAO groupDAO = getGroupDAO();
        return groupDAO != null && groupDAO.add(group);
    }

    @Override
    public boolean update(Group group) {
        GroupDAO groupDAO = getGroupDAO();
        return groupDAO != null && groupDAO.update(group);
    }

    @Override
    protected Group tryCreate(ResultSet resultSet) throws SQLException {
        return null;
    }

    @Override
    public boolean remove(Group group) {
        GroupDAO groupDAO = getGroupDAO();
        return groupDAO != null && groupDAO.remove(group);
    }

    @Override
    public Group findById(long id) {
        GroupDAO groupDAO = getGroupDAO();
        if(groupDAO != null)
            return groupDAO.findById(id);
        return null;
    }

    @Override
    public boolean contains(Group value) {
        GroupDAO groupDAO = getGroupDAO();
        return groupDAO != null && groupDAO.contains(value);
    }

    @Override
    public List<Group> getAll(Person person) {
        GroupDAO groupDAO = getGroupDAO();
        if(groupDAO != null)
            return groupDAO.getAll(person);
        return Collections.emptyList();
    }

    @Override
    public List<Group> getAll() {
        GroupDAO groupDAO = getGroupDAO();
        if(groupDAO != null)
            return groupDAO.getAll();
        return Collections.emptyList();
    }
}