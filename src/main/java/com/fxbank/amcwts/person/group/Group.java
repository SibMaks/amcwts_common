package com.fxbank.amcwts.person.group;

/**
 * The {@code Group} contains information about group.
 * Normally the values of id is non-negative, name is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-03-27
 */
public final class Group {
    private long id;
    private String name;

    /**
     * Creates a new Group object that represents the group.
     * The fields are initialized with the default values.
     */
    public Group() {
    }

    /**
     * Creates a new Group object that represents the group info.
     * @param id group id.
     * @param name group name.
     */
    public Group(long id, String name) {
        this.name = name;
        this.id = id;
    }

    /**
     * Returns the id of group.
     * @return Returns the id of group.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id of this Group object to the specified value.
     * @param id the new value for the Group object.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns the name of group.
     * @return Returns the name of group.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this Group object to the specified value.
     * @param name the new value for the Group object.
     */
    public void setName(String name) {
        this.name = name;
    }
}
