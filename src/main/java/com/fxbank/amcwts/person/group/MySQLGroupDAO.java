package com.fxbank.amcwts.person.group;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;

import java.sql.*;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Instance of {@code GroupDAO}, that can working with MySQL DB. For correct work must be initialized {@code ConnectionFactory}.
 * Class {@code MySQLGroupDAO} is Singleton, to obtain an instance, there is a method {@link MySQLGroupDAO#getInstance()}.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 * @see GroupDAO
 * @see Group
 * @see ConnectionFactory
 */
final class MySQLGroupDAO extends GroupDAO {
    private static GroupDAO GROUP_DAO;

    private final String EXCEPTION_TITLE = "Внутренняя ошибка сервера - Служба работы с сотрудниками не доступна: %s";

    private final String INSERT_QUERY = "INSERT INTO `GROUP` (`NAME`) VALUES (?)";
    private final String UPDATE_QUERY = "UPDATE `GROUP` SET NAME = ? WHERE GROUP_id = ?";
    private final String DELETE_QUERY = "DELETE FROM `GROUP` WHERE GROUP_id = ?";

    private final Queue<Group> GROUPS = new ConcurrentLinkedQueue<>();

    private MySQLGroupDAO() {
        super("`GROUP`");
        SELECT_QUERY = "SELECT * FROM `GROUP`";
    }

    /**
     * Method return instance of the MySQLGroupDAO.
     * @return Instance of the MySQLGroupDAO.
     */
    static GroupDAO getInstance() {
        if(GROUP_DAO == null)
            GROUP_DAO = new MySQLGroupDAO();
        return GROUP_DAO;
    }

    @Override
    public boolean add(Group group) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, group.getName());

            boolean successUpdate = preparedStatement.executeUpdate() == 1;
            if(successUpdate) {
                long groupKey = getGeneratedKeyLong(preparedStatement);
                if(groupKey != -1) {
                    group.setId(groupKey);
                    GROUPS.add(group);
                } else
                    return false;
            }
            return successUpdate;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean update(Group group) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, group.getName());
            preparedStatement.setLong(2, group.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result) {
                Optional<Group> groupOptional = Optional.ofNullable(findById(group.getId()));
                groupOptional.ifPresent(group1 -> group1.setName(group.getName()));
            }
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    protected Group tryCreate(ResultSet resultSet) throws SQLException {
        long groupId = resultSet.getLong("GROUP_id");

        if(!isCachedWithId(groupId)) {
            Group group = new Group(groupId, resultSet.getString("NAME"));
            GROUPS.add(group);
            return group;
        } else
            return findById(groupId);
    }

    @Override
    public boolean remove(Group group) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
            preparedStatement.setLong(1, group.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result)
                GROUPS.remove(group);
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public Group findById(long id) {
        for(Group group : GROUPS)
            if(group.getId() == id)
                return group;

        return find("GROUP_id", id);
    }

    @Override
    public boolean contains(Group value) {
        return findById(value.getId()) != null;
    }

    private boolean isCachedWithId(long groupId) {
        for(Group group : GROUPS)
            if(group.getId() == groupId)
                return true;
        return false;
    }

    @Override
    public List<Group> getAll(Person person) {
        return getAll();
    }
}