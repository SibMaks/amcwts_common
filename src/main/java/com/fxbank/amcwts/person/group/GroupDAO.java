package com.fxbank.amcwts.person.group;

import com.fxbank.amcwts.CommonDAO;

/**
 * Abstract class for managing a different {@code Group}. Extends CommonDAO with type {@code Group}.
 * This class is abstract for working with group.
 * Used to implement work with various types of databases, for example, such as My SQL, MS SQL.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 * @see ProxyGroupDAO
 * @see Group
 */
abstract public class GroupDAO extends CommonDAO<Group> {
    protected GroupDAO(String tableName) {
        super(tableName);
    }
}
