package com.fxbank.amcwts.person.accesslevel;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.person.Person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@code ProxyAccessLevelDAO} is a proxy class for {@code AccessLevelDAO}. Class stores instances classes {@code AccessLevelDAO}s.
 * And when {@code ProxyAccessLevelDAO} methods invoke, it's invoke such method in current {@code AccessLevelDAO}.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 * @see AccessLevelDAO
 * @see AccessLevel
 * @see ConfigWorker
 */
public final class ProxyAccessLevelDAO extends AccessLevelDAO {
    private static AccessLevelDAO ACCESS_LEVEL_DAO;
    private final Map<String, AccessLevelDAO> SUPPORTED_DB = new HashMap<>();

    private ProxyAccessLevelDAO() {
        super("ProxyAccessLevel");
        SUPPORTED_DB.put("MySQL", MySQLAccessLevelDAO.getInstance());
    }

    /**
     * Method return instance of the ProxyAccessLevelDAO.
     * @return Instance of the AccessLevelDAO class for ProxyAccessLevelDAO.
     * @see AccessLevelDAO
     */
    public static AccessLevelDAO getInstance() {
        if(ACCESS_LEVEL_DAO == null)
            ACCESS_LEVEL_DAO = new ProxyAccessLevelDAO();
        return ACCESS_LEVEL_DAO;
    }

    /**
     * Gets the current database and tries to find the corresponding {@code AccessLevelDAO}.
     * @return Instance of AccessLevelDAO for current DB or null.
     * @see AccessLevelDAO
     */
    private AccessLevelDAO getAccessLevelDAO() {
        String currentDB = ConfigWorker.getInstance().getSystemConfig().getCurrentDB();
        if(currentDB != null && !currentDB.isEmpty()) {
            return SUPPORTED_DB.get(currentDB);
        }
        return null;
    }

    @Override
    public boolean add(AccessLevel accessLevel) {
        AccessLevelDAO accessLevelDAO = getAccessLevelDAO();
        return accessLevelDAO != null && accessLevelDAO.add(accessLevel);
    }

    @Override
    public boolean update(AccessLevel accessLevel) {
        AccessLevelDAO accessLevelDAO = getAccessLevelDAO();
        return accessLevelDAO != null && accessLevelDAO.update(accessLevel);
    }

    @Override
    protected AccessLevel tryCreate(ResultSet resultSet) throws SQLException {
        return null;
    }

    @Override
    public boolean remove(AccessLevel accessLevel) {
        AccessLevelDAO accessLevelDAO = getAccessLevelDAO();
        return accessLevelDAO != null && accessLevelDAO.remove(accessLevel);
    }

    @Override
    public AccessLevel findById(long id) {
        AccessLevelDAO accessLevelDAO = getAccessLevelDAO();
        if(accessLevelDAO != null)
            return accessLevelDAO.findById(id);
        return null;
    }

    @Override
    public boolean contains(AccessLevel value) {
        AccessLevelDAO accessLevelDAO = getAccessLevelDAO();
        return accessLevelDAO != null && accessLevelDAO.contains(value);
    }

    @Override
    public List<AccessLevel> getAll(Person person) {
        AccessLevelDAO accessLevelDAO = getAccessLevelDAO();
        if(accessLevelDAO != null)
            return accessLevelDAO.getAll(person);
        return Collections.emptyList();
    }

    @Override
    public List<AccessLevel> getAll() {
        AccessLevelDAO accessLevelDAO = getAccessLevelDAO();
        if(accessLevelDAO != null)
            return accessLevelDAO.getAll();
        return Collections.emptyList();
    }
}