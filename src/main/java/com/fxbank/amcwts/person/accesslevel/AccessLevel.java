package com.fxbank.amcwts.person.accesslevel;

/**
 * The {@code AccessLevel} contains information about access level.
 * Normally the values of id and level is non-negative, name is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-03-27
 */
public final class AccessLevel {
    private long id;
    private String name;
    private Integer level;

    /**
     * Creates a new AccessLevel object that represents the access level.
     * The fields are initialized with the default values.
     */
    public AccessLevel() {
    }

    /**
     * Creates a new AccessLevel object that represents the access level info.
     * @param id access level id.
     * @param name access level name.
     * @param level access level.
     */
    public AccessLevel(long id, String name, Integer level) {
        this.id = id;
        this.name = name;
        this.level = level;
    }

    /**
     * Returns the id of access level.
     * @return Returns the id of access level.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id of this AccessLevel object to the specified value.
     * @param id the new value for the AccessLevel object.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns the name of access level.
     * @return Returns the name of access level.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this AccessLevel object to the specified value.
     * @param name the new value for the AccessLevel object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the level of access level.
     * @return Returns the level of access level.
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * Sets the level of this AccessLevel object to the specified value.
     * @param level the new value for the AccessLevel object.
     */
    public void setLevel(Integer level) {
        this.level = level;
    }
}
