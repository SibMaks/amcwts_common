package com.fxbank.amcwts.person.accesslevel;

import com.fxbank.amcwts.CommonDAO;

/**
 * Abstract class for managing a different {@code AccessLevel}. Extends CommonDAO with type {@code AccessLevel}.
 * This class is abstract for working with access level.
 * Used to implement work with various types of databases, for example, such as My SQL, MS SQL.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 * @see ProxyAccessLevelDAO
 * @see AccessLevel
 */
abstract public class AccessLevelDAO extends CommonDAO<AccessLevel> {
    protected AccessLevelDAO(String tableName) {
        super(tableName);
    }
}
