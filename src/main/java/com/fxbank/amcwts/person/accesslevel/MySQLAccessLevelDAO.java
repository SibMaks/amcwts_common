package com.fxbank.amcwts.person.accesslevel;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;

import java.sql.*;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Instance of {@code AccessLevelDAO}, that can working with MySQL DB. For correct work must be initialized {@code ConnectionFactory}.
 * Class {@code MySQLAccessLevelDAO} is Singleton, to obtain an instance, there is a method {@link MySQLAccessLevelDAO#getInstance()}.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 */
final class MySQLAccessLevelDAO extends AccessLevelDAO {
    private final String EXCEPTION_TITLE = "Внутренняя ошибка сервера - Служба работы с сотрудниками не доступна: %s";

    private final String INSERT_QUERY = "INSERT INTO ACCESS_LEVEL (`ACCESS_LEVEL_NAME`, `ACCESS_LEVEL`) VALUES (?, ?)";
    private final String UPDATE_QUERY = "UPDATE ACCESS_LEVEL SET ACCESS_LEVEL_name = ?, ACCESS_LEVEL = ? WHERE ACCESS_LEVEL_id = ?";
    private final String DELETE_QUERY = "DELETE FROM ACCESS_LEVEL WHERE ACCESS_LEVEL_id = ?";

    private static AccessLevelDAO ACCESS_LEVEL_DAO;
    private final Queue<AccessLevel> ACCESS_LEVELS;

    private MySQLAccessLevelDAO() {
        super("ACCESS_LEVEL");
        SELECT_QUERY = "SELECT * FROM ACCESS_LEVEL";
        ACCESS_LEVELS = new ConcurrentLinkedQueue<>();
    }

    /**
     * Method return instance of the MySQLAccessLevelDAO.
     * @return Instance of the MySQLAccessLevelDAO.
     */
    static AccessLevelDAO getInstance() {
        if(ACCESS_LEVEL_DAO == null)
            ACCESS_LEVEL_DAO = new MySQLAccessLevelDAO();
        return ACCESS_LEVEL_DAO;
    }

    @Override
    public boolean add(AccessLevel accessLevel) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, accessLevel.getName());
            preparedStatement.setInt(2, accessLevel.getLevel());

            boolean successUpdate = preparedStatement.executeUpdate() == 1;
            if(successUpdate) {
                long accessLevelId = getGeneratedKeyLong(preparedStatement);
                if(accessLevelId != -1){
                    accessLevel.setId(accessLevelId);
                    ACCESS_LEVELS.add(accessLevel);
                } else
                    return false;
            }
            return successUpdate;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean update(AccessLevel accessLevel) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, accessLevel.getName());
            preparedStatement.setInt(2, accessLevel.getLevel());
            preparedStatement.setLong(3, accessLevel.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result) {
                Optional<AccessLevel> accessLevelOptional = Optional.ofNullable(findById(accessLevel.getId()));
                accessLevelOptional.ifPresent(accessLevel1 -> {
                    accessLevel1.setName(accessLevel.getName());
                    accessLevel1.setLevel(accessLevel.getLevel());
                });
            }
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    protected AccessLevel tryCreate(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("ACCESS_LEVEL_id");
        if(!isCachedWithId(id)) {
            AccessLevel accessLevel = new AccessLevel(
                    id,
                    resultSet.getString("ACCESS_LEVEL_name"),
                    resultSet.getInt("ACCESS_LEVEL")
            );
            ACCESS_LEVELS.add(accessLevel);
            return accessLevel;
        } else
            return findById(id);
    }

    @Override
    public boolean remove(AccessLevel accessLevel) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
            preparedStatement.setLong(1, accessLevel.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result)
                ACCESS_LEVELS.remove(accessLevel);
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public AccessLevel findById(long id) {
        for(AccessLevel accessLevel : ACCESS_LEVELS)
            if(accessLevel.getId() == id)
                return accessLevel;

        return find("ACCESS_LEVEL_id", id);
    }

    @Override
    public boolean contains(AccessLevel value) {
        return findById(value.getId()) != null;
    }

    private boolean isCachedWithId(long id) {
        for(AccessLevel accessLevel : ACCESS_LEVELS)
            if(accessLevel.getId() == id)
                return true;
        return false;
    }

    @Override
    public List<AccessLevel> getAll(Person person) {
        return getAll();
    }
}