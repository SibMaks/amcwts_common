package com.fxbank.amcwts.person;

import com.fxbank.amcwts.CommonDAO;

import java.util.List;

/**
 * Abstract class for managing a different {@code Person}. Extends CommonDAO with type {@code Person}.
 * This class is abstract for working with person.
 * Used to implement work with various types of databases, for example, such as My SQL, MS SQL.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-02
 * @see ProxyPersonDAO
 * @see Person
 */
public abstract class PersonDAO extends CommonDAO<Person> {

    protected PersonDAO(String tableName) {
        super(tableName);
    }

    /**
     * Method, that return Person with the specified login.
     * @param login login of Person that need finding.
     * @return Person or null.
     * @see Person
     */
    abstract public Person findByLogin(String login);

    /**
     * Method, that return Person with the specified persId.
     * @param persId persId of Person that need finding.
     * @return Person or null.
     * @see Person
     */
    abstract public Person findByPersId(String persId);

    /**
     * Method, that add list of Person in data base.
     * @param persons List of Person that need add to data base.
     * @return Added or not all Persons in data base. May not be added if the one of Person already exist in data base.
     */
    abstract public boolean addAll(List<Person> persons);
}