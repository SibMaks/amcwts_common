package com.fxbank.amcwts.person;

import com.fxbank.amcwts.person.group.Group;
import com.fxbank.amcwts.person.position.Position;
import com.fxbank.amcwts.person.worktime.WorkTime;

import java.util.Optional;

/**
 * The {@code Person} contains information about person.
 * Normally the values of id is non-negative, personId, login, lastName, firstName, group, position, workTime is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-03-27
 */
public final class Person {
    private long id;
    transient private String personId;
    transient private String login;
    transient private String password;
    private String lastName;
    private String firstName;
    private String middleName;
    transient private Group group;
    transient private boolean admin;
    private Position position;
    transient private WorkTime workTime;

    /**
     * Creates a new Person object that represents the person info.
     * The fields are initialized with the default values.
     */
    public Person() { }

    /**
     * Creates a new Person object that represents the person info.
     * @param id person id.
     * @param personId person id from Parsec.
     * @param login person login for enter in service.
     * @param password person password for enter in service.
     * @param lastName person last name.
     * @param firstName person first name.
     * @param middleName person middle name.
     * @param group person group.
     * @param admin whether the person is an administrator.
     * @param position person position in company.
     * @param workTime person work time.
     */
    public Person(long id, String personId, String login, String password, String lastName, String firstName, String middleName, Group group, boolean admin, Position position, WorkTime workTime) {
        this.id = id;
        this.personId = personId;
        this.login = login;
        this.password = password;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.group = group;
        this.admin = admin;
        this.position = position;
        this.workTime = workTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (id != person.id) return false;
        return personId != null ? personId.equals(person.personId) : person.personId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (personId != null ? personId.hashCode() : 0);
        return result;
    }

    /**
     * Returns the id of person.
     * @return Returns the id of person.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id of this Person object to the specified value.
     * @param id the new value for the Person object.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns the personId of person.
     * @return Returns the personId of person.
     */
    public String getPersonId() {
        return personId;
    }

    /**
     * Sets the person id of this Person object to the specified value.
     * @param personId the new value for the Person object.
     */
    public void setPersonId(String personId) {
        this.personId = personId;
    }

    /**
     * Returns the last name of person.
     * @return Returns the last name of person.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name of this Person object to the specified value.
     * @param lastName the new value for the Person object.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns the first name of person.
     * @return Returns the first name of person.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name of this Person object to the specified value.
     * @param firstName the new value for the Person object.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Returns the middle name of person.
     * @return Returns the middle name of person.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the middle name of this Person object to the specified value.
     * @param middleName the new value for the Person object.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Returns the group of person.
     * @return Returns the group of person.
     * @see Group
     */
    public Group getGroup() {
        return group;
    }

    /**
     * Sets the group of this Person object to the specified value.
     * @param group the new value for the Person object.
     * @see Group
     */
    public void setGroup(Group group) {
        this.group = group;
    }

    /**
     * Returns whether the user is an administrator.
     * @return Returns whether the user is an administrator.
     */
    public boolean isAdmin() {
        return admin;
    }

    /**
     * Sets the whether the Person is an admin.
     * @param admin the new value for the Person object.
     */
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    /**
     * Returns the position of person.
     * @return Returns the position of person.
     * @see Position
     */
    public Position getPosition() {
        return position;
    }

    /**
     * Sets the position of this Person object to the specified value.
     * @param position the new value for the Person object.
     * @see Position
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * Returns the login of person.
     * @return Returns the login of person.
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the login of this Person object to the specified value.
     * @param login the new value for the Person object.
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Returns the password of person.
     * @return Returns the password of person.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password of this Person object to the specified value.
     * @param password the new value for the Person object.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns the work time of person.
     * @return Returns the work time of person.
     * @see WorkTime
     */
    public WorkTime getWorkTime() {
        return workTime;
    }

    /**
     * Sets the work time of this Person object to the specified value.
     * @param workTime the new value for the Person object.
     * @see WorkTime
     */
    public void setWorkTime(WorkTime workTime) {
        this.workTime = workTime;
    }

    /**
     * Returns the access level of person.
     * @return Returns the access level of Person.
     */
    public static Integer getAccessLevel(Person person) {
        Optional<Person> personOptional = Optional.ofNullable(person);

        return personOptional.map(Person::isAdmin).orElse(false) ?  0 :
                personOptional.flatMap(person1 -> Optional.ofNullable(person1.getPosition()))
                        .flatMap(position -> Optional.ofNullable(position.getAccessLevel()))
                        .flatMap(accessLevel -> Optional.ofNullable(accessLevel.getLevel()))
                        .orElse(-1);
    }
}
