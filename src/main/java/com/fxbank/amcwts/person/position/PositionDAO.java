package com.fxbank.amcwts.person.position;

import com.fxbank.amcwts.CommonDAO;

/**
 * Abstract class for managing a different {@code Position}. Extends CommonDAO with type {@code Position}.
 * This class is abstract for working with position.
 * Used to implement work with various types of databases, for example, such as My SQL, MS SQL.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 * @see ProxyPositionDAO
 * @see Position
 */
abstract public class PositionDAO extends CommonDAO<Position> {

    protected PositionDAO(String tableName) {
        super(tableName);
    }
}
