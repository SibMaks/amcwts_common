package com.fxbank.amcwts.person.position;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.person.accesslevel.AccessLevel;
import com.fxbank.amcwts.person.accesslevel.AccessLevelDAO;
import com.fxbank.amcwts.person.accesslevel.ProxyAccessLevelDAO;

import java.sql.*;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Instance of {@code PositionDAO}, that can working with MySQL DB. For correct work must be initialized {@code ConnectionFactory}.
 * Class {@code MySQLPositionDAO} is Singleton, to obtain an instance, there is a method {@link MySQLPositionDAO#getInstance()}.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 * @see MySQLPositionDAO
 * @see Position
 * @see ConnectionFactory
 */
final class MySQLPositionDAO extends PositionDAO {
    private static PositionDAO POSITION_DAO;

    private final String EXCEPTION_TITLE = "Внутренняя ошибка сервера - Служба работы с сотрудниками не доступна: %s";

    private final String INSERT_QUERY = "INSERT INTO `POSITION` (`POSITION_NAME`, `ACCESS_LEVEL_id`) VALUES (?, ?)";
    private final String UPDATE_QUERY = "UPDATE `POSITION` SET POSITION_name = ?, ACCESS_LEVEL_id = ? WHERE POSITION_id = ?";
    private final String DELETE_QUERY = "DELETE FROM `POSITION` WHERE POSITION_id = ?";

    private final Queue<Position> POSITIONS = new ConcurrentLinkedQueue<>();
    private final AccessLevelDAO ACCESS_LEVEL_DAO = ProxyAccessLevelDAO.getInstance();

    private MySQLPositionDAO() {
        super("`POSITION`");
        SELECT_QUERY = "SELECT * FROM `POSITION`";
    }

    /**
     * Method return instance of the MySQLPositionDAO.
     * @return Instance of the MySQLPositionDAO.
     */
    static PositionDAO getInstance() {
        if(POSITION_DAO == null)
            POSITION_DAO = new MySQLPositionDAO();
        return POSITION_DAO;
    }

    @Override
    public boolean add(Position position) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, position.getName());
            preparedStatement.setLong(2, position.getAccessLevel().getId());

            boolean successUpdate = preparedStatement.executeUpdate() == 1;
            if(successUpdate) {
                long posKey = getGeneratedKeyLong(preparedStatement);
                if(posKey != -1) {
                    position.setId(posKey);
                    POSITIONS.add(position);
                } else
                    return false;
            }
            return successUpdate;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean update(Position position) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, position.getName());
            preparedStatement.setLong(2, position.getAccessLevel().getId());
            preparedStatement.setLong(3, position.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result) {
                Optional<Position> positionOptional = Optional.ofNullable(findById(position.getId()));
                positionOptional.ifPresent(position1 -> {
                    position1.setName(position.getName());
                    position1.setAccessLevel(position.getAccessLevel());
                });
            }
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    protected Position tryCreate(ResultSet resultSet) throws SQLException {
        long positionId = resultSet.getLong("POSITION_id");

        if(!isCachedWithId(positionId)) {
            long accessLevelId = resultSet.getLong("ACCESS_LEVEL_id");
            AccessLevel accessLevel = ACCESS_LEVEL_DAO.findById(accessLevelId);
            if(accessLevel != null) {
                Position position = new Position();
                position.setId(positionId);
                position.setName(resultSet.getString("POSITION_name"));
                position.setAccessLevel(accessLevel);
                POSITIONS.add(position);
                return position;
            }
        } else
            return findById(positionId);
        return null;
    }

    @Override
    public boolean remove(Position position) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
            preparedStatement.setLong(1, position.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result)
                POSITIONS.remove(position);
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public Position findById(long id) {
        for(Position position : POSITIONS)
            if(position.getId() == id)
                return position;

        return find("POSITION_id", id);
    }

    @Override
    public boolean contains(Position value) {
        return findById(value.getId()) != null;
    }

    private boolean isCachedWithId(long id) {
        for(Position position : POSITIONS)
            if(position.getId() == id)
                return true;
        return false;
    }

    @Override
    public List<Position> getAll(Person person) {
        return getAll();
    }
}