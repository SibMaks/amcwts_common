package com.fxbank.amcwts.person.position;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.person.Person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@code ProxyPositionDAO} is a proxy class for {@code PositionDAO}. Class stores instances classes {@code PositionDAO}s.
 * And when {@code ProxyPositionDAO} methods invoke, it's invoke such method in current {@code PositionDAO}.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 * @see PositionDAO
 * @see Position
 * @see ConfigWorker
 */
public final class ProxyPositionDAO extends PositionDAO {
    private static PositionDAO POSITION_DAO;
    private final Map<String, PositionDAO> SUPPORTED_DB = new HashMap<>();

    private ProxyPositionDAO() {
        super("ProxyPosition");
        SUPPORTED_DB.put("MySQL", MySQLPositionDAO.getInstance());
    }

    /**
     * Method return instance of the ProxyPositionDAO.
     * @return Instance of the PositionDAO class for ProxyPositionDAO.
     * @see PositionDAO
     */
    public static PositionDAO getInstance() {
        if(POSITION_DAO == null)
            POSITION_DAO = new ProxyPositionDAO();
        return POSITION_DAO;
    }

    /**
     * Gets the current database and tries to find the corresponding {@code PositionDAO}.
     * @return Instance of PositionDAO for current DB or null.
     * @see PositionDAO
     */
    private PositionDAO getPositionDAO() {
        String currentDB = ConfigWorker.getInstance().getSystemConfig().getCurrentDB();
        if(currentDB != null && !currentDB.isEmpty()) {
            return SUPPORTED_DB.get(currentDB);
        }
        return null;
    }

    @Override
    public boolean add(Position position) {
        PositionDAO positionDAO = getPositionDAO();
        return positionDAO != null && positionDAO.add(position);
    }

    @Override
    public boolean update(Position position) {
        PositionDAO positionDAO = getPositionDAO();
        return positionDAO != null && positionDAO.update(position);
    }

    @Override
    protected Position tryCreate(ResultSet resultSet) throws SQLException {
        return null;
    }

    @Override
    public boolean remove(Position position) {
        PositionDAO positionDAO = getPositionDAO();
        return positionDAO != null && positionDAO.remove(position);
    }

    @Override
    public Position findById(long id) {
        PositionDAO positionDAO = getPositionDAO();
        if(positionDAO != null)
            return positionDAO.findById(id);
        return null;
    }

    @Override
    public boolean contains(Position value) {
        PositionDAO positionDAO = getPositionDAO();
        return positionDAO != null && positionDAO.contains(value);
    }

    @Override
    public List<Position> getAll(Person person) {
        PositionDAO positionDAO = getPositionDAO();
        if(positionDAO != null)
            return positionDAO.getAll(person);
        return Collections.emptyList();
    }

    @Override
    public List<Position> getAll() {
        PositionDAO positionDAO = getPositionDAO();
        if(positionDAO != null)
            return positionDAO.getAll();
        return Collections.emptyList();
    }
}