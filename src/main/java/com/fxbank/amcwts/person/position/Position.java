package com.fxbank.amcwts.person.position;

import com.fxbank.amcwts.person.accesslevel.AccessLevel;

/**
 * The {@code Position} contains information about position.
 * Normally the values of id is non-negative, name and accessLevel is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-03-27
 */
public final class Position {
    private long id;
    private String name;
    private AccessLevel accessLevel;

    /**
     * Creates a new Position object that represents the position.
     * The fields are initialized with the default values.
     */
    public Position() {
    }

    /**
     * Creates a new Position object that represents the position info.
     * @param id position id.
     * @param name position name.
     * @param accessLevel access level.
     * @see AccessLevel
     */
    public Position(long id, String name, AccessLevel accessLevel) {
        this.id = id;
        this.name = name;
        this.accessLevel = accessLevel;
    }

    /**
     * Returns the id of position.
     * @return Returns the id of position.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id of this Position object to the specified value.
     * @param id the new value for the Position object.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns the name of position.
     * @return Returns the name of position.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this Position object to the specified value.
     * @param name the new value for the Position object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the access level of position.
     * @return Returns the access level of position.
     * @see AccessLevel
     */
    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    /**
     * Sets the access level of this Position object to the specified value.
     * @param accessLevel the new value for the Position object.
     * @see AccessLevel
     */
    public void setAccessLevel(AccessLevel accessLevel) {
        this.accessLevel = accessLevel;
    }
}
