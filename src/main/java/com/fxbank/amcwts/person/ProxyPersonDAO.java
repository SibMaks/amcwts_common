package com.fxbank.amcwts.person;

import com.fxbank.amcwts.conf.ConfigWorker;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The {@code ProxyPersonDAO} is a proxy class for {@code PersonDAO}. Class stores instances classes {@code PersonDAO}s.
 * And when {@code ProxyPersonDAO} methods invoke, it's invoke such method in current {@code PersonDAO}.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-11
 * @see PersonDAO
 * @see Person
 * @see ConfigWorker
 */
public final class ProxyPersonDAO extends PersonDAO {
    private static PersonDAO PERSON_DAO;
    private final Map<String, PersonDAO> SUPPORTED_DB = new ConcurrentHashMap<>();

    private ProxyPersonDAO() {
        super("ProxyPerson");
        SUPPORTED_DB.put("MySQL", MySQLPersonDAO.getInstance());
    }

    /**
     * Method return instance of the ProxyPersonDAO.
     * @return Instance of the PersonDAO class for ProxyPersonDAO.
     * @see PersonDAO
     */
    public static PersonDAO getInstance() {
        if(PERSON_DAO == null)
            PERSON_DAO = new ProxyPersonDAO();
        return PERSON_DAO;
    }

    /**
     * Gets the current database and tries to find the corresponding {@code PersonDAO}.
     * @return Instance of PersonDAO for current DB or null.
     * @see PersonDAO
     */
    private PersonDAO getPersonDAO() {
        String currentDB = ConfigWorker.getInstance().getSystemConfig().getCurrentDB();
        if(currentDB != null && !currentDB.isEmpty()) {
            return SUPPORTED_DB.get(currentDB);
        }
        return null;
    }

    @Override
    public List<Person> getAll(Person person) {
        PersonDAO personDAO = getPersonDAO();
        if(personDAO != null)
            return personDAO.getAll(person);
        return Collections.emptyList();
    }

    @Override
    public List<Person> getAll() {
        PersonDAO personDAO = getPersonDAO();
        if(personDAO != null)
            return personDAO.getAll();
        return Collections.emptyList();
    }

    @Override
    public Person findByLogin(String login) {
        PersonDAO personDAO = getPersonDAO();
        if(personDAO != null)
            return personDAO.findByLogin(login);
        return null;
    }

    @Override
    public Person findByPersId(String persId) {
        PersonDAO personDAO = getPersonDAO();
        if(personDAO != null)
            return personDAO.findByPersId(persId);
        return null;
    }

    @Override
    public Person findById(long id) {
        PersonDAO personDAO = getPersonDAO();
        if(personDAO != null)
            return personDAO.findById(id);
        return null;
    }

    @Override
    public boolean contains(Person value) {
        PersonDAO personDAO = getPersonDAO();
        return personDAO != null && personDAO.contains(value);
    }

    @Override
    public boolean add(Person person) {
        PersonDAO personDAO = getPersonDAO();
        return personDAO != null && personDAO.add(person);
    }

    @Override
    public boolean addAll(List<Person> persons) {
        PersonDAO personDAO = getPersonDAO();
        return personDAO != null && personDAO.addAll(persons);
    }

    @Override
    public boolean update(Person person) {
        PersonDAO personDAO = getPersonDAO();
        return personDAO != null && personDAO.update(person);
    }

    @Override
    protected Person tryCreate(ResultSet resultSet) throws SQLException {
        return null;
    }

    @Override
    public boolean remove(Person person) {
        PersonDAO personDAO = getPersonDAO();
        return personDAO != null && personDAO.remove(person);
    }
}
