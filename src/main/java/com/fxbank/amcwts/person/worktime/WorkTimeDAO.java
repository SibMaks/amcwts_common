package com.fxbank.amcwts.person.worktime;

import com.fxbank.amcwts.CommonDAO;

/**
 * Abstract class for managing a different {@code WorkTime}. Extends CommonDAO with type {@code WorkTime}.
 * This class is abstract for working with work time.
 * Used to implement work with various types of databases, for example, such as My SQL, MS SQL.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 * @see ProxyWorkTimeDAO
 * @see WorkTime
 */
abstract public class WorkTimeDAO extends CommonDAO<WorkTime> {

    protected WorkTimeDAO(String tableName) {
        super(tableName);
    }
}
