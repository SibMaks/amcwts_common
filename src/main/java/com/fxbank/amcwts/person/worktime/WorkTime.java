package com.fxbank.amcwts.person.worktime;

import java.time.LocalTime;

/**
 * The {@code WorkTime} contains information about work time.
 * Normally the values of id is non-negative, timeStart and timeEnd is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-07
 */
public final class WorkTime {
    private long id;
    private LocalTime timeStart;
    private LocalTime timeEnd;

    /**
     * Creates a new WorkTime object that represents the work time info.
     * Time start init with value 9:00
     * Time end init with value 18:00
     * @see LocalTime
     */
    public WorkTime() {
        this.timeStart = LocalTime.of(9, 0);
        this.timeEnd = LocalTime.of(18, 0);
    }

    /**
     * Creates a new WorkTime object that represents the work time info.
     * @param id work time id.
     * @param timeStart time of start work time.
     * @param timeEnd time of end work time.
     * @see LocalTime
     */
    public WorkTime(long id, LocalTime timeStart, LocalTime timeEnd) {
        this.id = id;
        checkObject(timeStart, timeEnd);
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
    }

    /**
     * Returns the id of work time.
     * @return Returns the id of work time.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id of this WorkTime object to the specified value.
     * @param id the new value for the WorkTime object.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns the time of start of work time.
     * @return Returns the time of start of work time.
     */
    public LocalTime getTimeStart() {
        return timeStart;
    }

    /**
     * Sets the time of start of this WorkTime object to the specified value.
     * @param timeStart the new value for the WorkTime object.
     * @throws NullPointerException if timeStart is null.
     * @throws IllegalArgumentException if timeStart longer that time end.
     */
    public void setTimeStart(LocalTime timeStart) {
        checkObject(timeStart, timeEnd);
        this.timeStart = timeStart;
    }

    /**
     * Returns the time of end of work time.
     * @return Returns the time of end of work time.
     */
    public LocalTime getTimeEnd() {
        return timeEnd;
    }

    /**
     * Sets the time of end of this WorkTime object to the specified value.
     * @param timeEnd the new value for the WorkTime object.
     * @throws NullPointerException if timeEnd is null.
     * @throws IllegalArgumentException if timeEnd shortly that time start.
     */
    public void setTimeEnd(LocalTime timeEnd) {
        checkObject(timeStart, timeEnd);
        this.timeEnd = timeEnd;
    }

    private void checkObject(LocalTime timeStart, LocalTime timeEnd) {
        if(timeStart != null && timeEnd != null) {
            if(timeStart.compareTo(timeEnd) > 0)
                throw new IllegalArgumentException("The start time must be shorter than the end time.");
        } else
            throw new NullPointerException("Time start of work day and time end of work day can't be null.");
    }
}
