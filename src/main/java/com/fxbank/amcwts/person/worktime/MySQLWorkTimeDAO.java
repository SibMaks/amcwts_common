package com.fxbank.amcwts.person.worktime;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;

import java.sql.*;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Instance of {@code WorkTimeDAO}, that can working with MySQL DB. For correct work must be initialized {@code ConnectionFactory}.
 * Class {@code MySQLWorkTimeDAO} is Singleton, to obtain an instance, there is a method {@link MySQLWorkTimeDAO#getInstance()}.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 * @see WorkTime
 * @see WorkTimeDAO
 * @see ConnectionFactory
 */
final class MySQLWorkTimeDAO extends WorkTimeDAO {
    private static WorkTimeDAO WORK_TIME_DAO;

    private final String EXCEPTION_TITLE = "Внутренняя ошибка сервера - Служба работы с сотрудниками не доступна: %s";

    private final String INSERT_QUERY = "INSERT INTO WORK_TIME (`TIME_START`, `TIME_END`) VALUES (?, ?)";
    private final String UPDATE_QUERY = "UPDATE WORK_TIME SET TIME_START = ?, TIME_END = ? WHERE WORK_TIME_id = ?";
    private final String DELETE_QUERY = "DELETE FROM WORK_TIME WHERE WORK_TIME_id = ?";

    private final Queue<WorkTime> WORK_TIMES = new ConcurrentLinkedQueue<>();

    private MySQLWorkTimeDAO() {
        super("WORK_TIME");
        SELECT_QUERY = "SELECT * FROM WORK_TIME";
    }

    /**
     * Method return instance of the MySQLWorkTimeDAO.
     * @return Instance of the MySQLWorkTimeDAO.
     */
    static WorkTimeDAO getInstance() {
        if(WORK_TIME_DAO == null)
            WORK_TIME_DAO = new MySQLWorkTimeDAO();
        return WORK_TIME_DAO;
    }

    @Override
    public boolean add(WorkTime workTime) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, workTime.getTimeStart().toString());
            preparedStatement.setString(2, workTime.getTimeEnd().toString());

            boolean successUpdate = preparedStatement.executeUpdate() == 1;
            if(successUpdate) {
                long workTimeKey = getGeneratedKeyLong(preparedStatement);
                if(workTimeKey != -1) {
                    workTime.setId(workTimeKey);
                    WORK_TIMES.add(workTime);
                } else
                    return false;
            }
            return successUpdate;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean update(WorkTime workTime) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, workTime.getTimeStart().toString());
            preparedStatement.setString(2, workTime.getTimeEnd().toString());
            preparedStatement.setLong(3, workTime.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result) {
                Optional<WorkTime> workTimeOptional = Optional.ofNullable(findById(workTime.getId()));
                workTimeOptional.ifPresent(workTime1 -> {
                    workTime1.setTimeStart(workTime.getTimeStart());
                    workTime1.setTimeEnd(workTime.getTimeEnd());
                });
            }
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    protected WorkTime tryCreate(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("WORK_TIME_id");
        if(!isCachedWithId(id)) {
            LocalTime timeStart = LocalTime.parse(resultSet.getString("TIME_START"));
            LocalTime timeEnd = LocalTime.parse(resultSet.getString("TIME_END"));
            WorkTime workTime = new WorkTime(id, timeStart, timeEnd);
            WORK_TIMES.add(workTime);
            return workTime;
        } else
            return findById(id);
    }

    @Override
    public boolean remove(WorkTime workTime) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
            preparedStatement.setLong(1, workTime.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result)
                WORK_TIMES.remove(workTime);
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    private boolean isCachedWithId(long id) {
        for(WorkTime workTime : WORK_TIMES)
            if(workTime.getId() == id)
                return true;
        return false;
    }

    @Override
    public List<WorkTime> getAll(Person person) {
        return getAll();
    }

    @Override
    public WorkTime findById(long id) {
        for(WorkTime workTime : WORK_TIMES)
            if(workTime.getId() == id)
                return workTime;

        return find("WORK_TIME_id", id);
    }

    @Override
    public boolean contains(WorkTime value) {
        return findById(value.getId()) != null;
    }
}