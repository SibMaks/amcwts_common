package com.fxbank.amcwts.person.worktime;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.person.Person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@code ProxyWorkTimeDAO} is a proxy class for {@code WorkTimeDAO}. Class stores instances classes {@code WorkTimeDAO}s.
 * And when {@code ProxyWorkTimeDAO} methods invoke, it's invoke such method in current {@code WorkTimeDAO}.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-29
 * @see WorkTimeDAO
 * @see WorkTime
 * @see ConfigWorker
 */
public final class ProxyWorkTimeDAO extends WorkTimeDAO {
    private static WorkTimeDAO WORK_TIME_DAO;
    private final Map<String, WorkTimeDAO> SUPPORTED_DB = new HashMap<>();

    private ProxyWorkTimeDAO() {
        super("ProxyWorkTime");
        SUPPORTED_DB.put("MySQL", MySQLWorkTimeDAO.getInstance());
    }

    /**
     * Method return instance of the ProxyWorkTimeDAO.
     * @return Instance of the WorkTimeDAO class for ProxyWorkTimeDAO.
     * @see WorkTimeDAO
     */
    public static WorkTimeDAO getInstance() {
        if(WORK_TIME_DAO == null)
            WORK_TIME_DAO = new ProxyWorkTimeDAO();
        return WORK_TIME_DAO;
    }

    /**
     * Gets the current database and tries to find the corresponding {@code WorkTimeDAO}.
     * @return Instance of WorkTimeDAO for current DB or null.
     * @see WorkTimeDAO
     */
    private WorkTimeDAO getWorkTimeDAO() {
        String currentDB = ConfigWorker.getInstance().getSystemConfig().getCurrentDB();
        if(currentDB != null && !currentDB.isEmpty()) {
            return SUPPORTED_DB.get(currentDB);
        }
        return null;
    }

    @Override
    public boolean add(WorkTime workTime) {
        WorkTimeDAO workTimeDAO = getWorkTimeDAO();
        return workTimeDAO != null && workTimeDAO.add(workTime);
    }

    @Override
    public boolean update(WorkTime workTime) {
        WorkTimeDAO workTimeDAO = getWorkTimeDAO();
        return workTimeDAO != null && workTimeDAO.update(workTime);
    }

    @Override
    protected WorkTime tryCreate(ResultSet resultSet) throws SQLException {
        return null;
    }

    @Override
    public boolean remove(WorkTime workTime) {
        WorkTimeDAO workTimeDAO = getWorkTimeDAO();
        return workTimeDAO != null && workTimeDAO.remove(workTime);
    }

    @Override
    public List<WorkTime> getAll(Person person) {
        WorkTimeDAO workTimeDAO = getWorkTimeDAO();
        if(workTimeDAO != null)
            return workTimeDAO.getAll(person);
        return Collections.emptyList();
    }

    @Override
    public List<WorkTime> getAll() {
        WorkTimeDAO workTimeDAO = getWorkTimeDAO();
        if(workTimeDAO != null)
            return workTimeDAO.getAll();
        return Collections.emptyList();
    }

    @Override
    public WorkTime findById(long id) {
        WorkTimeDAO workTimeDAO = getWorkTimeDAO();
        if(workTimeDAO != null)
            return workTimeDAO.findById(id);
        return null;
    }

    @Override
    public boolean contains(WorkTime value) {
        WorkTimeDAO workTimeDAO = getWorkTimeDAO();
        return workTimeDAO != null && workTimeDAO.contains(value);
    }

}