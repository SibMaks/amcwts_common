package com.fxbank.amcwts.violation;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.person.Person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * The {@code ProxyViolationDAO} is a proxy class for {@code ViolationDAO}. Class stores instances classes {@code ViolationDAO}s.
 * And when {@code ProxyViolationDAO} methods invoke, it's invoke such method in current {@code ViolationDAO}.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-11
 * @see ViolationDAO
 * @see Violation
 * @see ConfigWorker
 */
public final class ProxyViolationDAO extends ViolationDAO {
    private static ViolationDAO VIOLATION_DAO;
    private final Map<String, ViolationDAO> SUPPORTED_DB = new HashMap<>();

    private ProxyViolationDAO() {
        super("ProxyViolation");
        SUPPORTED_DB.put("MySQL", MySQLViolationDAO.getInstance());
    }

    /**
     * Method return instance of the ProxyViolationDAO.
     * @return Instance of the ViolationDAO class for ProxyViolationDAO.
     * @see ViolationDAO
     */
    public static ViolationDAO getInstance() {
        if(VIOLATION_DAO == null)
            VIOLATION_DAO = new ProxyViolationDAO();
        return VIOLATION_DAO;
    }

    /**
     * Gets the current database and tries to find the corresponding {@code ViolationDAO}.
     * @return Instance of ViolationDAO for current DB or null.
     * @see ViolationDAO
     */
    private ViolationDAO getViolationDAO() {
        String currentDB = ConfigWorker.getInstance().getSystemConfig().getCurrentDB();
        if(currentDB != null && !currentDB.isEmpty()) {
            return SUPPORTED_DB.get(currentDB);
        }
        return null;
    }

    @Override
    public List<Violation> getAll(Person person) {
        ViolationDAO violationDAO = getViolationDAO();
        if(violationDAO != null)
            return violationDAO.getAll(person);
        return Collections.emptyList();
    }

    @Override
    public List<Violation> getAll() {
        ViolationDAO violationDAO = getViolationDAO();
        if(violationDAO != null)
            return violationDAO.getAll();
        return Collections.emptyList();
    }

    @Override
    public List<Violation> getByDatesForPerson(Person person, LocalDateTime from, LocalDateTime to) {
        ViolationDAO violationDAO = getViolationDAO();
        if(violationDAO != null)
            return violationDAO.getByDatesForPerson(person, from, to);
        return Collections.emptyList();
    }

    @Override
    public List<Violation> getAllByDates(Person person, LocalDateTime from, LocalDateTime to) {
        ViolationDAO violationDAO = getViolationDAO();
        if(violationDAO != null)
            return violationDAO.getAllByDates(person, from , to);
        return Collections.emptyList();
    }

    @Override
    public Violation findById(long id) {
        ViolationDAO violationDAO = getViolationDAO();
        if(violationDAO != null)
            return violationDAO.findById(id);
        return null;
    }

    @Override
    public boolean contains(Violation violation) {
        ViolationDAO violationDAO = getViolationDAO();
        return violationDAO != null && violationDAO.contains(violation);
    }

    @Override
    public Violation getLast(Person person) {
        ViolationDAO violationDAO = getViolationDAO();
        if(violationDAO != null)
            return violationDAO.getLast(person);
        return null;
    }

    @Override
    public boolean add(Violation violation) {
        ViolationDAO violationDAO = getViolationDAO();
        return violationDAO != null && violationDAO.add(violation);
    }

    @Override
    public boolean remove(Violation violation) {
        ViolationDAO violationDAO = getViolationDAO();
        return violationDAO != null && violationDAO.remove(violation);
    }

    @Override
    public boolean update(Violation violation) {
        ViolationDAO violationDAO = getViolationDAO();
        return violationDAO != null && violationDAO.update(violation);
    }

    @Override
    protected Violation tryCreate(ResultSet resultSet) throws SQLException {
        return null;
    }
}
