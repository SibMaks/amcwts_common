package com.fxbank.amcwts.violation;

import com.fxbank.amcwts.CommonDAO;
import com.fxbank.amcwts.person.Person;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Abstract class for managing a different {@code Violation}. Extends CommonDAO with type {@code Violation}.
 * This class is abstract for working with violation.
 * Used to implement work with various types of databases, for example, such as My SQL, MS SQL.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-02
 * @see ProxyViolationDAO
 * @see Violation
 */
public abstract class ViolationDAO extends CommonDAO<Violation> {

    protected ViolationDAO(String tableName) {
        super(tableName);
    }

    /**
     * A method that returns a list of violations occurred at the specified time committed by the user.
     * @param person user, who request get violation. It's need to limit access.
     * @param from start LocalDateTime for search.
     * @param to end LocalDateTime for search.
     * @return List of violations of user or empty list.
     */
    abstract public List<Violation> getByDatesForPerson(Person person, LocalDateTime from, LocalDateTime to);

    /**
     * A method that returns a list of violations occurred at the specified time accessible for the user.
     * @param person user, who request get violation. It's need to limit access.
     * @param from start LocalDateTime for search.
     * @param to end LocalDateTime for search.
     * @return List of violations of user or empty list.
     */
    abstract public List<Violation> getAllByDates(Person person, LocalDateTime from, LocalDateTime to);


    /**
     * Method, that return last Violation to which the user has access.
     * @param person user, who request get Violation. It's need to limit access.
     * @return Violation or null.
     */
    abstract public Violation getLast(Person person);

}
