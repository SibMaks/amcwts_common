package com.fxbank.amcwts.violation.violationtype;

import com.fxbank.amcwts.CommonDAO;

/**
 * Abstract class for managing a different {@code ViolationType}. Extends CommonDAO with type {@code ViolationType}.
 * This class is abstract for working with violation type.
 * Used to implement work with various types of databases, for example, such as My SQL, MS SQL.
 * @author Maksim
 * @version 0.1
 * @since 2017-05-01
 * @see ProxyViolationTypeDAO
 * @see ViolationType
 */
public abstract class ViolationTypeDAO extends CommonDAO<ViolationType> {

    protected ViolationTypeDAO(String tableName) {
        super(tableName);
    }
}
