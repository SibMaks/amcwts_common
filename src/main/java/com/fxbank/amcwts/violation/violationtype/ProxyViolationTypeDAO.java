package com.fxbank.amcwts.violation.violationtype;

import com.fxbank.amcwts.conf.ConfigWorker;
import com.fxbank.amcwts.person.Person;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * The {@code ProxyViolationTypeDAO} is a proxy class for {@code ViolationTypeDAO}. Class stores instances classes {@code ViolationTypeDAO}s.
 * And when {@code ProxyViolationTypeDAO} methods invoke, it's invoke such method in current {@code ViolationTypeDAO}.
 * @author Maksim
 * @version 0.1
 * @since 2017-05-01
 * @see ViolationTypeDAO
 * @see ViolationType
 * @see ConfigWorker
 */
public final class ProxyViolationTypeDAO extends ViolationTypeDAO {
    private static ViolationTypeDAO VIOLATION_TYPE_DAO;
    private final Map<String, ViolationTypeDAO> SUPPORTED_DB = new HashMap<>();

    private ProxyViolationTypeDAO() {
        super("ProxyViolationType");
        SUPPORTED_DB.put("MySQL", MySQLViolationTypeDAO.getInstance());
    }

    /**
     * Method return instance of the ProxyViolationTypeDAO.
     * @return Instance of the ViolationTypeDAO class for ProxyViolationTypeDAO.
     * @see ViolationTypeDAO
     */
    public static ViolationTypeDAO getInstance() {
        if(VIOLATION_TYPE_DAO == null)
            VIOLATION_TYPE_DAO = new ProxyViolationTypeDAO();
        return VIOLATION_TYPE_DAO;
    }

    /**
     * Gets the current database and tries to find the corresponding {@code ViolationTypeDAO}.
     * @return Instance of ViolationTypeDAO for current DB or null.
     * @see ViolationTypeDAO
     */
    private ViolationTypeDAO getViolationTypeDAO() {
        String currentDB = ConfigWorker.getInstance().getSystemConfig().getCurrentDB();
        if(currentDB != null && !currentDB.isEmpty()) {
            return SUPPORTED_DB.get(currentDB);
        }
        return null;
    }

    @Override
    public boolean add(ViolationType violationType) {
        ViolationTypeDAO violationTypeDAO = getViolationTypeDAO();
        return violationTypeDAO != null && violationTypeDAO.add(violationType);
    }

    @Override
    public boolean update(ViolationType violationType) {
        ViolationTypeDAO violationTypeDAO = getViolationTypeDAO();
        return violationTypeDAO != null && violationTypeDAO.update(violationType);
    }

    @Override
    protected ViolationType tryCreate(ResultSet resultSet) throws SQLException {
        return null;
    }

    @Override
    public boolean remove(ViolationType violationType) {
        ViolationTypeDAO violationTypeDAO = getViolationTypeDAO();
        return violationTypeDAO != null && violationTypeDAO.remove(violationType);
    }

    @Override
    public ViolationType findById(long id) {
        ViolationTypeDAO violationTypeDAO = getViolationTypeDAO();
        if(violationTypeDAO != null)
            return violationTypeDAO.findById(id);
        return null;
    }

    @Override
    public List<ViolationType> getAll(Person person) {
        ViolationTypeDAO violationTypeDAO = getViolationTypeDAO();
        if(violationTypeDAO != null)
            return violationTypeDAO.getAll(person);
        return Collections.emptyList();
    }

    @Override
    public List<ViolationType> getAll() {
        ViolationTypeDAO violationTypeDAO = getViolationTypeDAO();
        if(violationTypeDAO != null)
            return violationTypeDAO.getAll();
        return Collections.emptyList();
    }

    @Override
    public boolean contains(ViolationType value) {
        ViolationTypeDAO violationTypeDAO = getViolationTypeDAO();
        return violationTypeDAO != null && violationTypeDAO.contains(value);
    }
}
