package com.fxbank.amcwts.violation.violationtype;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;

import java.sql.*;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Instance of {@code ViolationTypeDAO}, that can working with MySQL DB. For correct work must be initialized {@code ConnectionFactory}.
 * Class {@code MySQLViolationTypeDAO} is Singleton, to obtain an instance, there is a method {@code getInstance()}.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-07
 * @see ViolationType
 * @see ViolationTypeDAO
 */
final class MySQLViolationTypeDAO extends ViolationTypeDAO {
    private static ViolationTypeDAO VIOLATION_TYPE_DAO;
    private final String EXCEPTION_TITLE = "Внутренняя ошибка сервера - Служба работы с нарушениями не доступна: %s";

    private final String INSERT_QUERY = "INSERT INTO VIOLATION_TYPE (`VIOLATION_TYPE_name`) VALUES (?)";
    private final String UPDATE_QUERY = "UPDATE VIOLATION_TYPE SET VIOLATION_TYPE_name = ? WHERE VIOLATION_TYPE_id = ? ";
    private final String DELETE_QUERY = "DELETE FROM VIOLATION_TYPE WHERE VIOLATION_TYPE_id = ?";

    private final Queue<ViolationType> VIOLATION_TYPES = new ConcurrentLinkedQueue<>();

    private MySQLViolationTypeDAO() {
        super("VIOLATION_TYPE");
        SELECT_QUERY = "SELECT * FROM VIOLATION_TYPE";
    }

    /**
     * Method return instance of the MySQLViolationTypeDAO.
     * @return Instance of the MySQLViolationTypeDAO.
     * @see ViolationTypeDAO
     */
    static ViolationTypeDAO getInstance() {
        if(VIOLATION_TYPE_DAO == null)
            VIOLATION_TYPE_DAO = new MySQLViolationTypeDAO();
        return VIOLATION_TYPE_DAO;
    }

    @Override
    public boolean add(ViolationType violationType) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, violationType.getName());

            boolean successUpdate = preparedStatement.executeUpdate() == 1;
            if(successUpdate) {
                long violationTypeKey = getGeneratedKeyLong(preparedStatement);
                if(violationTypeKey != -1) {
                    violationType.setId(violationTypeKey);
                    VIOLATION_TYPES.add(violationType);
                } else
                    return false;
            }
            return successUpdate;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean update(ViolationType violationType) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, violationType.getName());
            preparedStatement.setLong(2, violationType.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result) {
                Optional<ViolationType> violationTypeOptional = Optional.ofNullable(findById(violationType.getId()));
                violationTypeOptional.ifPresent(violationType1 -> violationType1.setName(violationType.getName()));
            }
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    protected ViolationType tryCreate(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("VIOLATION_TYPE_id");
        if(!isCachedWithId(id)) {
            ViolationType violationType = new ViolationType(id, resultSet.getString("VIOLATION_TYPE_name"));
            VIOLATION_TYPES.add(violationType);
            return violationType;
        } else
            return findById(id);
    }

    @Override
    public boolean remove(ViolationType violationType) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
            preparedStatement.setLong(1, violationType.getId());

            boolean result = preparedStatement.executeUpdate() == 1;
            if(result)
                VIOLATION_TYPES.remove(violationType);
            return result;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public ViolationType findById(long id) {
        for(ViolationType violationType : VIOLATION_TYPES)
            if(violationType.getId() == id)
                return violationType;

        return find("VIOLATION_TYPE_id", id);
    }

    @Override
    public boolean contains(ViolationType value) {
        return findById(value.getId()) != null;
    }

    private boolean isCachedWithId(long id) {
        for(ViolationType violationType : VIOLATION_TYPES)
            if(violationType.getId() == id)
                return true;
        return false;
    }

    @Override
    public List<ViolationType> getAll(Person person) {
        return getAll();
    }
}
