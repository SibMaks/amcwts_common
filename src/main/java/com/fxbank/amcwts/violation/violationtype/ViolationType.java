package com.fxbank.amcwts.violation.violationtype;

/**
 * The {@code ViolationType} contains information about violation type.
 * Normally the values of id is non-negative, name is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-03-27
 */
public final class ViolationType {
    private long id;
    private String name;

    /**
     * Creates a new ViolationType object that represents the violation type info.
     * The fields are initialized with the default values.
     */
    public ViolationType() {
    }

    /**
     * Creates a new ViolationType object that represents the violation type info.
     * @param id id of violation type.
     * @param name name of violation type.
     */
    public ViolationType(long id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Returns the id of violation type.
     * @return Returns the id of violation type.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id of this ViolationType object to the specified value.
     * @param id the new value for the ViolationType object.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns the name of violation type.
     * @return Returns the name of violation type.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this ViolationType object to the specified value.
     * @param name the new value for the ViolationType object.
     */
    public void setName(String name) {
        this.name = name;
    }
}
