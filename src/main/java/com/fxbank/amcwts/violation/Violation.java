package com.fxbank.amcwts.violation;

import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.person.worktime.WorkTime;
import com.fxbank.amcwts.violation.violationtype.ViolationType;

import java.time.LocalDateTime;

/**
 * The {@code Violation} contains information about violation.
 * Normally the values of id is non-negative, violationType, date, workTime and person is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-03-27
 */
public final class Violation {
    private long id;
    private String reason;
    private ViolationType violationType;
    private LocalDateTime date;
    private WorkTime workTime;
    private Person person;

    /**
     * Creates a new Violation object that represents the violation info.
     * The fields are initialized with the default values.
     */
    public Violation() {
    }

    /**
     * Creates a new Violation object that represents the violation info.
     * @param id id of violation
     * @param reason reason of violation
     * @param violationType type of violation
     * @param date date of violation
     * @param workTime working time in which the violation occurred.
     * @param person person to whom the violation belongs.
     * @see ViolationType
     * @see Person
     * @see WorkTime
     */
    public Violation(long id, String reason, ViolationType violationType, LocalDateTime date, WorkTime workTime, Person person) {
        this.id = id;
        this.reason = reason;
        this.violationType = violationType;
        this.date = date;
        this.workTime = workTime;
        this.person = person;
    }

    /**
     * Returns the id of violation.
     * @return Returns the id of violation.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id of this Violation object to the specified value.
     * @param id the new value for the Violation object.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns the work time of violation.
     * @return Returns the work time of violation.
     * @see WorkTime
     */
    public WorkTime getWorkTime() {
        return workTime;
    }

    /**
     * Sets the work time of this Violation object to the specified value.
     * @param workTime the new value for the Violation object.
     * @see WorkTime
     */
    public void setWorkTime(WorkTime workTime) {
        this.workTime = workTime;
    }

    /**
     * Returns the reason of violation.
     * @return Returns the reason of violation.
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the reason of this Violation object to the specified value.
     * @param reason the new value for the Violation object.
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * Returns the type of violation.
     * @return Returns the type of violation.
     * @see ViolationType
     */
    public ViolationType getViolationType() {
        return violationType;
    }

    /**
     * Sets the violation type of this Violation object to the specified value.
     * @param violationType the new value for the Violation object.
     * @see ViolationType
     */
    public void setViolationType(ViolationType violationType) {
        this.violationType = violationType;
    }

    /**
     * Returns the date of violation.
     * @return Returns the date of violation.
     * @see LocalDateTime
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     * Sets the date of this Violation object to the specified value.
     * @param date the new value for the Violation object.
     * @see LocalDateTime
     */
    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    /**
     * Returns the person of violation.
     * @return Returns the person of violation.
     * @see Person
     */
    public Person getPerson() {
        return person;
    }

    /**
     * Sets the person of this Violation object to the specified value.
     * @param person the new value for the Violation object.
     * @see Person
     */
    public void setPerson(Person person) {
        this.person = person;
    }
}
