package com.fxbank.amcwts.violation;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;
import com.fxbank.amcwts.person.PersonDAO;
import com.fxbank.amcwts.person.ProxyPersonDAO;
import com.fxbank.amcwts.person.worktime.ProxyWorkTimeDAO;
import com.fxbank.amcwts.person.worktime.WorkTime;
import com.fxbank.amcwts.person.worktime.WorkTimeDAO;
import com.fxbank.amcwts.violation.violationtype.ProxyViolationTypeDAO;
import com.fxbank.amcwts.violation.violationtype.ViolationType;
import com.fxbank.amcwts.violation.violationtype.ViolationTypeDAO;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * Instance of {@code ViolationDAO}, that can working with MySQL DB. For correct work must be initialized {@code ConnectionFactory}.
 * Class {@code MySQLViolationDAO} is Singleton, to obtain an instance, there is a method {@code getInstance()}.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-07
 * @see Violation
 * @see ViolationDAO
 */
final class MySQLViolationDAO extends ViolationDAO {
    private static ViolationDAO VIOLATION_DAO;
    private final String EXCEPTION_TITLE = "Внутренняя ошибка сервера - Служба работы с нарушениями не доступна: %s";

    private final String SELECT_QUERY = "SELECT * FROM VIOLATION ORDER BY DATE";
    private final String SELECT_BY_PERSON_GROUP_QUERY = "SELECT * FROM VIOLATION as violation " +
            "INNER JOIN PERSON person ON person.PERSON_ID = violation.PERSON_ID WHERE person.GROUP_ID = ? ORDER BY DATE";
    private final String SELECT_BY_PERSON_QUERY = "SELECT * FROM VIOLATION WHERE PERSON_id = ? ORDER BY DATE";
    private final String SELECT_BY_DATES = "SELECT * FROM VIOLATION WHERE DATE >= ? AND DATE <= ? ORDER BY DATE";
    private final String SELECT_BY_PERSON_IN_DATES_QUERY = "SELECT * FROM VIOLATION WHERE DATE >= ? AND DATE <= ? AND PERSON_id = ? ORDER BY DATE";
    private final String SELECT_BY_DATES_GROUP_QUERY = "SELECT * FROM VIOLATION as violation INNER JOIN PERSON person ON person.PERSON_ID = violation.PERSON_ID" +
            " WHERE person.GROUP_ID = ? AND DATE >= ? AND DATE <= ? ORDER BY DATE";
    private final String SELECT_LAST = "SELECT * FROM `VIOLATION` ORDER BY `DATE` DESC LIMIT 1";
    private final String SELECT_LAST_BY_PERSON = "SELECT * FROM `VIOLATION` WHERE PERSON_ID = ? ORDER BY `DATE` DESC LIMIT 1";
    private final String CONTAINS_QUERY = "SELECT * FROM VIOLATION WHERE PERSON_ID = ? AND DATE = ? AND VIOLATION_TYPE_id = ? OR VIOLATION_ID = ?";
    private final String INSERT_QUERY = "INSERT INTO VIOLATION (`VIOLATION_REASON`, `VIOLATION_TYPE_ID`, `DATE`, `PERSON_ID`, `WORK_TIME_id`) VALUES (?, ?, ?, ?, ?)";
    private final String UPDATE_QUERY = "UPDATE VIOLATION SET VIOLATION_REASON = ?, VIOLATION_TYPE_ID = ?, DATE = ?, PERSON_ID = ?, WORK_TIME_id = ? WHERE VIOLATION_ID = ? ";
    private final String DELETE_QUERY = "DELETE FROM VIOLATION WHERE VIOLATION_ID = ?";

    private final PersonDAO PERSON_DAO = ProxyPersonDAO.getInstance();
    private final WorkTimeDAO WORK_TIME_DAO = ProxyWorkTimeDAO.getInstance();
    private final ViolationTypeDAO VIOLATION_TYPE_DAO = ProxyViolationTypeDAO.getInstance();

    private MySQLViolationDAO() {
        super("VIOLATION");
    }

    /**
     * Method return instance of the MySQLViolationDAO.
     * @return Instance of the MySQLViolationDAO.
     * @see ViolationDAO
     */
    static ViolationDAO getInstance() {
        if(VIOLATION_DAO == null)
            VIOLATION_DAO = new MySQLViolationDAO();
        return VIOLATION_DAO;
    }

    @Override
    public List<Violation> getAll(Person person) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement;
            int accessLevel = Person.getAccessLevel(person);
            switch (accessLevel) {
                case 0:
                    preparedStatement = connection.prepareStatement(SELECT_QUERY);
                break;
                case 1:
                    preparedStatement = connection.prepareStatement(SELECT_BY_PERSON_GROUP_QUERY);
                    preparedStatement.setLong(1, person.getGroup().getId());
                break;
                default:
                    preparedStatement = connection.prepareStatement(SELECT_BY_PERSON_QUERY);
                    preparedStatement.setLong(1, person.getId());
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            List<Violation> violationList = new LinkedList<>();

            while (resultSet.next()) {
                Violation violation = tryCreate(resultSet);
                if(violation != null)
                    violationList.add(violation);
            }
            return violationList;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public List<Violation> getAll() {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(SELECT_QUERY);

            ResultSet resultSet = preparedStatement.executeQuery();
            List<Violation> violationList = new LinkedList<>();

            while (resultSet.next()) {
                Violation violation = tryCreate(resultSet);
                if(violation != null)
                    violationList.add(violation);
            }
            return violationList;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public List<Violation> getByDatesForPerson(Person person, LocalDateTime from, LocalDateTime to) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_PERSON_IN_DATES_QUERY);
            preparedStatement.setString(1, SERVICE_DATE_TIME_FORMATTER.format(from));
            preparedStatement.setString(2, SERVICE_DATE_TIME_FORMATTER.format(to));
            preparedStatement.setLong(3, person.getId());

            ResultSet resultSet = preparedStatement.executeQuery();
            List<Violation> violationList = new LinkedList<>();

            while (resultSet.next()) {
                Violation violation = tryCreate(resultSet);
                if(violation != null)
                    violationList.add(violation);
            }
            return violationList;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public List<Violation> getAllByDates(Person person, LocalDateTime from, LocalDateTime to) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement;
            int accessLevel = Person.getAccessLevel(person);
            if(accessLevel == 0) {
                preparedStatement = connection.prepareStatement(SELECT_BY_DATES);
                preparedStatement.setString(1, from.format(SERVICE_DATE_TIME_FORMATTER));
                preparedStatement.setString(2, to.format(SERVICE_DATE_TIME_FORMATTER));
            } else if(accessLevel == 1) {
                preparedStatement = connection.prepareStatement(SELECT_BY_DATES_GROUP_QUERY);
                preparedStatement.setLong(1, person.getGroup().getId());
                preparedStatement.setString(2, from.format(SERVICE_DATE_TIME_FORMATTER));
                preparedStatement.setString(3, to.format(SERVICE_DATE_TIME_FORMATTER));
            } else
                return getByDatesForPerson(person, from, to);

            ResultSet resultSet = preparedStatement.executeQuery();
            List<Violation> violationList = new LinkedList<>();

            while (resultSet.next()) {
                Violation violation = tryCreate(resultSet);
                if(violation != null)
                    violationList.add(violation);
            }
            return violationList;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public Violation findById(long id) {
        return find("VIOLATION_ID", id);
    }

    @Override
    public boolean contains(Violation violation) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(CONTAINS_QUERY);
            preparedStatement.setLong(1, violation.getPerson().getId());
            preparedStatement.setString(2, violation.getDate().format(SERVICE_DATE_TIME_FORMATTER));
            preparedStatement.setLong(3, violation.getViolationType().getId());
            preparedStatement.setLong(4, violation.getId());

            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public Violation getLast(Person person) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement;
            int accessLevel = Person.getAccessLevel(person);
            if(accessLevel != 0) {
                preparedStatement = connection.prepareStatement(SELECT_LAST_BY_PERSON);
                preparedStatement.setLong(1, person.getId());
            } else
                preparedStatement = connection.prepareStatement(SELECT_LAST);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next())
                return tryCreate(resultSet);
        } catch (SQLException e) {
            throw new RuntimeException("Внутренняя ошибка сервера - Служба работы с нарушениями не доступна: " + e.getMessage(), e);
        }
        return null;
    }

    @Override
    public boolean add(Violation violation) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, violation.getReason());
            preparedStatement.setLong(2, violation.getViolationType().getId());
            preparedStatement.setString(3, violation.getDate().format(SERVICE_DATE_TIME_FORMATTER));
            preparedStatement.setLong(4, violation.getPerson().getId());
            preparedStatement.setLong(5, violation.getWorkTime().getId());

            boolean successUpdate = preparedStatement.executeUpdate() == 1;
            if(successUpdate) {
                long key = getGeneratedKeyLong(preparedStatement);
                if(key != -1)
                    violation.setId(key);
                else
                    return false;
            }
            return successUpdate;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean remove(Violation violation) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
            preparedStatement.setLong(1, violation.getId());

            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    public boolean update(Violation violation) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, violation.getReason());
            preparedStatement.setLong(2, violation.getViolationType().getId());
            preparedStatement.setString(3, violation.getDate().format(SERVICE_DATE_TIME_FORMATTER));
            preparedStatement.setLong(4, violation.getPerson().getId());
            preparedStatement.setLong(5, violation.getWorkTime().getId());
            preparedStatement.setLong(6, violation.getId());

            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
    }

    @Override
    protected Violation tryCreate(ResultSet resultSet) throws SQLException {
        long violationTypeId = resultSet.getLong("VIOLATION_TYPE_ID");
        long workTimeId = resultSet.getLong("WORK_TIME_id");
        long personId = resultSet.getLong("PERSON_ID");

        ViolationType violationType = VIOLATION_TYPE_DAO.findById(violationTypeId);
        WorkTime workTime = WORK_TIME_DAO.findById(workTimeId);
        if(violationType != null && workTime != null) {
            LocalDateTime violationDate = LocalDateTime.parse(resultSet.getString("DATE"), SERVICE_DATE_TIME_FORMATTER);
            Person person = PERSON_DAO.findById(personId);

            if(person != null)
                return new Violation(resultSet.getLong("VIOLATION_id"), resultSet.getString("VIOLATION_REASON"),
                        violationType, violationDate, workTime, person);
        }
        return null;
    }
}