package com.fxbank.amcwts.response;

import com.fxbank.amcwts.violation.Violation;

import java.util.List;

/**
 * A {@code PeronStatistic} is a class that contains information about person violations, time worked in seconds and
 * the time not worked in seconds. Class extends ServiceResponse, and using then user request statistic.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-09
 * @see Violation
 * @see ServiceResponse
 */
public final class PersonStatisticResponse extends ListResponse<Violation> {
    private long nonWorkingTime;
    private long workingTime;

    /**
     * Creates a new PersonStatisticResponse object that represents the person statistic.
     * @param nonWorkingTime time in seconds that person spent not on the work.
     * @param workingTime time in seconds thar person spent on the work.
     * @param violations list of violations person.
     */
    public PersonStatisticResponse(long nonWorkingTime, long workingTime, List<Violation> violations) {
        super(violations);
        this.nonWorkingTime = nonWorkingTime;
        this.workingTime = workingTime;
    }

    /**
     * Returns the non working time of person in seconds.
     * @return Returns the non working time of person in seconds.
     */
    public long getNonWorkingTime() {
        return nonWorkingTime;
    }

    /**
     * Sets the non working time of this PersonStatisticResponse object to the specified value.
     * @param nonWorkingTime the new value for the PersonStatisticResponse object.
     */
    public void setNonWorkingTime(long nonWorkingTime) {
        this.nonWorkingTime = nonWorkingTime;
    }

    /**
     * Returns the working time of person in seconds.
     * @return Returns the working time of person in seconds.
     */
    public long getWorkingTime() {
        return workingTime;
    }

    /**
     * Sets the working time of this PersonStatisticResponse object to the specified value.
     * @param workingTime the new value for the PersonStatisticResponse object.
     */
    public void setWorkingTime(long workingTime) {
        this.workingTime = workingTime;
    }

    /**
     * Returns the list of violations of person.
     * @return Returns the list of violations of person.
     * @see Violation
     */
    public List<Violation> getViolations() {
        return getValue();
    }

    /**
     * Sets the list of violations of this PersonStatisticResponse object to the specified list.
     * @param violations the new value for the PersonStatisticResponse object.
     */
    public void setViolations(List<Violation> violations) {
        this.value = violations;
    }
}
