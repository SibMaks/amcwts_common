package com.fxbank.amcwts.response;

/**
 * A {@code ServiceResponse} generic class, that using for transfer information from server to user.
 * @author drobyshev-ma
 * @version 0.1
 * @since 05.05.2017
 */
public class ServiceResponse<T> {
    protected T value;
    private String comment;

    /**
     * Creates a new ServiceResponse object that keeping information to transfer.
     * @param value value, thar will be transfer to user.
     */
    protected ServiceResponse(T value) {
        this.value = value;
    }

    /**
     * Creates a new ServiceResponse object that keeping information to transfer.
     * @param value value, thar will be transfer to user.
     * @param comment additional information for user.
     */
    protected ServiceResponse(T value, String comment) {
        this.value = value;
        this.comment = comment;
    }

    /**
     * Returns the value type T that storing.
     * @return Returns the value that storing.
     */
    public T getValue() {
        return value;
    }

    /**
     * Returns the additional information of transfer.
     * @return Returns the additional information of transfer.
     */
    public String getComment() {
        return comment;
    }
}
