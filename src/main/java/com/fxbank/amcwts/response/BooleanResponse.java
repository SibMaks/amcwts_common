package com.fxbank.amcwts.response;

/**
 * A {@code BooleanResponse} extends {@code ServiceResponse} with type Boolean, have 2 static final field with 2 possible value,
 * BOOLEAN_RESPONSE_TRUE with value true and BOOLEAN_RESPONSE_FALSE with value false and both with null comment.
 * @author drobyshev-ma
 * @version 0.1
 * @since 05.05.2017
 * @see ServiceResponse
 */
public final class BooleanResponse extends ServiceResponse<Boolean> {
    public static final BooleanResponse TRUE = new BooleanResponse(true);
    public static final BooleanResponse FALSE = new BooleanResponse(false);

    public BooleanResponse(Boolean value) {
        super(value);
    }

    public BooleanResponse(Boolean value, String comment) {
        super(value, comment);
    }
}