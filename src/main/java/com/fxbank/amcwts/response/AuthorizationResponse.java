package com.fxbank.amcwts.response;

/**
 * The {@code AuthorizationResponse} contains information about authorization response.
 * Normally the values of status is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-10
 * @see ServiceResponse
 */
public final class AuthorizationResponse extends ServiceResponse<AuthorizationResponse.Status> {
    /**
     * Status of authorization response. Can be OK or ERROR.
     */
    public enum Status {
        OK, ERROR
    }

    /**
     * Creates a new AuthorizationResponse object that represents the authorization response info.
     * @param status authorization response status.
     */
    public AuthorizationResponse(Status status) {
        super(status);
    }

    /**
     * Creates a new AuthorizationResponse object that represents the authorization response info.
     * @param status authorization response status.
     * @param comment authorization response comment.
     */
    public AuthorizationResponse(Status status, String comment) {
        super(status, comment);
    }

    /**
     * Returns the status of authorization response.
     * @return Returns the status of authorization response.
     */
    public Status getStatus() {
        return getValue();
    }
}
