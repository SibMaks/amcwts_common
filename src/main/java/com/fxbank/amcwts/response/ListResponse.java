package com.fxbank.amcwts.response;

import java.util.List;

/**
 * A {@code ListResponse} extends {@code ServiceResponse} with type List, but it's too generic class.
 * @author drobyshev-ma
 * @version 0.1
 * @since 05.05.2017
 * @see ServiceResponse
 */
public class ListResponse<T> extends ServiceResponse<List<T>> {
    protected ListResponse(List<T> value) {
        super(value);
    }

    protected ListResponse(List<T> value, String comment) {
        super(value, comment);
    }
}
