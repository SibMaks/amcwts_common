package com.fxbank.amcwts;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.fxbank.amcwts.person.Person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

/**
 * A {@code CommonDAO} contains common methods for DAO classes.
 * @author drobyshev-ma
 * @version 0.1
 * @since 10.05.2017
 */
public abstract class CommonDAO<T> {
    private final String EXCEPTION_TITLE = "Ошибка работы в CommonDAO. %s";
    private final String FIND_QUERY = "SELECT * FROM %s WHERE %s = ?";

    protected static final String CONNECTION_FACTORY_NULL_EXCEPTION = "ConnectionFactory is null.";
    protected String SELECT_QUERY;

    private String tableName;

    protected CommonDAO(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Field contains date format for DBs such as MySQL, MS SQL and more.
     */
    public static final DateTimeFormatter SERVICE_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter MS_SQL_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    /**
     * Method, that return all T of user.
     * @param person user which requested list.
     * @return List of T for person or empty list.
     */
    abstract public List<T> getAll(Person person);

    /**
     * Method, that return all T.
     * @return List of T or empty list.
     */
    public List<T> getAll() {
        List<T> response = new LinkedList<>();
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_QUERY);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                T resultCreate = tryCreate(resultSet);
                if(resultCreate != null)
                    response.add(resultCreate);
            }
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
        return response;
    }

    /**
     * Method, that return T with the specified id.
     * @param id id of T that need finding.
     * @return T or null.
     */
    abstract public T findById(long id);

    /**
     * Method checks whether the value is in the database.
     * @param value value that need check in data base.
     * @return Is there a value in the database.
     */
    abstract public boolean contains(T value);

    /**
     * Method, that add value to data base.
     * @param value value that need add to data base.
     * @return Added or not value in data base. May not be added if the value already added in data base.
     */
    abstract public boolean add(T value);

    /**
     * Method, that remove value from data base.
     * @param value value that need remove from data base.
     * @return Removed or not value from data base. May not be removed if the value already removed from data base.
     */
    abstract public boolean remove(T value);

    /**
     * Method, that update value in data base.
     * @param value value that need update in data base.
     * @return Updated or not value in data base. May not be update if the value not exist in data base.
     */
    abstract public boolean update(T value);

    /**
     * Method, that try create T from ResultSet.
     * @param resultSet result set for create T.
     * @return T or null.
     * @throws SQLException thrown then result set throws exception.
     */
    abstract protected T tryCreate(ResultSet resultSet) throws SQLException;

    /**
     * Method, that return a T that satisfy the function.
     * @param field field on which the search will be made.
     * @param value value that must equals field.
     * @return T or null.
     */
    public<S> T find(String field, S value) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        if(connectionFactory == null)
            throw makeException(EXCEPTION_TITLE, CONNECTION_FACTORY_NULL_EXCEPTION);

        try(Connection connection = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(String.format(FIND_QUERY, tableName, field));
            preparedStatement.setObject(1, value);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next())
                return tryCreate(resultSet);
        } catch (SQLException e) {
            throw makeException(EXCEPTION_TITLE, e);
        }
        return null;
    }

    /**
     * Method, that return generated key from prepare statement long type.
     * @param preparedStatement preparedStatement, whence it is necessary to get the generated key.
     * @return Generated key type long or -1. May throw exception.
     * @see SQLException
     */
    protected final long getGeneratedKeyLong(PreparedStatement preparedStatement) throws SQLException {
        ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
        if (generatedKeys.next()) {
            return generatedKeys.getLong(1);
        }
        return -1;
    }

    /**
     * Method, that generate exception with specific title and cause.
     * @param title title of exception, must contains {@code %s}.
     * @param cause cause of exception.
     * @return RuntimeException with specific title and cause.
     * @see RuntimeException
     */
    protected final RuntimeException makeException(String title, Exception cause) {
        String message = cause == null ? "" : cause.getMessage();
        return new RuntimeException(String.format(title, message), cause);
    }

    /**
     * Method, that generate exception with specific title and message.
     * @param title title of exception, must contains {@code %s}.
     * @param message message for exception.
     * @return RuntimeException with specific title and cause.
     * @see RuntimeException
     */
    protected final RuntimeException makeException(String title, String message) {
        message = message == null ? "" : message;
        return new RuntimeException(String.format(title, message));
    }
}
