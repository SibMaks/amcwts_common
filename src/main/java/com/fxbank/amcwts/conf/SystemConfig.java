package com.fxbank.amcwts.conf;

/**
 * The {@code SystemConfig} contains information about system config.
 * Normally the values of currentDB, adminLogin, adminPassword, parsecConfig and supportedBd is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-06
 */
public final class SystemConfig {
    private boolean serviceOn;
    private String currentDB;
    private String adminLogin;
    private String adminPassword;
    private boolean adminActive;
    private ParsecConfig parsecConfig;
    private DBDescription[] supportedBd;

    /**
     * Creates a new SystemConfig object that represents the system configuration info.
     * The fields are initialized with the default values.
     */
    public SystemConfig() {
    }

    /**
     * Creates a new SystemConfig object that represents the system configuration info.
     * @param serviceOn service on.
     * @param currentDB current data base.
     * @param adminLogin admin login for service.
     * @param adminPassword admin password for service.
     * @param adminActive admin is active.
     * @param parsecConfig Parsec configuration.
     * @param supportedBd array of supported data bases.
     */
    public SystemConfig(boolean serviceOn, String currentDB, String adminLogin, String adminPassword, boolean adminActive, ParsecConfig parsecConfig, DBDescription[] supportedBd) {
        this.serviceOn = serviceOn;
        this.currentDB = currentDB;
        this.adminLogin = adminLogin;
        this.adminPassword = adminPassword;
        this.adminActive = adminActive;
        this.parsecConfig = parsecConfig;
        this.supportedBd = supportedBd;
    }

    /**
     * Returns the service is on.
     * @return Returns the service is on.
     */
    public boolean isServiceOn() {
        return serviceOn;
    }

    /**
     * Sets the serviceOn of this SystemConfig object to the specified value.
     * @param serviceOn the new value for the SystemConfig object.
     */
    public void setServiceOn(boolean serviceOn) {
        this.serviceOn = serviceOn;
    }

    /**
     * Returns the current data base of system.
     * @return Returns the current data base of system.
     */
    public String getCurrentDB() {
        return currentDB;
    }

    /**
     * Sets the current data base of this SystemConfig object to the specified value.
     * @param currentDB the new value for the SystemConfig object.
     */
    public void setCurrentDB(String currentDB) {
        this.currentDB = currentDB;
    }

    /**
     * Returns the admin login for enter in system.
     * @return Returns the admin login for enter in system.
     */
    public String getAdminLogin() {
        return adminLogin;
    }

    /**
     * Sets the admin login of this SystemConfig object to the specified value.
     * @param adminLogin the new value for the SystemConfig object.
     */
    public void setAdminLogin(String adminLogin) {
        this.adminLogin = adminLogin;
    }

    /**
     * Returns the admin password for enter in system.
     * @return Returns the admin password for enter in system.
     */
    public String getAdminPassword() {
        return adminPassword;
    }

    /**
     * Sets the admin password of this SystemConfig object to the specified value.
     * @param adminPassword the new value for the SystemConfig object.
     */
    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    /**
     * Returns the admin is active.
     * @return Returns the admin is active.
     */
    public boolean isAdminActive() {
        return adminActive;
    }

    /**
     * Sets the admin is active of this SystemConfig object to the specified value.
     * @param adminActive the new value for the SystemConfig object.
     */
    public void setAdminActive(boolean adminActive) {
        this.adminActive = adminActive;
    }

    /**
     * Returns the Parsec configuration of system.
     * @return Returns the Parsec configuration of system.
     * @see ParsecConfig
     */
    public ParsecConfig getParsecConfig() {
        return parsecConfig;
    }

    /**
     * Sets the Parsec configuration of this SystemConfig object to the specified value.
     * @param parsecConfig the new value for the SystemConfig object.
     * @see ParsecConfig
     */
    public void setParsecConfig(ParsecConfig parsecConfig) {
        this.parsecConfig = parsecConfig;
    }

    /**
     * Returns the array of supported data bases.
     * @return Returns the array of supported data bases.
     * @see DBDescription
     */
    public DBDescription[] getSupportedBd() {
        return supportedBd;
    }

    /**
     * Sets the array of supported data bases of this SystemConfig object to the specified value.
     * @param supportedBd the new value for the SystemConfig object.
     * @see DBDescription
     */
    public void setSupportedBd(DBDescription[] supportedBd) {
        this.supportedBd = supportedBd;
    }

    /**
     * Find and return {@code DBDescription} by name.
     * @param name name of data base for search.
     * @return {@code DBDescription} or null.
     * @see DBDescription
     */
    public DBDescription findDBDescriptionByName(String name) {
        if(supportedBd == null)
            throw new NullPointerException("DB descriptions is null.");
        if(name == null)
            throw new IllegalArgumentException("Parameter name must be initialized.");

        for(DBDescription dbDescription : supportedBd)
            if(dbDescription.getName().equals(name))
                return dbDescription;
        return null;
    }
}
