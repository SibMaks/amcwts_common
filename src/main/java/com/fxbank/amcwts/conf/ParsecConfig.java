package com.fxbank.amcwts.conf;

/**
 * The {@code ParsecConfig} contains information about Parsec configuration.
 * Normally the values of login, password and address is not null, port is not negative.
 * @author drobyshev-ma
 * @version 0.1
 * @since 2017-04-30
 */
public final class ParsecConfig {
    private String login;
    private String password;
    private String address;
    private int port;

    /**
     * Creates a new ParsecConfig object that represents the Parsec configuration info.
     * The fields are initialized with the default values.
     */
    public ParsecConfig() {
    }

    /**
     * Creates a new ParsecConfig object that represents the Parsec configuration info.
     * @param login login for connecting to Parsec.
     * @param password password for connecting to Parsec.
     * @param address address for connecting to Parsec.
     * @param port port for connecting to Parsec.
     */
    public ParsecConfig(String login, String password, String address, int port) {
        this.login = login;
        this.password = password;
        this.address = address;
        this.port = port;
    }

    /**
     * Returns the login for Parsec.
     * @return Returns the login for Parsec.
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the login of this ParsecConfig object to the specified value.
     * @param login the new value for the ParsecConfig object.
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Returns the password for Parsec.
     * @return Returns the password for Parsec.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password of this ParsecConfig object to the specified value.
     * @param password the new value for the ParsecConfig object.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns the address for Parsec.
     * @return Returns the address for Parsec.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the address of this ParsecConfig object to the specified value.
     * @param address the new value for the ParsecConfig object.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Returns the port for Parsec.
     * @return Returns the port for Parsec.
     */
    public int getPort() {
        return port;
    }

    /**
     * Sets the port of this ParsecConfig object to the specified value.
     * @param port the new value for the ParsecConfig object.
     */
    public void setPort(int port) {
        this.port = port;
    }
}