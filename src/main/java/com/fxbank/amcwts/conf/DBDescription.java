package com.fxbank.amcwts.conf;

/**
 * The {@code DBDescription} contains description of data base.
 * Normally the values of name, login, password, connectionString and driverClassName is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-06
 */
public final class DBDescription {
    private String name;
    private String login;
    private String password;
    private String connectionString;
    private String driverClassName;

    /**
     * Creates a new DBDescription object that represents the data base description.
     * The fields are initialized with the default values.
     */
    public DBDescription() {
    }

    /**
     * Creates a new DBDescription object that represents the data base description.
     * @param name name of data base.
     * @param login login for connect to data base.
     * @param password password for connect to data base.
     * @param connectionString connectionString for connect to data base.
     * @param driverClassName driverClassName that must be using for connect to data base.
     */
    public DBDescription(String name, String login, String password, String connectionString, String driverClassName) {
        this.name = name;
        this.login = login;
        this.password = password;
        this.connectionString = connectionString;
        this.driverClassName = driverClassName;
    }

    /**
     * Returns the name of data base.
     * @return Returns the name of data base.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of data base of this DBDescription object to the specified value.
     * @param name the new value for the DBDescription object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns login for data base.
     * @return Returns login for data base.
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the login for data base of this DBDescription object to the specified value.
     * @param login the new value for the DBDescription object.
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Returns password for data base.
     * @return Returns password for data base.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password for data base of this DBDescription object to the specified value.
     * @param password the new value for the DBDescription object.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns connection string for data base.
     * @return Returns connection string for data base.
     */
    public String getConnectionString() {
        return connectionString;
    }

    /**
     * Sets the connection string for data base of this DBDescription object to the specified value.
     * @param connectionString the new value for the DBDescription object.
     */
    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    /**
     * Returns driver class name for data base.
     * @return Returns driver class name for data base.
     */
    public String getDriverClassName() {
        return driverClassName;
    }

    /**
     * Sets the driver class name for data base of this DBDescription object to the specified value.
     * @param driverClassName the new value for the DBDescription object.
     */
    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }
}
