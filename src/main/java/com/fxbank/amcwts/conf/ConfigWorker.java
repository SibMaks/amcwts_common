package com.fxbank.amcwts.conf;

import com.fxbank.amcwts.connection.ConnectionFactory;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.*;

/**
 * The {@code ConfigWorker} is needed to work with {@code SystemConfig}.
 * Before using a class, you must initialize it - {@link ConfigWorker#init(File)}.
 * Normally the values of configFile, systemConfig is not null.
 * @author Maksim
 * @version 0.1
 * @since 2017-04-06
 */
public final class ConfigWorker {
    private static ConfigWorker instance;

    private final Gson GSON;
    private File configFile;
    private SystemConfig systemConfig;

    private ConfigWorker() {
        GSON = new Gson();
    }

    public static ConfigWorker getInstance() {
        if(instance == null)
            instance = new ConfigWorker();
        return instance;
    }

    /**
     * Initialization {@code ConfigWorker} by using configuration file.
     * @param fileToConf file that references an existing configuration file.
     * @see File
     */
    public void init(File fileToConf) {
        if(fileToConf.exists()) {
            configFile = fileToConf;
            try (FileReader fileReader = new FileReader(fileToConf);
                 JsonReader reader = new JsonReader(fileReader)) {
                systemConfig = GSON.fromJson(reader, SystemConfig.class);
            } catch (IOException ioException) {
                ioException.printStackTrace();
                throw new RuntimeException("Ошибка чтения файла конфигурации.", ioException);
            }
        } else
            throw new IllegalArgumentException("Файл конфигурации не найден.");
    }

    /**
     * Returns system configuration.
     * @return Returns system configuration.
     */
    public SystemConfig getSystemConfig() {
        if(systemConfig == null)
            throw new IllegalStateException("ConfigWorker не инициализирован.");
        return systemConfig;
    }

    /**
     * Updated configuration file by current system configuration.
     */
    public synchronized void update() {
        if(systemConfig == null)
            throw new IllegalStateException("ConfigWorker не инициализирован.");
        try (FileWriter writer = new FileWriter(configFile)) {
            String jsonString = GSON.toJson(systemConfig, SystemConfig.class);
            writer.write(jsonString);
            writer.flush();

            ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
            if(connectionFactory == null)
                throw new RuntimeException("ConnectionFactory is null.");
            connectionFactory.updateProperties();

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Ошибка работы с файлом конфигурации.", e);
        }
    }
}